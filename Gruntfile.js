module.exports = function (grunt) {

    var jsFiles = [
        'assets/template_front/js/jquery-3.6.0.min.js',
        'assets/template_front/js/bootstrap.min.js',
        'assets/template_front/js/jquery.counterup.min.js',
        'assets/template_front/js/popper.min.js',
        'assets/template_front/js/progressbar.min.js',
        'assets/template_front/js/jquery.magnific-popup.min.js',
        // 'assets/template_front/js/swiper-bundle.min.js',
        'assets/template_front/js/jquery.waypoints.min.js',
        'assets/template_front/js/jquery.meanmenu.min.js',
        'assets/template_front/js/jquery.nice-select.min.js',
        'assets/template_front/js/wow.min.js',
        'assets/template_front/js/custom.js',
    ];
    var cssFiles = [
        'assets/template_front/css/bootstrap.min.css"',
        'assets/template_front/css/fontawesome.min.css',
        'assets/template_front/css/animate.css',
        'assets/template_front/css/swiper-bundle.min.css',
        'assets/template_front/css/magnific-popup.css',
        'assets/template_front/css/nice-select.css',
        'assets/template_front/css/meanmenu.min.css',
        'assets/template_front/fonts/flaticon.css',
        'assets/template_front/sass/style.css',
        'assets/template_front/sass/custom.css',
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'assets/template_front/js/',
        cssDistDir: 'assets/template_front/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};