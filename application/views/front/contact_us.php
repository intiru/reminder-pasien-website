<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $top_contact_us->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="team-area info-box-two pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-9">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line"><?php echo $page->title ?></h5>
                    <h2 class="title"><?php echo $page->title_sub ?></h2>
                    <p class="content"><?php echo $page->description ?></p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <a href="<?php echo $address_link ?>" target="_blank" class="col-lg-4 col-md-6">
                <div class="single-contact-inner text-center">
                    <div class="icon-box">
                        <i class="icomoon-pin"></i>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h3><?= $dict_office ?></h3>
                            <p><?php echo $address ?></p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="<?php echo $email_link ?>" target="_blank" class="col-lg-4 col-md-6">
                <div class="single-contact-inner text-center">
                    <div class="icon-box">
                        <i class=" icomoon-email"></i>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h3><?= $dict_email_address ?></h3>
                            <p><?php echo $email ?></p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="<?php echo $whatsapp_link ?>" target="_blank" class="col-lg-4 col-md-6">
                <div class="single-contact-inner text-center">
                    <div class="icon-box">
                        <i class=" icomoon-telephone"></i>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h3>Whatsapp Link</h3>
                            <p><?php echo $whatsapp_phone ?></p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="contact-area info-box-two pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="g-map-contact">
            <div class="row justify-content-end">
                <div class="col-lg-12 col-md-12">
                    <form class="contact-form-wrap" action="<?php echo site_url('contact_us/send') ?>" method="post" target="_blank">
                        <div class="consulting-contact-form mx-4">
                            <h3 class="mb-3"><?= $dict_consulting ?> </h3>
                            <div class="single-input-inner style-bg">
                                <input type="text" name="name" placeholder="<?= $dict_full_name ?>" required>
                            </div>
                            <div class="single-input-inner style-bg">
                                <input type="text" name="city" placeholder="<?= $dict_contact_city ?>" required>
                            </div>
                            <div class="single-input-inner style-bg">
                                <textarea name="message" placeholder="<?= $dict_contact_message ?>" required></textarea>
                            </div>
                            <div class="btn-wrap pb-3">
                                <button type="submit" class="btn btn-base"><?= $dict_contact_submit ?> </button>
                            </div>
                            <p class="form-messege mb-0 mt-20 text-center"></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--<div class="g-map-inner">-->
<!--    <iframe src="--><?php //echo $maps ?><!--"></iframe>-->
<!--</div>-->
<!--<div class="g-map-contact">-->
<!--    <div class="row justify-content-end">-->
<!--        <div class="col-lg-5 col-md-7">-->
<!--            <form class="contact-form-wrap" action="--><?php //echo site_url('contact_us/send') ?><!--" method="post" target="_blank">-->
<!--                <div class="consulting-contact-form mx-4">-->
<!--                    <h3 class="mb-3">--><?//= $dict_consulting ?><!-- </h3>-->
<!--                    <div class="single-input-inner style-bg">-->
<!--                        <input type="text" name="name" placeholder="--><?//= $dict_full_name ?><!--" required>-->
<!--                    </div>-->
<!--                    <div class="single-input-inner style-bg">-->
<!--                        <input type="text" name="city" placeholder="--><?//= $dict_contact_city ?><!--" required>-->
<!--                    </div>-->
<!--                    <div class="single-input-inner style-bg">-->
<!--                        <textarea name="message" placeholder="--><?//= $dict_contact_message ?><!--" required></textarea>-->
<!--                    </div>-->
<!--                    <div class="btn-wrap pb-3">-->
<!--                        <button type="submit" class="btn btn-base">--><?//= $dict_contact_submit ?><!-- </button>-->
<!--                    </div>-->
<!--                    <p class="form-messege mb-0 mt-20 text-center"></p>-->
<!--                </div>-->
<!--            </form>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->