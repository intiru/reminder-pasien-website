<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h1 class="page-title"><?php echo $page->title ?></h1>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li>
                            <a href="<?php echo $this->main->permalink([$top_blog->title_menu]) ?>"><?php echo $top_blog->title_menu ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blog-area pd-top-120 pd-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="blog-details-page-content">
                    <div class="single-blog-inner">
                        <div class="thumb">
                            <img src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>"
                                 alt="<?php echo $page->thumbnail_alt ?>">
                        </div>
                        <div class="details">
                            <ul class="blog-meta">
                                <li><i class="far fa-user"></i> <?= $dict_admin ?></li>
                                <li>
                                    <i class="far fa-calendar-alt"></i> <?php echo date('d F Y', strtotime($page->created_at)); ?>
                                </li>
                                <li><i class="far fa-eye"></i> <?php echo $page->views ?> Views</li>
                            </ul>
                            <?php echo $page->description ?>
                            <div class="tag-and-share">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="tags d-inline-block">

                                        </div>
                                    </div>
                                    <div class="col-sm-5 mt-3 mt-sm-0 text-sm-end align-self-center">
                                        <div class="blog-share">
                                            <ul>
                                                <li>
                                                    <a href="<?php echo $this->main->share_link('facebook', $page->title, $this->main->permalink([$top_blog->title_menu, $page->title])) ?>"
                                                       target="_blank"><i class="fab fa-facebook-f"
                                                                          aria-hidden="true"></i></a></li>
                                                <li>
                                                    <a href="<?php echo $this->main->share_link('twitter', $page->title, $this->main->permalink([$top_blog->title_menu, $page->title])) ?>"
                                                       target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo $this->main->share_link('linkedin', $page->title, $this->main->permalink([$top_blog->title_menu, $page->title])) ?>"
                                                       target="_blank"><i class="fab fa-linkedin"
                                                                          aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="td-sidebar">
                    <div class="widget widget_search">
                        <h4 class="widget-title"><?= $dict_search ?></h4>
                        <form class="search-form" action="<?php echo $this->main->permalink([$top_blog->title_menu]) ?>"
                              method="get">
                            <div class="form-group">
                                <input type="text" name="search" placeholder="Search here ..."
                                       value="<?php echo $keyword ?>">
                            </div>
                            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <div class="widget widget-recent-post">
                        <h4 class="widget-title">Recent <?php echo $top_blog->title_menu ?></h4>
                        <ul>
                            <?php foreach ($blog_recent as $row) {
                                $permalink = $this->main->permalink([$top_blog->title_menu, $row->title]); ?>
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="<?php echo $permalink ?>">
                                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                     width="100"
                                                     alt="<?php echo $row->thumbnail_alt ?>">
                                            </a>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h6 class="title">
                                                <a href="<?php echo $permalink ?>"><?php echo $row->title ?></a>
                                            </h6>
                                            <div class="post-info"><i
                                                        class="far fa-calendar-alt"></i><span><?php echo date('d F Y', strtotime($row->created_at)) ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>