<footer class="style-14" id="kontak">
    <div class="container">
        <div class="content section-padding">
            <div class="row justify-content-between gx-0">
                <div class="col-lg-4">
                    <div class="foot-info">
                        <div class="foot-logo mb-3 text-center text-md-start">
                            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-reminder-pasien-footer.png" alt="Logo Reminder Pasien">
                        </div>
                        <p class="text-center text-md-start">Kami memberikan Pelayanan & Kenyamanan Maksimal bagi
                            Pengguna ReminderPasien.com
                        </p>
                        <div class="social-icons mt-3 text-center">
                            <a href="https://www.facebook.com/reminderpasien" class="teks"> <i
                                        class="fab fa-facebook-f "></i> </a>
                            <a href="https://www.instagram.com/reminderpasien" class="teks"> <i
                                        class="fab fa-instagram "></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="links text-center text-md-start">
                        <h6 class="sub-link text-uppercase mb-20 mt-5 mt-lg-0"> Daftar Menu </h6>
                        <ul class="d-flex flex-column flex-md-row justify-content-center align-items-center">
                            <li><a href="#" class="teks"> Beranda </a></li>
                            <li><a href="#fitur" class="teks"> Fitur </a></li>
                            <li><a href="#harga" class="teks"> Harga </a></li>
                            <li><a href="blog-hub-pasien.html" class="teks"> Blog </a></li>
                            <li><a href="faq.html" class="teks"> FAQ </a></li>
                            <li><a href="#kontak" class="teks"> Kontak Kami </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 d-inline-block">
                    <div class="links text-center text-md-start">
                        <h6 class="sub-link text-uppercase mb-20 mt-5 mt-lg-0"> Kontak Kami </h6>
                        <p class="fs-14px mb-2"><a
                                    href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                                    class="teks"> <i class="fa-solid fa-phone text-black"></i> 0812 3737 6068 </a></p>
                        <p class="fs-14px mb-2"><a href="mailto:reminderpasien@gmail.com" class="teks"> <i
                                        class="fa-solid fa-envelope"></i> reminderpasien@gmail.com </a></p>
                        <p class="fs-14px"><a href="home-payment-solutions.html#" class="teks"> <i
                                        class="fa-solid fa-location-dot"></i>
                                Tembau, Jl. Sangalangit, Penatih, Kec. Denpasar Tim., Kota Denpasar, Bali 80238 </a></p>
                    </div>
                </div>

            </div>
        </div>
        <div class="foot border-1 border-top brd-gray py-3 mt-0-imp">
            <div class="row align-items-center">
                <div class="col-lg-12 text-center">
                    <div class="text color-666 my-2"> © copyright 2022 by <a href="#"
                                                                             class="fw-bold text-decoration-underline teks">reminderpasien.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
   class="for-wa-fixed">
    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/whatsapp-kontak.png" alt="Hubungi Kami">
</a>
<a href="#"
   class="to_top bg-gray rounded-circle icon-40 d-inline-flex align-items-center justify-content-center">
    <i class="bi bi-chevron-up fs-6 text-dark"></i>
</a>