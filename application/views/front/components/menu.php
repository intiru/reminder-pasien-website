<nav class="navbar navbar-expand-lg navbar-light style-5">
    <div class="container">
        <a class="col-lg-2 col-md-3 col-8" href="<?php echo site_url() ?>">
            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-reminder-pasien.png" alt="Logo Reminder Pasien">
        </a>
        <button class="navbar-toggler py-3" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav m-auto">
                <li class="nav-item">
                    <a class="nav-link text-center" href="<?php echo site_url() ?>">
                        Beranda
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-center" href="#fitur">
                        Fitur
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-center" href="#harga">
                        Harga
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-center" href="<?php echo base_url('blog') ?>">
                        Blog
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-center" href="#kontak">
                        Kontak Kami
                    </a>
                </li>
                <li class="nav-item d-inline d-md-none">
                    <a class="nav-link text-center pt-0" href="page-contact-5.html">
                        <i class="bi bi-person fs-5 me-1 for-sign"></i>
                        sign in
                    </a>
                </li>
            </ul>
            <div class="nav-side">
                <div class="d-flex align-items-center justify-content-center">
                        <span class="nav-item d-none d-md-inline">
                            <a class="nav-link" href="<?php echo base_url('login') ?>">
                                <i class="bi bi-person fs-5 me-1 for-sign"></i>
                                sign in
                            </a>
                        </span>
                    <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                       class="btn rounded-pill blue5-3Dbutn hover-blue3 sm-butn fw-bold fs-16px bg-hijau-wa">
                        <span class="text-light"> <i class="fab fa-whatsapp fs-5 mx-1 pe-1 border-end"></i> Registrasi </span>
                    </a>
                    <div class="dropdown">
                        <button class="icon-35 dropdown-toggle p-0 border-0 bg-transparent rounded-circle img-cover text-black ms-2"
                                type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/lang.png" alt="lang">
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#">English</a></li>
                            <li><a class="dropdown-item" href="#">Arabic</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>