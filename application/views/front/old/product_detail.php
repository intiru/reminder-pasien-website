<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title">Service Details</h2>
                    <ul class="page-list">
                        <li><a href="index.html">Home</a></li>
                        <li>Service Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="project-area pd-top-120 mb-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="blog-details-page-content">
                    <div class="single-blog-inner">
                        <div class="thumb">
                            <img src="<?php echo base_url() ?>assets/template_front/img/service/service-single.webp" alt="img">
                        </div>
                        <div class="details">
                            <h2>It management</h2>
                            <p>Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget elementum acondimentum eget, diam. Nam at tortor in tellus interdum sagitliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet.</p>
                            <p>Lorem available market standard dummy text available market industry Lorem Ipsum simply dummy text of free available market.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,</p>
                            <h4 class="pt-4 mb-4">Key benefits</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="media single-choose-inner">
                                        <div class="media-left">
                                            <div class="icon">
                                                <i class="icomoon-gear"></i>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h4>Flexible Solutions</h4>
                                            <p>Maecenas tempus, tellus eget condime honcus sem quam semper </p>
                                        </div>
                                    </div>
                                    <div class="media single-choose-inner">
                                        <div class="media-left">
                                            <div class="icon">
                                                <i class="icomoon-time"></i>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h4>24/7 Unlimited Support</h4>
                                            <p>Maecenas tempus, tellus eget condime honcus sem quam semper </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media single-choose-inner">
                                        <div class="media-left">
                                            <div class="icon">
                                                <i class="icomoon-team"></i>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h4>Flexible Solutions</h4>
                                            <p>Maecenas tempus, tellus eget condime honcus sem quam semper </p>
                                        </div>
                                    </div>
                                    <div class="media single-choose-inner">
                                        <div class="media-left">
                                            <div class="icon">
                                                <i class="icomoon-profile"></i>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h4>24/7 Unlimited Support</h4>
                                            <p>Maecenas tempus, tellus eget condime honcus sem quam semper </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4>More information</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="accordion mt-2" id="accordionExample">
                                        <div class="accordion-item single-accordion-inner style-2">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Why we are?
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Maecenas tempus, tellus eget condime honcus sem quam semper libero sit amet adipiscingem neque sed ipsum. amquam nunc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item single-accordion-inner style-2">
                                            <h2 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    What we do for you?
                                                </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Maecenas tempus, tellus eget condime honcus sem quam semper libero sit amet adipiscingem neque sed ipsum. amquam nunc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item single-accordion-inner style-2">
                                            <h2 class="accordion-header" id="headingThree">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    100% data security
                                                </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Maecenas tempus, tellus eget condime honcus sem quam semper libero sit amet adipiscingem neque sed ipsum. amquam nunc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>