<div class="page__banner" data-background="<?php echo base_url() ?>assets/template_front/img/page-banner.png">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page__banner-content">
                    <h1><?php echo $page->title ?></h1>
                    <div class="page__banner-menu">
                        <ul>
                            <li><a href="<?php echo site_url() ?>"><i
                                            class="flaticon-home-icon-silhouette"></i> <?php echo $top_home->title_menu ?>
                                    - </a>
                            </li>
                            <li> <?php echo $page->title_menu ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about__area section-padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-12 col-lg-12">
                <div class="about__area-right">
                    <div class="about__area-right-title">
                        <p class="section-top"><?php echo $page->title_sub ?></p>
                        <h2><?php echo $page->title ?></h2>
                        <?php echo $page->description ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team__area-team section-padding">
    <div class="container">
        <div class="row">
            <?php foreach($product_list as $row) {
                $permalink = $this->main->permalink([$top_our_product->title_menu, $row->title]); ?>
            <div class="col-sm-12 col-xl-4 col-lg-4 col-md-4 mb-30">
                <div class="team__area-item">
                    <div class="team__area-image p-relative">
                        <div class="team__area-image-img">
                            <a href="<?php echo $permalink ?>">
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->thumbnail_alt ?>">
                            </a>
                        </div>
                        <div class="team__area-team-content text-center">
                            <h3><a href="<?php echo $permalink ?>"><?php echo $row->title ?></a></h3>
                            <p><?php echo $row->thumbnail_alt ?></p>
                            <br />
                            <a href="<?php echo $permalink ?>" class="btn-sm theme-btn"><?php echo $dict_read_more ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>