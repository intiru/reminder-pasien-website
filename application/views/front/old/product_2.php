<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title">Income Protection</h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $page->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-area pd-top-90 pd-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="mask-bg-wrap mask-bg-img-3">
                    <div class="thumb">
                        <img src="<?php echo base_url() ?>assets/template_front/img/about/3.webp" alt="img">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="section-title px-lg-5 mb-0">
                    <h5 class="sub-title left-border">About The Insurance</h5>
                    <p class="content-strong mt-2 mb-4">
                        Protect your income against an accident or an illness that prevents you from attending your
                        usual occupation.
                    </p>
                    <h5 class="sub-title left-border">Key Features</h5>
                    <p class="content-strong mt-2 mb-4">Income Replacement Insurance</p>
                    <div class="list-wrap mt-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Lump sum for Critical illness</li>
                                    <li>Lump sum for Accidental Death & Disablement</li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Lump sum for Total Permanent Disability</li>
                                    <li>Get extra life insurance</li>
                                    <li>Designed with Expats in mind</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team-area bg-blue pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">About the Plans</h5>
                    <h2 class="title">What is Income Protection Insurance?</h2>
                    <p class="content">
                        Critical illness, injury and casualties can occur while you are anywhere and anytime. Depending
                        on the severity of the accident you could end up with permanent disability, loss of limb or loss
                        of life. Protecting yourself and your family while the main breadwinner is unable to work can
                        save you a lot of extra stress.
                    </p>
                    <p class="content">
                        Income protection insurance by Allianz protects you and your family by providing you with a lump
                        sum payment if you can no longer attend your normal job because you are diagnosed with a
                        critical illness, accident or permanent total disability.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>01</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Critical illness Benefit (Flexi CI)</h4>
                            <p>Flexi CI is the latest additional insurance from Allianz. Through the Flexi CI program,
                                insurance participants get protection against the risk of up to 168 critical illness
                                conditions. If the insured, as the breadwinner, is diagnosed with a critical illness for
                                the first time, the insured's family will get additional compensation.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>02</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Accidental Death & Disablement Benefit (ADDB)</h4>
                            <p>Will the amount of life benefit you have prepared as an inheritance value will remain
                                valuable against future inflation? Moreover, there is always the risk of permanent
                                disability befall the breadwinner so that future financial plans can be scrapped.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>03</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Total Permanent Disability (TPD)</h4>
                            <p>Total Permanent Disability is an additional product from the insurance company Allianz,
                                which offers protection in the form of compensation if the insurance policyholder
                                experiences a total permanent disability due to illness or accident which results in
                                loss of ability to earn a living.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="faq-area pd-top-100 pd-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line">Income Protection</h5>
                    <h2 class="title">Product Terms</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-service-inner style-2 text-center"
                             style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp');">
                            <div class="icon-box">
                                <i class="icomoon-user-3"></i>
                            </div>
                            <div class="details">
                                <h3>Entry Age</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-service-inner style-2 text-center"
                             style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp');">
                            <div class="icon-box">
                                <i class="icomoon-money"></i>
                            </div>
                            <div class="details">
                                <h3>Currency</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-service-inner style-2 text-center"
                             style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp');">
                            <div class="icon-box">
                                <i class="icomoon-analysis"></i>
                            </div>
                            <div class="details">
                                <h3>Underwriting</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                Entry Age
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>For Critical illness Benefit, Prospective policyholders age 1 month - 70 years
                                    </li>
                                    <li>For Accidental Death &amp; Disablement Benefit, Prospective policyholders age 1
                                        month - 64 years
                                    </li>
                                    <li>For Total Permanent&nbsp;<em>Disability Benefit</em>, Prospective policyholders
                                        age 6 month - 69 years
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                Currency
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>The currency used for insurance policy payments is Rupiah (IDR)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                Underwriting
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Full Underwriting from the insurance company Allianz follows the Basic Policy
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="faq-area pd-top-60 pd-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title mb-0 text-center">
                    <h5 class="sub-title double-line">Income Protection</h5>
                    <h2 class="title">Product Details</h2>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                Premium Payment Method
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Premium Payment Method for Income Protection Insurance Program following the
                                        Basic Policy (monthly, quarterly, semester, yearly)
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                Age of Coverage of Insurance Policy Holders
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Up to 99 years for Critical illness Benefit</li>
                                    <li>Up to 65 years for Accidental Death &amp; Disablement Benefit</li>
                                    <li>Up to 70 years for Total Permanent&nbsp;<em>Disability Benefit</em></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                Waiting Period
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <p>The details of the waiting period for Policyholders are as follows:</p>
                                <ul>
                                    <li>90 days waiting period for Critical illness Benefit</li>
                                    <li>No waiting period for Accidental Death &amp; Disablement Benefit</li>
                                    <li>No waiting period for Total Permanent&nbsp;<em>Disability Benefit, But for a
                                            defect, there is a requirement to survive 180 days to show that the defect
                                            is permanent</em></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-relative pd-top-60 pd-bottom-60">
    <img class="shape-left-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/3.webp"
         alt="img">
    <img class="shape-right-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/4.webp"
         alt="img">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line">Income Insurance</h5>
                    <h2 class="title">Identity required to apply</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6">
                <div class="single-service-inner style-2 text-center"
                     style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp');">
                    <div class="icon-box">
                        <i class="icomoon-profile"></i>
                    </div>
                    <div class="details">
                        <h3>KITAS/KITAP</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-service-inner style-2 text-center"
                     style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp');">
                    <div class="icon-box">
                        <i class="icomoon-cloud-data"></i>
                    </div>
                    <div class="details">
                        <h3>PASSPORT</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>