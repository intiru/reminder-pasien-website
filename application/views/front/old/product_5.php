<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h4 class="text-white">TravelPRO International Insurance - Enhanced</h4>
                    <h2 class="page-title">Travel More, Worry Less</h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $page->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="service-area bg-relative pd-top-60 pd-bottom-90">
    <img class="shape-left-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/3.webp" alt="img">
    <img class="shape-right-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/4.webp" alt="img">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line">Travel Insurance</h5>
                    <h2 class="title">Maximum protection accompanies your trip</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp'); height: 90%">
                    <div class="icon-box">
                        <img src="<?php echo base_url() ?>assets/template_front/img/bg/1.png">
                    </div>
                    <div class="details">
                        <h4>Trip cancellation & change protection</h4>
                        <h6>(extra epidemic/pandemic protection)</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp'); height: 90%">
                    <div class="icon-box">
                        <img src="<?php echo base_url() ?>assets/template_front/img/bg/2.png">
                    </div>
                    <div class="details">
                        <h4>Treatment abroad without cost limits for the highest plan</h4>
                        <h6>(extra epidemic/pandemic protection)</h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp'); height: 90%">
                    <div class="icon-box">
                        <img src="<?php echo base_url() ?>assets/template_front/img/bg/3.png">
                    </div>
                    <div class="details">
                        <h4>Personal accident & permanent disability compensation</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp'); height: 90%">
                    <div class="icon-box">
                        <img src="<?php echo base_url() ?>assets/template_front/img/bg/4.png">
                    </div>
                    <div class="details">
                        <h4>Departure & baggage delays for a minimum of 4 hours</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp'); height: 90%">
                    <div class="icon-box">
                        <img src="<?php echo base_url() ?>assets/template_front/img/bg/5.png">
                    </div>
                    <div class="details">
                        <h4>24-hour Emergency Assistance</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp'); height: 90%">
                    <div class="icon-box">
                        <img src="<?php echo base_url() ?>assets/template_front/img/bg/6.png">
                    </div>
                    <div class="details">
                        <h4>Baggage damage & loss protection</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="team-area bg-blue pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">Travel Insurance</h5>
                    <h2 class="title">Product Characteristics</h2>
                    <p class="content">
                    </p>
                </div>
                <div class="col-lg-6  offset-lg-3">
                    <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation" style="border-bottom: 2px solid white;">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home"
                                    type="button" role="tab" aria-controls="pills-home" aria-selected="true"
                                    style="background-color: transparent;">
                                Insured
                            </button>
                        </li>
                        <li class="nav-item" role="presentation" style="border-bottom: 2px solid white;">
                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile"
                                    type="button" role="tab" aria-controls="pills-profile" aria-selected="false"
                                    style="background-color: transparent;">
                                Coverage period
                            </button>
                        </li>
                        <li class="nav-item" role="presentation" style="border-bottom: 2px solid white;">
                            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact"
                                    type="button" role="tab" aria-controls="pills-contact" aria-selected="false"
                                    style="background-color: transparent;">
                                Coverage benefits
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content text-white" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                            <p class="content text-white">All customers who live and work in Indonesia can purchase TravelPro products, with the following ages:</p>
                            <p class="content text-white">Children aged 14 days - 17 years (before 18 years).</p>
                            <p class="content text-white">Adults aged 18 - 69 years (before the age of 70 years). </p>
                            <p class="content text-white">Elderly age 70 years - 84 years (before age 85 years).</p>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0">
                            <p class="content text-white">The maximum length of the trip is 183 days.</p>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab" tabindex="0">
                            <p class="content text-white">Providing comprehensive and affordable protection as well as extra protection against epidemics/pandemic for
                                international travel for you, and a flexible choice of plans according to your needs
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="faq-area pd-top-100 pd-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
<!--                <div class="section-title mb-0 text-center">-->
<!--                    <h5 class="sub-title double-line">International Healthcare Insurance</h5>-->
<!--                    <h2 class="title">Product Details</h2>-->
<!--                </div>-->
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                Medical & Medical Related Expenses
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="mt-4">Overseas Medical Expenses</h5>
                                <p>We will pay you for necessary and reasonable medical expenses incurred or paid due to serious injury or illness
                                    including due to a virus that has been declared as an Epidemic or Pandemic that you suffer during your trip,
                                    as long as it is not excluded in your policy, up to the maximum as listed in the table of benefits that you choose.
                                </p>
                                <p>Benefits for ages before 70 years and after 70 years may be different, please refer to the benefits table listed on the brochure.</p>
                                <h5 class="mt-4">Expenses Related to Emergency Medical Overseas</h5>
                                <h5>Emergency Medical Evacuation and Repatriation</h5>
                                <p>If the result of an injury or illness occurs while you are on your trip and if based on Allianz's opinion, it is
                                    deemed medically appropriate to transfer you to another location for medical treatment or return you to
                                    Indonesia, we will arrange for evacuation using the method we think is most appropriate. according to
                                    the severity of your medical condition.
                                </p>
                                <h5 class="mt-4">Accompanying Person</h5>
                                <p>If you are hospitalized or die abroad and/or at the destination of your trip due to a serious illness or injury and no close adult family member
                                    accompanies you on the trip, we will pay a reasonable fee for transportation, accommodation or cost of changing the trip for the companion, with further conditions as explained in the Policy, up to the maximum as stated in the table of benefits you choose.
                                </p>
                                <h5 class="mt-4">Child protection</h5>
                                <p>In the case of traveling with children and you must be cared for during the trip and there are no other adults accompanying your child, we will pay a reasonable
                                    fee for transportation and accommodation for close family members to care for and accompany your child back, with further conditions as stated described in the Policy, up to the maximum as stated in the table of benefits you choose.
                                </p>
                                <h5 class="mt-4">Cash Benefit for Daily Hospitalization in Overseas Hospitals</h5>
                                <p>If a medical practitioner requires you to stay in the hospital as an inpatient due to an injury or illness that first arises on an overseas trip, we will pay for every 24 (twenty four) hours of continuous hospitalization
                                    for the hospital, with more provisions continue as described in the Policy, up to the maximum as stated in the table of benefits you choose.
                                </p>
                                <h5 class="mt-4"><b>Cash Benefit for Daily Inpatient Hospital in Indonesia</b></h5>
                                <p>This coverage is applied in Indonesia, provided that you must be hospitalized within 12 (twelve) hours of your arrival back in Indonesia, and you can only submit a claim
                                    for one time hospitalization, with further conditions such as described in the Policy, up to the maximum as stated in the table of benefits you choose.
                                </p>
                                <h5 class="mt-4">Emergency Telephone and Internet Usage Fees</h5>
                                <p>We will reimburse you for the telephone costs incurred and paid for by you for the use of personal or third party cellphones
                                    and the use of the internet or telephone using a standard LAN connection, only to obtain the services of the Assistance Service Company in an Emergency Medical condition, with further conditions such as described in the Policy, up to the maximum as stated in the table of benefits you choose.
                                </p>
                                <h5 class="mt-4">Cost of Follow-Up Treatment in Indonesia</h5>
                                <p>We will reimburse you for reasonable and necessary Medical Expenses that you incur and pay for further treatment
                                    in Indonesia for your Serious Illness or Injury, and for the first time receiving treatment under the TravelPro International Insurance - Enhanced Policy when traveling abroad .
                                </p>
                                <p>This guarantee must be provided within 30 (thirty) days from the date of your return to Indonesia.
                                    Further provisions as explained in the Policy, up to the maximum as stated in the benefits table you choose.
                                </p>
                                <h5 class="mt-4"><b>Expenses for Emergency Dental Treatment Abroad</b></h5>
                                <p>This coverage provides reimbursement to you for all reasonable Overseas Emergency Dental Treatment Expenses and needs to be incurred
                                    and paid for Injury to teeth that is natural and reasonable as a result of an Accident that occurs during Your Trip. The amount paid is part of the Medical Expenses coverage limit.
                                </p>
                                <h5 class="mt-4">Repatriation of Bodies or Funeral Expenses</h5>
                                <p>If you die suddenly as a result of a serious illness or injury while you are traveling, we will pay the reasonable and necessary costs incurred to return your body
                                    to Indonesia for burial or cremation at the location of your death overseas up to the amount we will pay. pay if your body will be returned to Indonesia, with further conditions as explained in the Policy, up to the maximum as stated in the table of benefits you choose.
                                </p>
                                <h5 class="mt-4">Automatic Policy Extension</h5>
                                <p>We will automatically extend the insurance period while you are abroad, based on this policy without additional premium:</p>
                                <p>For 10 (ten) consecutive days if a situation occurs beyond your control; and you are not the cause of the delay; or</p>
                                <p>For 30 (thirty) consecutive days if you are hospitalized or quarantined abroad and the reasons for hospitalization or quarantine are covered in this Policy; or</p>
                                <p>For 48 (forty eight) consecutive days if you are unfit to travel by air.</p>
                                <p>It is a condition of the TravelPro International Insurance - Enhanced Policy that you must make every effort to return
                                    home at the first available opportunity. Further provisions for this section as explained in the Policy.
                                </p>
                                <h5 class="mt-4">Worldwide 24 hour Emergency Assistance</h5>
                                <p>We work with various partner companies to provide you with 24-hour medical emergency assistance.
                                    This service includes medical advice, referrals from doctors, specialists, hospitals, lawyers, translators, and others as further explained in the Policy.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                Travel Inconvenience
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="mt-4">Early return</h5>
                                <p>If after starting your trip, you reduce your travel period and return home earlier than the previously planned
                                    return date as a result of reasons covered in the TravelPRO International Insurance - Enhanced Policy, we will pay the costs incurred as a result of this return if you do not can obtain these fees from
                                    other parties, up to the maximum as stated in the table of benefits that you choose. This benefit also guarantees Epidemic/Pandemic coverage with further conditions as explained in the Policy.
                                </p>
                                <h5 class="mt-4">Trip Interruption and Loss of Onward Transportation</h5>
                                <h5 class="mt-4"><b>Travel Disruption</b></h5>
                                <p>If you travel overseas, your trip is unexpectedly interrupted for more than 24 (twenty four) consecutive hours due
                                    to events covered in the TravelPRO International Insurance - Enhanced Policy, we will pay a reasonable additional
                                    fee for transportation, accommodation , parking renewal fees and other costs as explained in the Policy up to the maximum as stated in the benefits table you choose. In the event that you are in quarantine during your trip or the destination of your trip because you are exposed to an infectious disease other than an epidemic/pandemic, and are exposed to an epidemic/pandemic, we will pay additional accommodation per day up to the maximum as stated in the table of benefits you choose.
                                </p>
                                <h5 class="mt-4">Lost Connection</h5>
                                <p>If during the trip, you are abandoned by the scheduled connecting public transportation and cannot arrive at the destination as scheduled due to unforeseen matters as described in the TravelPRO Policy, then we will pay the necessary and reasonable
                                    costs incurred so that it is possible for you to use alternative public transportation to arrive on time at your destination. The minimum distance between your arrival time and the departure time of your onward journey is as specified on your Certificate of Insurance. For the TravelPro Policy, the minimum distance between the arrival and departure times of connecting flights is at least 6 hours and only applies to different airlines.
                                </p>
                                <p>The compensation that we will provide is up to the maximum as stated in the benefits table that you choose.</p>
                                <h5 class="mt-4">Flight Delay</h5>
                                <p>In the event that the Covered Transportation that has been ordered and paid for which You have arranged for the trip, is delayed from the departure or arrival time specified in the itinerary provided to You by the Covered Transportation provider, and as a direct result of one or more events- event guaranteed in the TravelPro Policy, we will pay an amount for every 4 (four) hours of the delay period,
                                    up to the maximum as stated in the Benefit Table that you choose.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                Lost Goods and Baggage
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="mt-4">Loss of Personal Baggage</h5>
                                <p>If the personal belongings that you carry or you buy during the trip experience loss and damage arising from:</p>
                                <p>theft or plunder or robbery; or</p>
                                <p>when under the care and supervision of the Covered Transportation Equipment or accommodation Service Providers that have been paid for.</p>
                                <p>At our own discretion, we will have the authority to replace or repair the item according to the conditions of the Policy.
                                    Further provisions as explained in the Policy and Our obligations are up to the maximum as stated in the Benefit Table that You choose.
                                </p>
                                <h5 class="mt-4">Baggage Delay</h5>
                                <p>If during your trip, you experience baggage delay by the airline for a minimum delay period as stated in the Table of Benefits on your Insurance Certificate after your arrival at the destination of your trip. For the TravelPro International Insurance - Enhanced Policy, the baggage delay period is 4 consecutive hours – along after your arrival at the destination. Further provisions as explained in the Policy and Our obligations are up to the maximum as stated in the Benefit Table that You choose.</p>
                                <h5 class="mt-4">Credit Card Abuse</h5>
                                <p>Benefits provided when the credit card is stolen by someone other than your relative or travel companion and you are legally responsible for payments arising from unauthorized use of your credit card which occurs 1x24 hours before you inform / report to the issuing bank to block credit card, <br>
                                    Further provisions as explained in the Policy and Our obligations are up to the maximum as stated in the Benefit Table that You choose
                                </p>
                                <h5 class="mt-4">Lost Travel Documents</h5>
                                <p>If your Passport, Visa, ID Card or Entrance Permit Card is stolen or damaged during travel, we will pay reasonable and necessary fees for transportation, accommodation and replacement costs, and what you actually paid to secure your Passport, Visa or relevant emergency travel documents to enable you to continue your journey or return home. Further provisions as explained in the Policy and Our obligations are up to the maximum as stated in the Benefit Table that You choose.</p>
                                <h5 class="mt-4">Personal Money Theft</h5>
                                <p>If personal money (limited to cash, bank notes, traveler's checks and money orders only) that you take with you on your trip, is lost during your trip as a direct result of theft or robbery. We will pay you up to the sum insured guaranteed in the TravelPro International Insurance - Enhanced Policy.</p>
                                <p>Further provisions as explained in the Policy and Our obligations are up to the maximum as stated in the Benefit Table that You choose</p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-4">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-4" aria-expanded="true" aria-controls="benefit-4">
                                Death and Permanent Disability
                            </button>
                        </h2>
                        <div id="benefit-4" class="accordion-collapse collapse" aria-labelledby="judul-4"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="mt-4">Death Due to Accident Age of the Insured 14 Days - 17 Years</h5>
                                <p>We will provide compensation for the death of the insured due to an accident suffered during your trip.</p>
                                <h5 class="mt-4">Permanent Disability Age of the Insured 14 Days - 17 Years</h5>
                                <p>We will provide compensation due to an accident suffered during your trip that causes permanent disability guaranteed in the TravelPRO International Insurance - Enhanced Policy.</p>
                                <h5 class="mt-4">Death or Permanent Disability Insured Age 17 - 69 Years</h5>
                                <p>We will provide compensation due to an accident suffered during your trip that causes death or permanent disability guaranteed in the TravelPRO International Insurance - Enhanced Policy.</p>
                                <h5 class="mt-4">Death or Permanent Disability Age of the Insured 70 - 84 Years</h5>
                                <p>We will provide compensation due to an accident suffered during your trip that causes death or permanent disability guaranteed in the TravelPRO International Insurance - Enhanced Policy.</p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-5">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-5" aria-expanded="true" aria-controls="benefit-5">
                                Legal Responsibility & Cost at Own Risk
                            </button>
                        </h2>
                        <div id="benefit-5" class="accordion-collapse collapse" aria-labelledby="judul-5"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="mt-4">Personal Responsibility</h5>
                                <p>We will make payments for your Benefits according to the Table of Benefits above:</p>
                                <p>Compensation for losses that are legally your responsibility due to an Overseas Trip that causes someone to be injured or dies, loses or damages someone's belongings.</p>
                                <p>All your reasonable legal fees and expenses to settle and defend Claims brought against you which you have paid</p>
                                <p>Further provisions as explained in the Policy and Our obligations are up to the maximum as stated in the Benefit Table that You choose.</p>
                                <h5 class="mt-4">Own Risk Fees for Cars Rented and Return Fees for Vehicles Rented</h5>
                                <p>We will make payments for the cost of the rented vehicle with the conditions, the vehicle rented must be from an official rental agent, you are registered as a driver, the vehicle rented is only for carrying passengers without paying, you have purchased motor vehicle insurance with a comprehensive guarantee. Further provisions as explained in the Policy and Our obligations are up to the maximum as stated in the Benefit Table that You choose.</p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-6">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-6" aria-expanded="true" aria-controls="benefit-6">
                                Term & Condition
                            </button>
                        </h2>
                        <div id="benefit-6" class="accordion-collapse collapse" aria-labelledby="judul-6"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="mt-4"></h5>
                                <p>If you change your travel plans, you can contact AllianzCare 1500136 or Travel@allianz.co.id, before your travel date</p>
                                <p>All terms and conditions for travel insurance policies are available in the TravelPRO Policy. You can download and print the link below for more detailed information about TravelPRO insurance products.</p>
                                <h6>
                                    <a href="<?php echo base_url() ?>assets/template_front/content/New-TravelPro_Policy-Wording.pdf" target="_blank"><i class='far fa-file-pdf'></i> DOWNLOAD POLIS TRAVELPRO INTERNATIONAL INSURANCE-ENHANCED</a>
                                </h6>
                                <h6>
                                    <a href="<?php echo base_url()?>assets/template_front/content/Travel-International-Policy.pdf" target="_blank"><i class='far fa-file-pdf'></i> DOWNLOAD POLIS TRAVELPRO INTERNATIONAL WITH ENHANCED BENEFIT</a>
                                </h6>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="how-it-work-area bg-blue pd-top-110 pd-bottom-110">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">Travel Asurance</h5>
                    <h2 class="title">Policy Type</h2>
                    <p class="content">
                        Determine the type of policy according to your travel needs.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>01</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Individual</h4>
                            <p>A policy that will only cover the insured.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>02</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Couple</h4>
                            <p>This policy will cover two people traveling together for the entire trip on the same itinerary.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>03</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Family</h4>
                            <p>This policy will cover the Insured, spouse and children traveling on the same itinerary. The amount of benefits will be divided proportionally according to the number of insureds.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="team-area info-box-two pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-9">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line">Travel Asurance</h5>
                    <h2 class="title">Coverage Area</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center coverage-area">
            <div class="col-lg-6 col-md-6">
                <div class="single-contact-inner text-center">
                    <div class="row">
                        <div class="icon-box col-lg-6 col-md-6">
                            <img src="<?php echo base_url()?>assets/template_front/img/travel-asurance/4.jpeg">
                        </div>
                        <div class="details-wrap col-lg-6 col-md-6">
                            <div class="details-inner" style="text-align: left;">
                                <h3>WORLDWIDE</h3>
                                <p>All countries in the world, except: Afghanistan, Rep. Democratic Congo, Cuba, Iran, Iraq, Liberia, Sudan, Syria, Ukraine and the Crimera Area of Ukraine.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-contact-inner text-center">
                    <div class="row">
                        <div class="icon-box col-lg-6 col-md-6">
                            <img src="<?php echo base_url()?>assets/template_front/img/travel-asurance/1.jpeg">
                        </div>
                        <div class="details-wrap col-lg-6 col-md-6">
                            <div class="details-inner" style="text-align: left;">
                                <h3>Europe & Pacific</h3>
                                <p>Australia, New Zealand and all countries in the Schengen area (Austria, Belgium, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Iceland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Slovakia, Slovenia, Spain, Sweden, Switzerland, Liechtenstein).</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-contact-inner text-center">
                    <div class="row">
                        <div class="icon-box col-lg-6 col-md-6">
                            <img src="<?php echo base_url()?>assets/template_front/img/travel-asurance/2.jpeg">
                        </div>
                        <div class="details-wrap col-lg-6 col-md-6">
                            <div class="details-inner" style="text-align: left;">
                                <h3>Asia</h3>
                                <p>Bangladesh, Bhutan, Brunei, Burma, Cambodia, China, East Timor, India, Japan, Laos, Malaysia, Maldives, Mongolia, Nepal, North Korea, Pakistan, Philippines, Singapore, South Korea, Sri Lanka, Taiwan, Thailand, Vietnam, Hong Kong, Macau.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-contact-inner text-center">
                    <div class="row">
                        <div class="icon-box col-lg-6 col-md-6">
                            <img src="<?php echo base_url()?>assets/template_front/img/travel-asurance/3.jpeg">
                        </div>
                        <div class="details-wrap col-lg-6 col-md-6">
                            <div class="details-inner" style="text-align: left;">
                                <h3>Schengen Basic</h3>
                                <p>Austria, Belgium, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Iceland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Slovakia, Slovenia, Spain, Sweden, Switzerland , Liechtenstein.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>