<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $top_quote->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blog-area pd-top-60 pd-bottom-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="blog-details-page-content">
                    <div class="single-blog-inner">
                        <div class="details">
                            <ul class="blog-meta">
                                <li><i class="far fa-user"></i> <?= $dict_admin ?></li>
                                <li><i class="far fa-calendar-alt"></i> <?php echo date('d F Y') ?></li>
                                <li><i class="far fa-eye"></i> <?php echo $page->views ?> Views</li>
                            </ul>
                            <?php echo $page->description ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-12">
                <form class="form-send email-send" target="_blank">
                    <div class="td-sidebar">
                        <div class="widget widget_search">
                            <h4 class="widget-title"><?= $dict_perconal_detail ?></h4>
                            <div class="row g-3">
                                <div class="col-md-6">
                                    <label for="input-name" class="form-label"><?= $dict_full_name ?></label>
                                    <input type="text" class="form-control" id="input-name" name="name" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-birth" class="form-label">Date of Birth</label>
                                    <input type="date" class="form-control" id="input-birth" name="birthdate" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-gender" class="form-label">Gender</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender" id="input-man"
                                               required value="Man"
                                               checked>
                                        <label class="form-check-label" for="input-man">
                                            Man
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gender" id="input-woman" value="Woman"
                                               required>
                                        <label class="form-check-label" for="input-woman">
                                            Women
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-marital" class="form-label">Marital Status</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="marital" id="input-single"
                                               value="Single" checked>
                                        <label class="form-check-label" for="input-single">
                                            Single
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="marital" value="Married" id="input-married">
                                        <label class="form-check-label" for="input-married">
                                            Married
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-country" class="form-label">Country</label>
                                    <select class="form-control select2" id="input-country" name="country" required>
                                        <?php require("components/country_list.php") ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-email" class="form-label">Email</label>
                                    <input type="text" class="form-control" id="input-email" name="email" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-phone" class="form-label">Telephone</label>
                                    <input type="text" class="form-control" id="input-phone" name="phone" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-whatsapp" class="form-label">WhatsApp</label>
                                    <input type="text" class="form-control" id="input-whatsapp" name="whatsapp" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-product" class="form-label">Product Insurance</label>
                                    <select class="form-control" id="input-product" name="product" required>
                                        <option>International Healthcare</option>
                                        <option>Income Protection</option>
                                        <option>Property Insurance</option>
                                        <option>Car Insurance</option>
                                        <option>Travel Insurance</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-pesan" class="form-label">Message or Question</label>
                                    <textarea class="form-control" id="input-pesan" name="pesan" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="widget widget_search">
                            <div class="row g-3 pd-bottom-60">
                                <h4 class="widget-title"><?= $dict_add_person ?></h4>
                                <p>Note : <i><?= $dict_quote_note ?></i></p>
                                <div class="col-md-12">
                                    <label for="input-email" class="form-label">Add People to Insure</label>
                                    <select class="form-control" name="person_add" required>
                                        <option value="0">- Total People -</option>
                                        <?php for ($i = 1; $i <= 10; $i++) { ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="person-list-wrapper"></div>
                        </div>
                        <div class="widget widget_search">
                            <div class="row g-3">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary"><?= $dict_get_quote ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="person-add-wrapper d-none">
    <div class="row g-3 pd-bottom-60">
        <div class="col-md-12">
            <h4 class="widget-title">Person <span class="person-number"></span></h4>
        </div>
        <div class="col-md-6">
            <label for="input-name" class="form-label">Full Name</label>
            <input type="text" class="form-control" id="input-name" name="person_name[]" required>
        </div>
        <div class="col-md-6">
            <label for="input-birth" class="form-label">Date of Birth</label>
            <input type="date" class="form-control" id="input-birth" name="person_birthdate[]" required>
        </div>
        <div class="col-md-6">
            <label for="input-email" class="form-label">Family Member</label>
            <select name="person_member[]" class="form-control" required>
                <option value="">- Select -</option>
                <option value="Husband">Husband</option>
                <option value="Wife">Wife</option>
                <option value="Son">Son</option>
                <option value="Daughter">Daughter</option>
                <option value="Father">Father</option>
                <option value="Mother">Mother</option>
                <option value="Brother">Brother</option>
                <option value="Sister">Sister</option>
                <option value="Grand Father">Grand Father</option>
                <option value="Grand Mother">Grand Mother</option>
                <option value="Others">Others</option>
            </select>
        </div>
        <div class="col-md-6">
            <label for="input-gender" class="form-label">Gender</label>
            <select name="person_gender[]" class="form-control" required>
                <option value="">- Select -</option>
                <option value="Man">Man</option>
                <option value="Women">Woman</option>
            </select>
        </div>
    </div>
</div>