<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $page->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-area pd-top-90 pd-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="mask-bg-wrap mask-bg-img-3">
                    <div class="thumb">
                        <img src="<?php echo base_url() ?>assets/template_front/img/about/3.webp" alt="img">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="section-title px-lg-5 mb-0">
                    <h5 class="sub-title left-border">About The Insurance</h5>
                    <p class="content-strong mt-2 mb-4">
                        Enjoy Peace Of Mind By Protecting Your
                        Home & Its Contents
                    </p>
                    <h5 class="sub-title left-border">Key Features</h5>
                    <p class="content-strong mt-2 mb-4">Comprehensive shelter guarantees for shelter, more than
                        imagined</p>
                    <div class="list-wrap mt-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Fire</li>
                                    <li>Property Damage</li>
                                    <li>Terrorists and Sabotage</li>
                                    <li>Legal Liability</li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Temporary Accommodation</li>
                                    <li>Compensation for the Death of the Insured</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team-area bg-blue pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">Property Insurance</h5>
                    <h2 class="title">Allianz Property Insurance</h2>
                    <p class="content">
                        Property is one of the most important assets for our progression. With various unforeseen risks
                        such as natural disasters, robbery, vandalism, and so on, it can result in fatal losses and make
                        the loss of valuable assets. Allianz Bali provides a variety of property insurance products
                        ranging from building insurance and property insurance
                    </p>
                    <p class="content">
                        With a wide selection of the best products, Allianz Propety Insurance is able to provide a sense
                        of security and comfort from the risks that can occur in properties and such as houses, villas,
                        hotels, and resorts.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 offset-lg-2 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>01</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Houseowner</h4>
                            <p>RumahKu Plus Houseowner provides protection for buildings, including walls, roofs,
                                floors, garages, and fences for your home. Policyholders can choose the type of RumahKu
                                Plus Houseowner Insurance only, or RumahKu Plus Householder according to the needs of
                                the policyholder.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>02</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Householder</h4>
                            <p>RumahKu Plus Householder provides protection for the contents of the building residence,
                                including electronic equipment and furniture in your home. You can choose the type of
                                RumahKu Plus Householder Insurance only, or RumahKu Plus Houseowner according to your
                                needs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="faq-area pd-top-60 pd-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title mb-0 text-center">
                    <h5 class="sub-title double-line">Property Protection</h5>
                    <h2 class="title">Product Details</h2>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                Building Specifications
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>The type of residence that can be insured with Allianz Rumahku Plus is a
                                        residence that is not more than three floors with a class 1 building
                                        construction (concrete or steel construction). Not rented, not empty, and the
                                        road can be traversed by a fire engine.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                General Exceptions
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>War, rebellion, riot, revolution, taking power.</li>
                                    <li>Confiscation, retrieval &amp; damage due to order from a legitimate
                                        government.
                                    </li>
                                    <li>Nuclear weapons, radiation &amp; radioactive pollution.</li>
                                    <li>Fur coats, platinum, gold, and silver, jewelery, paintings, and art objects with
                                        a value not exceeding 5% of the sum insured max. IDR 50,000,000 (whichever is
                                        smaller).
                                    </li>
                                </ul>
                                <p>You can see the full exceptions in the Policy.</p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                Claim
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <p>Complete the documents, get the claim.</p>
                                <ul>
                                    <li>Pay attention to the claim deadline stated on the policy, it could be 7, 14, or
                                        30 days, according to the policy provisions.
                                    </li>
                                    <li>Report to Allianz in writing (verbal and followed by a written report).</li>
                                    <li>Send the required claim supporting documents.</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="faq-area pd-top-100 pd-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line">Property Protection</h5>
                    <h2 class="title">FAQ</h2>
                    <h3 class="title">Questions Regarding Allianz RumahKu Plus</h3>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                What types of buildings does Allianz cover?
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Residential houses are not more than 3 floors and do not include shop houses.
                                        Class I (concrete, steel, not including buildings with wooden construction).
                                        Flood-free housing / settlement location. Minimum premium according to the
                                        standard (minimum IDR 100,000). Covering the building (houseowner) and the
                                        contents of the house (householder).
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                What is the premium rate?
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>0.1295% for building and house contents.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                What types of protection are provided?
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>FLTSEA (Fire, Linghthing, Thunderbolt, Subterranean Fire, Explosion, Faling
                                        Aircraft)
                                    </li>
                                    <li>Riot, Strikes, Evil Deeds</li>
                                    <li>Riot, Terrorism, Sabotage</li>
                                    <li>Collision with vehicles or others</li>
                                    <li>Rupture or overflow of device tanks or household plumbing</li>
                                    <li>Theft</li>
                                    <li>Hurricane, Cyclone, Cyclone, Hurricane</li>
                                    <li>Earthquakes, Volcanic Eruptions, Tsunamis</li>
                                    <li>Floods (including overflowing sea water)</li>
                                    <li>Property damage (Accidental Damage)</li>
                                    <li>Morror Damage</li>
                                    <li>Public Liabilitty (Bright body and property)</li>
                                    <li>Accidental Death</li>
                                    <li>Servants Property</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-relative pd-top-60 pd-bottom-60">
    <img class="shape-left-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/3.webp"
         alt="img">
    <img class="shape-right-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/4.webp"
         alt="img">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line">Property Insurance</h5>
                    <h2 class="title">Enjoy Peace Of Mind By Protecting Your Home & Its Contents</h2>
                </div>
            </div>
        </div>
    </div>
</div>