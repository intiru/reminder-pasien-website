<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $top_contact_us->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="team-area info-box-two pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-9">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line"><?php echo $page->title ?></h5>
                    <h2 class="title"><?php echo $page->title_sub ?></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <h4 class="title mt-4">1. General</h4>
                <div class="content">
                    <p>By accessing this website (the "Allianz Website") and using its content, you acknowledge and you agree that you have read and understood the following terms of use and you agree to be bound by them. Do not use the Allianz Website, if you do not agree with these terms of use. As used below, the terms “Allianz”, “we”, “us”, and “our” refer to Allianz Indonesia.</p>
                </div>
                <h4 class="title mt-4">2. Use of Content</h4>
                <div class="content">
                    <p>The content is available for information only. The posting of content and access to this website do not render, either explicitly or implicitly, any provision of services or products by us. Information concerning general or life insurance or services is only available through the respective affiliate company that carries on such business.</p>
                </div>
                <h4 class="title mt-4">3. No Offer</h4>
                <div class="content">
                    <p>No information on the Allianz Website is or should be interpreted as an offer or a solicitation for an offer, as investment, legal, tax or other advice. Where such is needed, an independent professional should be consulted.</p>
                </div>
                <h4 class="title mt-4">4. No Duty To Update</h4>
                <div class="content">
                    <p>All content on the Allianz Website is published as of its date only. We assume no responsibility to update or amend.</p>
                </div>
                <h4 class="title mt-4">5. Copyright</h4>
                <div class="content">
                    <p>All content of the Allianz Website is protected by copyright with all rights reserved. All rights in the pages, site content and arrangement are owned by Allianz and/or the respective Allianz Group of Companies. You are prohibited from copying, modifying, displaying, distributing, transmitting, redelivering through the use of "framing" technology, publishing, selling, licensing, creating derivative works or using any site content for any purpose without the prior written approval of Allianz and/or the respective Allianz Group of Companies.</p>
                </div>
                <h4 class="title mt-4">6. Trademaarks, Service Marks and Logos ("Marks")</h4>
                <div class="content">
                    <p>The marks appearing on the Allianz Website are the property of Allianz and/or the respective Allianz Group of Companies.</p>
                </div>
                <h4 class="title mt-4">7. No Warranty</h4>
                <div class="content">
                    <p>All content on the Allianz Website, including but not limited to graphics, text and hyperlinks or references to other sites, is provided on an "as is" basis without warranty of any kind, express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement and freedom from computer viruses or other harmful components.</p>
                    <p>We do not warrant the adequacy, accuracy, reliability or completeness of any information on the Allianz Website and expressly disclaim any liability for errors or omissions therein.</p>
                    <p>We do not warrant that the functions of the Allianz Website will be uninterrupted and/or error-free, that defects will be corrected or that the Allianz Website or the server that makes it available are free from computer viruses or other harmful components.</p>
                </div>
                <h4 class="title mt-4">8. Limotation of Liability</h4>
                <div class="content">
                    <p>We expressly disclaim any liability, whether in contract, tort, strict liability or otherwise, for any direct, indirect, incidental, consequential, punitive or special damages arising out of or in any way connected with your access or use or inability to access or use the Allianz Website or reliance on its content, or any failure of performance, interruption, defect, delay in transmission, computer viruses or other harmful components, or line or system failure associated with the Allianz Website, regardless of our knowledge thereof.</p>
                </div>
                <h4 class="title mt-4">9. Hyperlinked and Referenced Websites</h4>
                <div class="content">
                    <p>Certain hyperlinks or referenced websites on the Allianz Website may for your convenience forward you to third parties’ websites, which generally are recognized by their top level domain name. Their content has not been investigated or analysed by us, and we do not warrant the adequacy, accuracy, reliability or completeness of any information on hyperlinked or referenced websites. We further do not warrant that the hyperlinks or referenced websites are free from viruses and expressly disclaim any liability for any and all of their content and damages resulting from its use.</p>
                    <p>By accessing the Allianz Website, you also agree to abide by the proprietary guidelines set forth at any website accessed or hyperlinked to through the Allianz Website.</p>
                </div>
                <h4 class="title mt-4">10. Local Legal Restrictions</h4>
                <div class="content">
                    <p>The Allianz Website is not directed to any person in any jurisdiction where (by reason of that person's nationality, residence or otherwise) the publication or availability of the Allianz Website is prohibited. Persons in respect of whom such prohibitions apply must not access the Allianz Website.</p>
                </div>
                <h4 class="title mt-4">11. Reservation of Rights</h4>
                <div class="content">
                    <p>We reserve the right to replace, change, modify, add to, or remove portions of these terms of use at any time.</p>
                </div>
                <h4 class="title mt-4">12. Jurisdiction, Severability</h4>
                <div class="content">
                    <p>Any action arising out of these terms or this website shall be litigated in, and only in, courts located in Indonesia, and you agree to submit to the exclusive jurisdiction of those courts and further agree that they are a convenient forum for you.</p>
                    <p>In the event that any of these terms is held unenforceable, the validity or enforceability of the remaining terms will not be affected, and the unenforceable terms will be replaced with enforceable terms that come close to the intention underlying the unenforceable terms.</p>
                </div>
            </div>
        </div>
    </div>
</div>
