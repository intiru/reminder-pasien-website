<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?= $page->title ?><img src="<?= base_url('assets/template_front/images/logos2.png') ?>" alt="Simbol Logo Rumah Sunat Bali"></h1>
                </div>
            </div>
        </div>
        <div class="space"></div>
        <div class="descripsi-banner text-center">
            <?= $page->description ?>
        </div>
    </div>
</section>
<div id="portfolio" class="container">
    <div class="row">
        <div id="container" class="large-12 columns transitions-enabled large-centered clearfix">

            <?php foreach ($testimonial_list as $row) { ?>
                <a class="box col2 fancybox-thumb" rel="fancybox-thumb" href="<?= $this->main->image_preview_url($row->thumbnail) ?>">
                    <img src="<?= $this->main->image_preview_url($row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                </a>
            <?php } ?>

        </div>
    </div>
</div>