<div class="page__banner" data-background="<?php echo base_url() ?>assets/template_front/img/page-banner.png">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page__banner-content">
                    <h1><?php echo $page->title ?></h1>
                    <div class="page__banner-menu">
                        <ul>
                            <li><a href="<?php echo site_url() ?>"><i
                                            class="flaticon-home-icon-silhouette"></i> <?php echo $top_home->title_menu ?>
                                    - </a>
                            </li>
                            <li> <?php echo $page->title_menu ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services__area section-padding" data-background="assets/img/shape.png"
     style="background-image: url(<?php echo base_url() ?>assets/template_front/img/shape.png);">
    <div class="container">
        <div class="row align-items-center mb-30">
            <div class="col-xl-12 col-lg-12">
                <div class="about__area-right">
                    <div class="about__area-right-title">
                        <p class="section-top"><?php echo $page->title_sub ?></p>
                        <h2><?php echo $page->title ?></h2>
                        <?php echo $page->description ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($portfolio_list as $row) { ?>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                    <div class="services__area-item">
                        <h3><?php echo $row->title ?></h3>
                        <?php echo $row->description ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>