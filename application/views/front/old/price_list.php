<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?= $page->title ?><img src="<?= base_url('assets/template_front/images/logos2.png') ?>"
                                                alt="Simbol Logo Rumah Sunat Bali"></h1>
                </div>
            </div>
        </div>
        <div class="space"></div>
        <div class="descripsi-banner text-center">
            <?= $page->description ?>
        </div>
    </div>
</section>
<div class="container">
    <div class="row justify-content-center metode-biaya-wrapper">
        <?php foreach ($category_price_list as $row) { ?>
            <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="1s">
                <div class="service-item">
                    <a href="<?php echo $whatsapp_link . '&text=' . urlencode('Pesan Paket Sunat ' . $row->title) ?>"
                       target="_blank">
                        <img class="layanan-img" src="<?= $this->main->image_preview_url($row->thumbnail) ?>"
                             alt="<?php echo $row->thumbnail_alt ?>">
                    </a>
                    <div class="space"></div>
                    <h2><?= $row->title ?></h2>
                    <?= $row->description ?>
                    <a href="<?php echo $whatsapp_link . '&text=' . urlencode('Pesan Paket Sunat ' . $row->title) ?>"
                       target="_blank"><img
                                src="<?= base_url('assets/template_front/images/icons/pesan-paket-sunat-via-whatsapp.png') ?>"
                                alt="Pesan Paket Sunat <?php echo $row->title ?> via WhatsApp"></a>
                </div>
                <div class="space"></div>
            </div>
        <?php } ?>
    </div>
</div>