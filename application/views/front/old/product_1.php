<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title">International Healthcare Insurance</h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $page->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-area pd-top-90 pd-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="mask-bg-wrap mask-bg-img-3">
                    <div class="thumb">
                        <img src="<?php echo base_url() ?>assets/template_front/img/about/3.webp" alt="img">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="section-title px-lg-5 mb-0">
                    <h5 class="sub-title left-border">About The Insurance</h5>
                    <p class="content-strong mt-2 mb-4">
                        With 100,000 unique customers in 182 different countries, Allianz Insurance are
                        the experts in delivering international medical insurance to expats worldwide.
                    </p>
                    <h5 class="sub-title left-border">Key Features</h5>
                    <p class="content-strong mt-2 mb-4">of our International Healthcare Policies</p>
                    <div class="list-wrap mt-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Fully cover</li>
                                    <li>Cashless claims</li>
                                    <li>Up to 99 years cover</li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Private room facility</li>
                                    <li>Cover organ transplant & critical illness treatment</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team-area bg-blue pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">About the Plans</h5>
                    <h2 class="title">Hospital & Surgical Care Premier Plus</h2>
                    <p class="content">
                        Allianz presents Hospital & Surgical Care Premier Plus,
                        additional Individual Health Insurance that provides a variety of plus benefits for you and the whole family.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="faq-area pd-top-100 pd-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title mb-0 text-center">
                    <h5 class="sub-title double-line">International Healthcare Insurance</h5>
                    <h2 class="title">Benefit Table</h2>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                Indonesia
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr class="row-1 odd">
                                        <th class="column-1">BENEFIT</th>
                                        <th class="column-2">DESCRIPTION</th>
                                        <th class="column-3">BASIC</th>
                                        <th class="column-4">BASIC PLUS</th>
                                        <th class="column-5">CLASSIC</th>
                                        <th class="column-6">CLASSIC PLUS</th>
                                    </tr>
                                    </thead>
                                    <tbody class="row-hover">
                                    <tr class="row-2 even">
                                        <td class="column-1"><b>Area of Coverage</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"><b>Indonesia</b></td>
                                        <td class="column-4"><b>Indonesia</b></td>
                                        <td class="column-5"><b>Indonesia</b></td>
                                        <td class="column-6"><b>Indonesia</b></td>
                                    </tr>
                                    <tr class="row-3 odd">
                                        <td class="column-1">Prorated Factor for Payment of Inpatient Care Benefits
                                            outside the coverage area</td>
                                        <td class="column-2">Indonesia</td>
                                        <td class="column-3">100% covered</td>
                                        <td class="column-4">100% covered</td>
                                        <td class="column-5">100% covered</td>
                                        <td class="column-6">100% covered</td>
                                    </tr>
                                    <tr class="row-4 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Asia excluding Singapore. Hong Kong. Japan.</td>
                                        <td class="column-3">60% covered</td>
                                        <td class="column-4">60% covered</td>
                                        <td class="column-5">60% covered</td>
                                        <td class="column-6">60% covered</td>
                                    </tr>
                                    </tr>
                                    <tr class="row-5 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Singapore, Hong Kong, Japan</td>
                                        <td class="column-3">20% covered</td>
                                        <td class="column-4">20% covered</td>
                                        <td class="column-5">20% covered</td>
                                        <td class="column-6">20% covered</td>
                                    </tr>
                                    <tr class="row-6 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Worldwide excluding USA, Asia</td>
                                        <td class="column-3">NIA</td>
                                        <td class="column-4">NIA</td>
                                        <td class="column-5">NIA</td>
                                        <td class="column-6">NIA</td>
                                    </tr>
                                    <tr class="row-7 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">USA</td>
                                        <td class="column-3">NIA</td>
                                        <td class="column-4">NIA</td>
                                        <td class="column-5">NIA</td>
                                        <td class="column-6">NIA</td>
                                    </tr>
                                    <tr class="row-8 odd">
                                        <td class="column-1"><b>Hospitalization & Surgery Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                        <td class="column-5"></td>
                                        <td class="column-6"></td>
                                    </tr>
                                    <tr class="row-9 odd">
                                        <td class="column-1">Rooms and Accommodation</td>
                                        <td class="column-2">No maximum day limit</td>
                                        <td class="column-3">NIA</td>
                                        <td class="column-4">2 bedded with
                                            attached bathroom
                                            or R&B that
                                            doesn't exceed</td>
                                        <td class="column-5">2 bedded with
                                            attached bathroom
                                            or R&B that doesn't
                                            exceed</td>
                                        <td class="column-6">1 bedded with
                                            attached bathroom or
                                            R&B that doesn't
                                            exceed</td>
                                    </tr>
                                    <tr class="row-10 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Room Rate Limit</td>
                                        <td class="column-3">500</td>
                                        <td class="column-4">700</td>
                                        <td class="column-5">700</td>
                                        <td class="column-6">1.300</td>
                                    </tr>
                                    <tr class="row-11 odd">
                                        <td class="column-1">ICU/ NICU/ PICU/ HDU/ Intermediary Ward/ Isolation Room</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-12 odd">
                                        <td class="column-1">Surgery, including Daily Surgical Care</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-13 odd">
                                        <td class="column-1">Prostheses and Implants</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-14 odd">
                                        <td class="column-1">Doctor's Visit</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">30.000</td>
                                        <td class="column-4">40.000</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-15 odd">
                                        <td class="column-1">Miscellaneous Expense</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">30.000</td>
                                        <td class="column-4">40.000</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-16 odd">
                                        <td class="column-1">Pre-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization</td>
                                        <td class="column-3">30.000</td>
                                        <td class="column-4">40.000</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-17 odd">
                                        <td class="column-1">Post-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">30.000</td>
                                        <td class="column-4">40.000</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-18 odd">
                                        <td class="column-1">Outpatient Physiotherapy Treatment*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">30.000</td>
                                        <td class="column-4">40.000</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-19 odd">
                                        <td class="column-1">Alternative Inpatient Care*</td>
                                        <td class="column-2">Per policy year;</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">100.000</td>
                                        <td class="column-6">100.000</td>
                                    </tr>
                                    <tr class="row-20 odd">
                                        <td class="column-1">Rehabilitation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">15.000</td>
                                        <td class="column-6">15.000</td>
                                    </tr>
                                    <tr class="row-21 odd">
                                        <td class="column-1">Traditional Chinese Medicine</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">Overall 15,000 per year;
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                        <td class="column-6">Overall 15,000 per year;
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                    </tr>
                                    <tr class="row-22 odd">
                                        <td class="column-1">Outpatient Psychiatric Consultation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">15.000</td>
                                        <td class="column-6">15.000</td>
                                    </tr>
                                    <tr class="row-23 odd">
                                        <td class="column-1">Companion Benefit</td>
                                        <td class="column-2">Per day</td>
                                        <td class="column-3">250</td>
                                        <td class="column-4">350</td>
                                        <td class="column-5">350</td>
                                        <td class="column-6">650</td>
                                    </tr>
                                    <tr class="row-24 odd">
                                        <td class="column-1">Alternative Daily Cash*</td>
                                        <td class="column-2">Per day; max. 90 days per policy year</td>
                                        <td class="column-3">250</td>
                                        <td class="column-4">350</td>
                                        <td class="column-5">350</td>
                                        <td class="column-6">650</td>
                                    </tr>
                                    <tr class="row-25 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-26 odd">
                                        <td class="column-1"><b>High Profile Critical illness Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                        <td class="column-5"></td>
                                        <td class="column-6"></td>
                                    </tr>
                                    <tr class="row-27 odd">
                                        <td class="column-1">Dialysis Treatment</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-28 odd">
                                        <td class="column-1">Organ Transplant Cost</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-29 odd">
                                        <td class="column-1">Donor Expenses For Organ Transplant*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-30 odd">
                                        <td class="column-1">Cancer Treatment, including:
                                            Cancer remission examination & laboratory tests</td>
                                        <td class="column-2">Max. 5 yearsfrom last treatment</td>
                                        <td class="column-3">As Charged, max 80%
                                            of the billing cost</td>
                                        <td class="column-4">As Charged, max 80%
                                            of the billing cost</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-31 odd">
                                        <td class="column-1">HIV/AIDS Treatment</td>
                                        <td class="column-2">Per year</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">15.000</td>
                                        <td class="column-6">15.000</td>
                                    </tr>
                                    <tr class="row-32 odd">
                                        <td class="column-1">Palliative Care</td>
                                        <td class="column-2">Per year policy</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">250.000</td>
                                        <td class="column-6">250.000</td>
                                    </tr>
                                    <tr class="row-33 odd">
                                        <td class="column-1"><b>Emergency Treatment Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                        <td class="column-5"></td>
                                        <td class="column-6"></td>
                                    </tr>
                                    <tr class="row-34 odd">
                                        <td class="column-1">Emergency & Accidental IP treatment outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-35 odd">
                                        <td class="column-1">Emergency & Accidental OP treatment including Dental, inside and outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-36 odd">
                                        <td class="column-1">Continued outpatient treatment for accidental injury*</td>
                                        <td class="column-2">Undergoing outpatient treatment within 30 days from
                                            the time of accident or other emergency conditions.</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-37 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                        <td class="column-5">As Charged</td>
                                        <td class="column-6">As Charged</td>
                                    </tr>
                                    <tr class="row-38 odd">
                                        <td class="column-1"><b>Additional Special Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                        <td class="column-5"></td>
                                        <td class="column-6"></td>
                                    </tr>
                                    <tr class="row-39 odd">
                                        <td class="column-1">Durable medical equipment</td>
                                        <td class="column-2">Per Year Policy;
                                            Max 90 days post-hospitalization/surgery</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">15.000</td>
                                        <td class="column-6">15.000</td>
                                    </tr>
                                    <tr class="row-40 odd">
                                        <td class="column-1">External artificial body part</td>
                                        <td class="column-2">Per Year Policy; during hospitalization, max 90 days
                                            after hospitalization/surgery</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                        <td class="column-5">250.000</td>
                                        <td class="column-6">250.000</td>
                                    </tr>
                                    <tr class="row-41 odd">
                                        <td class="column-1">Funeral Expense*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">25.000</td>
                                        <td class="column-4">25.000</td>
                                        <td class="column-5">25.000</td>
                                        <td class="column-6">25.000</td>
                                    </tr>
                                    <tr class="row-42 odd">
                                        <td class="column-1"><b>Service</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                        <td class="column-5"></td>
                                        <td class="column-6"></td>
                                    </tr>
                                    <tr class="row-43 odd">
                                        <td class="column-1">Expert Medical Opinion</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                        <td class="column-4">Available</td>
                                        <td class="column-5">Available</td>
                                        <td class="column-6">Available</td>
                                    </tr>
                                    <tr class="row-44 odd">
                                        <td class="column-1">Medical Assisstance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                        <td class="column-4">Available</td>
                                        <td class="column-5">Available</td>
                                        <td class="column-6">Available</td>
                                    </tr>
                                    <tr class="row-45 odd">
                                        <td class="column-1">Annual Benefit Limit</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">1 .000.000</td>
                                        <td class="column-4">2.500.000</td>
                                        <td class="column-5">5.000.000</td>
                                        <td class="column-6">5.000.000</td>
                                    </tr>
                                    <tr class="row-46 odd">
                                        <td class="column-1" style="font: small-caption; color: red;">*Claims for Insurance Benefits can only be made on a reimbursement</td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                        <td class="column-5"></td>
                                        <td class="column-6"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                Asia Exclude SG , HK & JPN
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table id="tablepress-2" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr class="row-1 odd">
                                        <th class="column-1">BENEFIT</th>
                                        <th class="column-2">DESCRIPTION</th>
                                        <th class="column-3">ESSENTIAL</th>
                                        <th class="column-4">ESSENTIAL PLUS</th>
                                    </tr>
                                    </thead>
                                    <tbody class="row-hover">
                                    <tr class="row-2 even">
                                        <td class="column-1"><b>Area of Coverage</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"><b>Asia, Excluding HKG, SC, JPN</b></td>
                                        <td class="column-4"><b>Asia, Excluding HKG, SC, JPN</b></td>
                                    </tr>
                                    <tr class="row-3 odd">
                                        <td class="column-1">Prorated Factor for Payment of Inpatient Care
                                            Benefits outside the coverage area</td>
                                        <td class="column-2">Indonesia</td>
                                        <td class="column-3">100% covered</td>
                                        <td class="column-4">100% covered</td>
                                    </tr>
                                    <tr class="row-4 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Asia excluding Singapore, Hong Kong, Japan.</td>
                                        <td class="column-3">100% covered</td>
                                        <td class="column-4">100% covered</td>
                                    </tr>
                                    <tr class="row-5 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Singapore, Hong Kong, Japan</td>
                                        <td class="column-3">30% covered</td>
                                        <td class="column-4">30% covered</td>
                                    </tr>
                                    <tr class="row-6 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Worldwide excluding USA, Asia</td>
                                        <td class="column-3">20% covered</td>
                                        <td class="column-4">20% covered</td>
                                    </tr>
                                    <tr class="row-7 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">USA</td>
                                        <td class="column-3">N/A</td>
                                        <td class="column-4">N/A</td>
                                    </tr>
                                    <tr class="row-8 odd">
                                        <td class="column-1"><b>Hospitalization & Surgery Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-9 odd">
                                        <td class="column-1">Rooms and Accommodation</td>
                                        <td class="column-2">No maximum day limit</td>
                                        <td class="column-3">2 bedded with
                                            attached bathroom or
                                            R&B that doesn't
                                            exceed</td>
                                        <td class="column-4">1 bedded with
                                            attached bathroom or
                                            R&B that doesn't
                                            exceed</td>
                                    </tr>
                                    <tr class="row-10 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Room Rate Limit</td>
                                        <td class="column-3">700</td>
                                        <td class="column-4">1.300</td>
                                    </tr>
                                    <tr class="row-11 odd">
                                        <td class="column-1">ICU/ NICU/ PICU/ HDU/ Intermediary Ward/ Isolation Room</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-12 odd">
                                        <td class="column-1">Surgery, including Daily Surgical Care</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-13 odd">
                                        <td class="column-1">Prostheses and Implants</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-14 odd">
                                        <td class="column-1">Doctor's Visit</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-15 odd">
                                        <td class="column-1">Miscellaneous Expense</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-16 odd">
                                        <td class="column-1">Pre-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-17 odd">
                                        <td class="column-1">Post-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-18 odd">
                                        <td class="column-1">Outpatient Physiotherapy Treatment*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-19 odd">
                                        <td class="column-1">Alternative Inpatient Care*</td>
                                        <td class="column-2">Per policy year;</td>
                                        <td class="column-3">200.000</td>
                                        <td class="column-4">200.000</td>
                                    </tr>
                                    <tr class="row-20 odd">
                                        <td class="column-1">Rehabilitation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">15.000</td>
                                        <td class="column-4">15.000</td>
                                    </tr>
                                    <tr class="row-21 odd">
                                        <td class="column-1">Traditional Chinese Medicine</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">Overall 15,000 per year;<br>
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                        <td class="column-4">Overall 15,000 per year;<br>
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                    </tr>
                                    <tr class="row-22 odd">
                                        <td class="column-1">Outpatient Psychiatric Consultation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">15.000</td>
                                        <td class="column-4">15.000</td>
                                    </tr>
                                    <tr class="row-23 odd">
                                        <td class="column-1">Companion Benefit</td>
                                        <td class="column-2">Per day</td>
                                        <td class="column-3">350</td>
                                        <td class="column-4">350</td>
                                    </tr>
                                    <tr class="row-24 odd">
                                        <td class="column-1">Alternative Daily Cash*</td>
                                        <td class="column-2">Per day; max. 90 days per policy year</td>
                                        <td class="column-3">350</td>
                                        <td class="column-4">350</td>
                                    </tr>
                                    <tr class="row-25 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-26 odd">
                                        <td class="column-1"><b>High Profile Critical illness Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-27 odd">
                                        <td class="column-1">Dialysis Treatment</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-28 odd">
                                        <td class="column-1">Organ Transplant Cost</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-29 odd">
                                        <td class="column-1">Donor Expenses For Organ Transplant*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-30 odd">
                                        <td class="column-1">Cancer Treatment, including:
                                            Cancer remission examination &laboratory tests</td>
                                        <td class="column-2">Max. 5 yearsfrom last treatment</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-31 odd">
                                        <td class="column-1">HIV/AIDS Treatment</td>
                                        <td class="column-2">Per year</td>
                                        <td class="column-3">15.000</td>
                                        <td class="column-4">15.000</td>
                                    </tr>
                                    <tr class="row-32 odd">
                                        <td class="column-1">Palliative Care</td>
                                        <td class="column-2">Per year policy</td>
                                        <td class="column-3">250.000</td>
                                        <td class="column-4">250.000</td>
                                    </tr>
                                    <tr class="row-33 odd">
                                        <td class="column-1"><b>Emergency Treatment Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-34 odd">
                                        <td class="column-1">Emergency & Accidental IP treatment outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-35 odd">
                                        <td class="column-1">Emergency & Accidental OP treatment including Dental, inside and outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-36 odd">
                                        <td class="column-1">Continued outpatienttreatment for accidental
                                            injury*</td>
                                        <td class="column-2">Undergoing outpatient treatment within 30 daysfrom
                                            the time of accident or other emergency conditions.</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-37 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-38 odd">
                                        <td class="column-1"><b>Additional Special Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-39 odd">
                                        <td class="column-1">Durable medical equipment</td>
                                        <td class="column-2">Per Year Policy;<br>
                                            Max 90 days post-hospitalization/surgery</td>
                                        <td class="column-3">15.000</td>
                                        <td class="column-4">15.000</td>
                                    </tr>
                                    <tr class="row-40 odd">
                                        <td class="column-1">External artificial body part</td>
                                        <td class="column-2">Per Year Policy; during hospitalization, max 90 days
                                            after hospitalization/surgery</td>
                                        <td class="column-3">250.000</td>
                                        <td class="column-4">250.000</td>
                                    </tr>
                                    <tr class="row-41 odd">
                                        <td class="column-1">Funeral Expense*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">25 000</td>
                                        <td class="column-4">25 000</td>
                                    </tr>
                                    <tr class="row-42 odd">
                                        <td class="column-1"><b>Service</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-43 odd">
                                        <td class="column-1">Expert Medical Opinion</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                        <td class="column-4">Available</td>
                                    </tr>
                                    <tr class="row-44 odd">
                                        <td class="column-1">Medical Assisstance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                        <td class="column-4">Available</td>
                                    </tr>
                                    <tr class="row-45 odd">
                                        <td class="column-1"><b>Annual Benefit Limit</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3">7.000.000</td>
                                        <td class="column-4">7.000.000</td>
                                    </tr>
                                    <tr class="row-46 odd">
                                        <td class="column-1" style="font: small-caption; color: red;">*Claims for Insurance Benefits can only be made on a reimbursement</td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                All Asia
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table id="tablepress-3" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr class="row-1 odd">
                                        <th class="column-1">BENEFIT</th>
                                        <th class="column-2">DESCRIPTION</th>
                                        <th class="column-3">ELITE</th>
                                        <th class="column-4">ELITE PLUS</th>
                                    </tr>
                                    </thead>
                                    <tbody class="row-hover">
                                    <tr class="row-2 even">
                                        <td class="column-1"><b>Coverage Area</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"><b>Asia</b></td>
                                        <td class="column-4"><b>Asia</b></td>
                                    </tr>
                                    <tr class="row-3 odd">
                                        <td class="column-1">Prorated Factor for Payment of Inpatient Care
                                            Benefits outside the coverage area</td>
                                        <td class="column-2">Indonesia</td>
                                        <td class="column-3">100% covered</td>
                                        <td class="column-4">100% covered</td>
                                    </tr>
                                    <tr class="row-4 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Asia excluding Singapore, Hong Kong, Japan.</td>
                                        <td class="column-3">100% covered</td>
                                        <td class="column-4">100% covered</td>
                                    </tr>
                                    <tr class="row-5 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Singapore, Hong Kong, Japan</td>
                                        <td class="column-3">100% covered</td>
                                        <td class="column-4">100% covered</td>
                                    </tr>
                                    <tr class="row-6 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Worldwide excluding USA, Asia</td>
                                        <td class="column-3">60% covered</td>
                                        <td class="column-4">60% covered</td>
                                    </tr>
                                    <tr class="row-7 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">USA</td>
                                        <td class="column-3">30% covered</td>
                                        <td class="column-4">30% covered</td>
                                    </tr>
                                    <tr class="row-8 odd">
                                        <td class="column-1"><b>Hospitalization & Surgery Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-9 odd">
                                        <td class="column-1">Rooms and Accommodation</td>
                                        <td class="column-2">No maximum day limit</td>
                                        <td class="column-3">2 bedded with
                                            attached bathroom or
                                            R&B that doesn't
                                            exceed</td>
                                        <td class="column-4">1 bedded with
                                            attached bathroom or
                                            R&B that doesn't
                                            exceed</td>
                                    </tr>
                                    <tr class="row-10 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Room Rate Limit</td>
                                        <td class="column-3">1.100</td>
                                        <td class="column-4">1.650</td>
                                    </tr>
                                    <tr class="row-11 odd">
                                        <td class="column-1">ICU/ NICU/ PICU/ HDU/ Intermediary Ward/ Isolation Room</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-12 odd">
                                        <td class="column-1">Surgery, including Daily Surgical Care</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-13 odd">
                                        <td class="column-1">Prostheses and Implants</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-14 odd">
                                        <td class="column-1">Doctor's Visit</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-15 odd">
                                        <td class="column-1">Miscellaneous Expense</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-16 odd">
                                        <td class="column-1">Pre-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-17 odd">
                                        <td class="column-1">Post-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-18 odd">
                                        <td class="column-1">Outpatient Physiotherapy Treatment*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-19 odd">
                                        <td class="column-1">Alternative Inpatient Care*</td>
                                        <td class="column-2">Per policy year;</td>
                                        <td class="column-3">300.000</td>
                                        <td class="column-4">300.000</td>
                                    </tr>
                                    <tr class="row-20 odd">
                                        <td class="column-1">Rehabilitation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">25.000</td>
                                        <td class="column-4">25.000</td>
                                    </tr>
                                    <tr class="row-21 odd">
                                        <td class="column-1">Traditional Chinese Medicine</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">Overall 25,000 per year;<br>
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                        <td class="column-4">Overall 25,000 per year;<br>
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                    </tr>
                                    <tr class="row-22 odd">
                                        <td class="column-1">Outpatient Psychiatric Consultation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">25.000</td>
                                        <td class="column-4">25.000</td>
                                    </tr>
                                    <tr class="row-23 odd">
                                        <td class="column-1">Companion Benefit</td>
                                        <td class="column-2">Per day</td>
                                        <td class="column-3">550</td>
                                        <td class="column-4">850</td>
                                    </tr>
                                    <tr class="row-24 odd">
                                        <td class="column-1">Alternative Daily Cash*</td>
                                        <td class="column-2">Per day; max. 90 days per policy year</td>
                                        <td class="column-3">550</td>
                                        <td class="column-4">850</td>
                                    </tr>
                                    <tr class="row-25 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-26 odd">
                                        <td class="column-1"><b>High Profile Critical illness Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-27 odd">
                                        <td class="column-1">Dialysis Treatment</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-28 odd">
                                        <td class="column-1">Organ Transplant Cost</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-29 odd">
                                        <td class="column-1">Donor Expenses For Organ Transplant*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-30 odd">
                                        <td class="column-1">Cancer Treatment, including:
                                            Cancer remission examination & laboratory tests</td>
                                        <td class="column-2">Max. 5 years from last treatment</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-31 odd">
                                        <td class="column-1">HIV/AIDS Treatment</td>
                                        <td class="column-2">Per year</td>
                                        <td class="column-3">15.000</td>
                                        <td class="column-4">15.000</td>
                                    </tr>
                                    <tr class="row-32 odd">
                                        <td class="column-1">Palliative Care</td>
                                        <td class="column-2">Per year policy</td>
                                        <td class="column-3">250.000</td>
                                        <td class="column-4">250.000</td>
                                    </tr>
                                    <tr class="row-33 odd">
                                        <td class="column-1"><b>Emergency Treatment Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-34 odd">
                                        <td class="column-1">Emergency & Accidental IP treatment outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-35 odd">
                                        <td class="column-1">Emergency & Accidental OP treatment including Dental, inside and outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-36 odd">
                                        <td class="column-1">Continued outpatienttreatment for accidental
                                            injury*</td>
                                        <td class="column-2">Undergoing outpatient treatment within 30 days from
                                            the time of accident or other emergency conditions.</td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-37 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                        <td class="column-4">As Charged</td>
                                    </tr>
                                    <tr class="row-38 odd">
                                        <td class="column-1"><b>Additional Special Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-39 odd">
                                        <td class="column-1">Durable medical equipment</td>
                                        <td class="column-2">Per Year Policy;<br>
                                            Max 90 days post-hospitalization/surgery</td>
                                        <td class="column-3">15.000</td>
                                        <td class="column-4">15.000</td>
                                    </tr>
                                    <tr class="row-40 odd">
                                        <td class="column-1">External artificial body part</td>
                                        <td class="column-2">Per Year Policy; during hospitalization, max 90 days
                                            after hospitalization/surgery</td>
                                        <td class="column-3">250.000</td>
                                        <td class="column-4">250.000</td>
                                    </tr>
                                    <tr class="row-41 odd">
                                        <td class="column-1">Funeral Expense*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">25 000</td>
                                        <td class="column-4">25 000</td>
                                    </tr>
                                    <tr class="row-42 odd">
                                        <td class="column-1"><b>Service</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    <tr class="row-43 odd">
                                        <td class="column-1">Expert Medical Opinion</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                        <td class="column-4">Available</td>
                                    </tr>
                                    <tr class="row-44 odd">
                                        <td class="column-1">Medical Assisstance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                        <td class="column-4">Available</td>
                                    </tr>
                                    <tr class="row-45 odd">
                                        <td class="column-1"><b>Annual Benefit Limit</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3">10.000.000</td>
                                        <td class="column-4">10.000.000</td>
                                    </tr>
                                    <tr class="row-46 odd">
                                        <td class="column-1" style="font: small-caption; color: red;">*Claims for Insurance Benefits can only be made on a reimbursement</td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                        <td class="column-4"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-4">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-4" aria-expanded="true" aria-controls="benefit-4">
                                Worldwide Excluding USA
                            </button>
                        </h2>
                        <div id="benefit-4" class="accordion-collapse collapse" aria-labelledby="judul-4"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table id="tablepress-4" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr class="row-1 odd">
                                        <th class="column-1"><b>BENEFIT</b></th>
                                        <th class="column-2"><b>DESCRIPTION</b></th>
                                        <th class="column-3"><b>PRIME</b></th>
                                    </tr>
                                    </thead>
                                    <tbody class="row-hover">
                                    <tr class="row-2 even">
                                        <td class="column-1"><b>Coverage Area</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Worldwide Excluding USA</td>
                                    </tr>
                                    <tr class="row-3 odd">
                                        <td class="column-1">Prorated Factor for Payment of Inpatient Care
                                            Benefits outside the coverage area</td>
                                        <td class="column-2">Indonesia</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-4 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Asia excluding Singapore, Hong Kong, Japan.</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-5 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Singapore, Hong Kong, Japan</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-6 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Worldwide excluding USA, Asia</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-7 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">USA</td>
                                        <td class="column-3">60% covered</td>
                                    </tr>
                                    <tr class="row-8 odd">
                                        <td class="column-1"><b>Hospitalization & Surgery Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-9 odd">
                                        <td class="column-1">Rooms and Accommodation</td>
                                        <td class="column-2">No maximum day limit</td>
                                        <td class="column-3">Which is bigger between
                                            Room 1
                                            level above lowest room
                                            with 1
                                            bed and bathroom inside
                                            with Room Rate Limit</td>
                                    </tr>
                                    <tr class="row-10 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Room Rate Limit</td>
                                        <td class="column-3">3.000</td>
                                    </tr>
                                    <tr class="row-11 odd">
                                        <td class="column-1">ICU/ NICU/ PICU/ HDU/ Intermediary Ward/ Isolation Room</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-12 odd">
                                        <td class="column-1">Surgery, including Daily Surgical Care</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-13 odd">
                                        <td class="column-1">Prostheses and Implants</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-14 odd">
                                        <td class="column-1">Doctor's Visit</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-15 odd">
                                        <td class="column-1">Miscellaneous Expense</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-16 odd">
                                        <td class="column-1">Pre-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-17 odd">
                                        <td class="column-1">Post-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-18 odd">
                                        <td class="column-1">Outpatient Physiotherapy Treatment*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-19 odd">
                                        <td class="column-1">Alternative Inpatient Care*</td>
                                        <td class="column-2">Per policy year;</td>
                                        <td class="column-3">500.000</td>
                                    </tr>
                                    <tr class="row-20 odd">
                                        <td class="column-1">Rehabilitation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">25.000</td>
                                    </tr>
                                    <tr class="row-21 odd">
                                        <td class="column-1">Traditional Chinese Medicine</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">Overall 25,000 per year;
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                    </tr>
                                    <tr class="row-22 odd">
                                        <td class="column-1">Outpatient Psychiatric Consultation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">25.000</td>
                                    </tr>
                                    <tr class="row-23 odd">
                                        <td class="column-1">Companion Benefit</td>
                                        <td class="column-2">Per day</td>
                                        <td class="column-3">1.500</td>
                                    </tr>
                                    <tr class="row-24 odd">
                                        <td class="column-1">Alternative Daily Cash*</td>
                                        <td class="column-2">Per day; max. 90 days per policy year</td>
                                        <td class="column-3">1.500</td>
                                    </tr>
                                    <tr class="row-25 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-26 odd">
                                        <td class="column-1"><b>High Profile Critical illness Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-27 odd">
                                        <td class="column-1">Dialysis Treatment</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-28 odd">
                                        <td class="column-1">Organ Transplant Cost</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-29 odd">
                                        <td class="column-1">Donor Expenses For Organ Transplant*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-30 odd">
                                        <td class="column-1">Cancer Treatment, including:
                                            Cancer remission examination & laboratory tests</td>
                                        <td class="column-2">Max. 5 years from last treatment</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-31 odd">
                                        <td class="column-1">HIV/AIDS Treatment</td>
                                        <td class="column-2">Per year</td>
                                        <td class="column-3">15.000</td>
                                    </tr>
                                    <tr class="row-32 odd">
                                        <td class="column-1">Palliative Care</td>
                                        <td class="column-2">Per year policy</td>
                                        <td class="column-3">250.000</td>
                                    </tr>
                                    <tr class="row-33 odd">
                                        <td class="column-1"><b>Emergency Treatment Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-34 odd">
                                        <td class="column-1">Emergency& Accidental IP treatment outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-35 odd">
                                        <td class="column-1">Emergency & Accidental OP treatment including Dental, inside and outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-36 odd">
                                        <td class="column-1">Continued outpatient treatment for accidental
                                            injury*</td>
                                        <td class="column-2">Undergoing outpatient treatment within 30 days from
                                            the time of accident or other emergency conditions.</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-37 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-38 odd">
                                        <td class="column-1"><b>Additional Special Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-39 odd">
                                        <td class="column-1">Durable medical equipment</td>
                                        <td class="column-2">Per Year Policy;<br>
                                            Max 90 days post-hospitalization/surgery</td>
                                        <td class="column-3">15.000</td>
                                    </tr>
                                    <tr class="row-40 odd">
                                        <td class="column-1">External artificial body part</td>
                                        <td class="column-2">Per Year Policy; during hospitalization, max 90 days
                                            after hospitalization/surgery</td>
                                        <td class="column-3">250.000</td>
                                    </tr>
                                    <tr class="row-41 odd">
                                        <td class="column-1">Funeral Expense*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">25 000</td>
                                    </tr>
                                    <tr class="row-42 odd">
                                        <td class="column-1"><b>Service</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-43 odd">
                                        <td class="column-1">Expert Medical Opinion</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                    </tr>
                                    <tr class="row-44 odd">
                                        <td class="column-1">Medical Assisstance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                    </tr>
                                    <tr class="row-45 odd">
                                        <td class="column-1"><b>Annual Benefit Limit</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3">20.000.000</td>
                                    </tr>
                                    <tr class="row-46 odd">
                                        <td class="column-1" style="font: small-caption; color: red;">*Claims for Insurance Benefits can only be made on a reimbursement</td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-5">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-5" aria-expanded="true" aria-controls="benefit-5">
                                Worldwide
                            </button>
                        </h2>
                        <div id="benefit-5" class="accordion-collapse collapse" aria-labelledby="judul-5"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table id="tablepress-5" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr class="row-1 odd">
                                        <th class="column-1"><b>BENEFIT</b></th>
                                        <th class="column-2"><b>DESCRIPTION</b></th>
                                        <th class="column-3"><b>SIGNATURE</b></th>
                                    </tr>
                                    </thead>
                                    <tbody class="row-hover">
                                    <tr class="row-2 even">
                                        <td class="column-1"><b>Coverage Area</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3">WORLDWIDE</td>
                                    </tr>
                                    <tr class="row-3 odd">
                                        <td class="column-1">Prorated Factor for Payment of Inpatient Care
                                            Benefits outside the coverage area</td>
                                        <td class="column-2">Indonesia</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-4 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Asia excluding Singapore, Hong Kong, Japan.</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-5 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Singapore, Hong Kong, Japan</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-6 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Worldwide excluding USA, Asia</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-7 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">USA</td>
                                        <td class="column-3">100% covered</td>
                                    </tr>
                                    <tr class="row-8 odd">
                                        <td class="column-1"><b>Hospitalization & Surgery Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-9 odd">
                                        <td class="column-1">Rooms and Accommodation</td>
                                        <td class="column-2">No maximum day limit</td>
                                        <td class="column-3">Which is bigger between
                                            Room 1 level above The
                                            lowest room with 1 bed
                                            and bathroom inside with
                                            the Room Price Limit</td>
                                    </tr>
                                    <tr class="row-10 odd">
                                        <td class="column-1"></td>
                                        <td class="column-2">Room Rate Limit</td>
                                        <td class="column-3">8.000</td>
                                    </tr>
                                    <tr class="row-11 odd">
                                        <td class="column-1">ICU/ NICU/ PICU/ HDU/ Intermediary Ward/ Isolation Room</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-12 odd">
                                        <td class="column-1">Surgery, including Daily Surgical Care</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-13 odd">
                                        <td class="column-1">Prostheses and Implants</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-14 odd">
                                        <td class="column-1">Doctor's Visit</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-15 odd">
                                        <td class="column-1">Miscellaneous Expense</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-16 odd">
                                        <td class="column-1">Pre-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-17 odd">
                                        <td class="column-1">Post-Hospitalization Expenses*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-18 odd">
                                        <td class="column-1">Outpatient Physiotherapy Treatment*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 60 days before hospitalization<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-19 odd">
                                        <td class="column-1">Alternative Inpatient Care*</td>
                                        <td class="column-2">Per policy year;</td>
                                        <td class="column-3">500.000</td>
                                    </tr>
                                    <tr class="row-20 odd">
                                        <td class="column-1">Rehabilitation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">50.000</td>
                                    </tr>
                                    <tr class="row-21 odd">
                                        <td class="column-1">Traditional Chinese Medicine</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">Overall 50,000 per year;
                                            1 ,000 per hospitalization
                                            for medication.</td>
                                    </tr>
                                    <tr class="row-22 odd">
                                        <td class="column-1">Outpatient Psychiatric Consultation*</td>
                                        <td class="column-2">Per policy year;<br>
                                            Max. 90 days after hospitalization</td>
                                        <td class="column-3">50.000</td>
                                    </tr>
                                    <tr class="row-23 odd">
                                        <td class="column-1">Companion Benefit</td>
                                        <td class="column-2">Per day</td>
                                        <td class="column-3">4.000</td>
                                    </tr>
                                    <tr class="row-24 odd">
                                        <td class="column-1">Alternative Daily Cash*</td>
                                        <td class="column-2">Per day; max. 90 days per policy year</td>
                                        <td class="column-3">4.000</td>
                                    </tr>
                                    <tr class="row-25 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-26 odd">
                                        <td class="column-1"><b>High Profile Critical illness Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-27 odd">
                                        <td class="column-1">Dialysis Treatment</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-28 odd">
                                        <td class="column-1">Organ Transplant Cost</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-29 odd">
                                        <td class="column-1">Donor Expenses For Organ Transplant*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-30 odd">
                                        <td class="column-1">Cancer Treatment, including:
                                            Cancer remission examination & laboratory tests</td>
                                        <td class="column-2">Max. 5 years from last treatment</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-31 odd">
                                        <td class="column-1">HIV/AIDS Treatment</td>
                                        <td class="column-2">Per year</td>
                                        <td class="column-3">15.000</td>
                                    </tr>
                                    <tr class="row-32 odd">
                                        <td class="column-1">Palliative Care</td>
                                        <td class="column-2">Per year policy</td>
                                        <td class="column-3">250.000</td>
                                    </tr>
                                    <tr class="row-33 odd">
                                        <td class="column-1"><b>Emergency Treatment Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-34 odd">
                                        <td class="column-1">Emergency & Accidental IP treatment outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-35 odd">
                                        <td class="column-1">Emergency & Accidental OP treatment including Dental, inside and outside coverage area</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-36 odd">
                                        <td class="column-1">Continued outpatient treatment for accidental
                                            injury*</td>
                                        <td class="column-2">Undergoing outpatient treatment within 30 days from
                                            the time of accident or other emergency conditions.</td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-37 odd">
                                        <td class="column-1">Local Ambulance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">As Charged</td>
                                    </tr>
                                    <tr class="row-38 odd">
                                        <td class="column-1"><b>Additional Special Benefits</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-39 odd">
                                        <td class="column-1">Durable medical equipment</td>
                                        <td class="column-2">Per Year Policy;<br>
                                            Max 90 days post-hospitalization/surgery</td>
                                        <td class="column-3">15.000</td>
                                    </tr>
                                    <tr class="row-40 odd">
                                        <td class="column-1">External artificial body part</td>
                                        <td class="column-2">Per Year Policy; during hospitalization, max 90 days
                                            after hospitalization/surgery</td>
                                        <td class="column-3">250.000</td>
                                    </tr>
                                    <tr class="row-41 odd">
                                        <td class="column-1">Funeral Expense*</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">25 000</td>
                                    </tr>
                                    <tr class="row-42 odd">
                                        <td class="column-1"><b>Service</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    <tr class="row-43 odd">
                                        <td class="column-1">Expert Medical Opinion</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                    </tr>
                                    <tr class="row-44 odd">
                                        <td class="column-1">Medical Assisstance</td>
                                        <td class="column-2"></td>
                                        <td class="column-3">Available</td>
                                    </tr>
                                    <tr class="row-45 odd">
                                        <td class="column-1"><b>Annual Benefit Limit</b></td>
                                        <td class="column-2"></td>
                                        <td class="column-3">25.000.000</td>
                                    </tr>
                                    <tr class="row-46 odd">
                                        <td class="column-1" style="font: small-caption; color: red;">*Claims for Insurance Benefits can only be made on a reimbursement</td>
                                        <td class="column-2"></td>
                                        <td class="column-3"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="how-it-work-area bg-blue pd-top-110 pd-bottom-110">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">International Healthcare Insurance</h5>
                    <h2 class="title">What are the advantages Premier Plus Hospital & Surgical Care?</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>
                                <img src="<?php echo base_url() ?>assets/template_front/img/bg/7.png" alt="img" style="max-width: 40px;">
                            </h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Plus Options</h4>
                            <p>Plan options, Coverage Area & Treatment Room that can be selected according to your needs.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>
                                <img src="<?php echo base_url() ?>assets/template_front/img/bg/8.png" alt="img" style="max-width: 45px;">
                            </h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Plus Benefits</h4>
                            <p>Complete your protection with a comprehensive range of optional benefits.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>
                                <img src="<?php echo base_url() ?>assets/template_front/img/bg/9.png" alt="img" style="max-width: 40px;">
                            </h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Plus Service</h4>
                            <p>A wide range of practical services to help meet your needs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="faq-area pd-top-100 pd-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title mb-0 text-center">
                    <h5 class="sub-title double-line">International Healthcare Insurance</h5>
                    <h3 class="title"></h3>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                + Options
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6">
                                                <div class="section-title style-white text-center">
                                                    <h3 class="title" style="color: #000000;">+ Options</h3>
                                                    <p class="content" style="color: #000000;">Area of coverage:<br>
                                                        Indonesia | Asia | Worldwide<br>

                                                        Various Treatment Room Options:
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-right: -7vw;">
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Basic </h5>
                                                            <p>Indonesia</p><br><br>
                                                            <p>Rp500.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Basic Plus</h5>
                                                            <p>Indonesia</p><br><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> <i class='fas fa-bed fa-sm' style='color:#000000'></i> | Rp700.000 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Classic </h5>
                                                            <p>Indonesia</p><br><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> <i class='fas fa-bed fa-sm' style='color:#000000'></i> | Rp700.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Classic Plus</h5>
                                                            <p>Indonesia</p><br><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> | Rp1.300.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Essential</h5>
                                                            <p>Asia, Exclude HKG, SG, JPN</p><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> <i class='fas fa-bed fa-sm' style='color:#000000'></i> | Rp700.000 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 0.5vw; margin-right: -7vw;">
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Essential Plus </h5>
                                                            <p>Asia, Exclude HKG, SG, JPN</p><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> | Rp1.300.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Elite</h5>
                                                            <p>Asia</p><br><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> <i class='fas fa-bed fa-sm' style='color:#000000'></i> | Rp1.100.000 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Elite Plus</h5>
                                                            <p>Asia</p><br><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> | Rp1.650.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Prime</h5>
                                                            <p>Worldwide Excluding USA</p><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> <i class='fas fa-star fa-sm' style='color:#000000'></i> | Rp3.000.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-6" style="border: 1px solid #ec9006; margin-right: 1vw;">
                                                <div class="single-work-inner-advantages style-two text-center">
                                                    <div class="details-wrap">
                                                        <div class="details-inner-advantages">
                                                            <h5>Signature</h5>
                                                            <p>Worldwide</p><br><br>
                                                            <p><i class='fas fa-bed fa-sm' style='color:#000000'></i> <i class='fas fa-star fa-sm' style='color:#000000'></i> | Rp8.000.000 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 1vw;">
                                            <p><i class='fas fa-bed fa-lg' style='color:#000000'></i> : Amount of beds in one treatment room </p>
                                            <p><i class='fas fa-star fa-lg' style='color:#000000'></i> : For Plan Prime and Signature, you can occupy a room 1 level above the lowest single bed room </p>
                                            <p>HKG, SG, JPN : Hong Kong, Singapore, Japan.</p>
                                            <p>USA : United State of America.</p>
                                        </div>
                                    </div>
                            </div>
                        </div><br>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                + Plus Benefits*
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">

                                <div class="project-area half-bg-top pd-top-70">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6 col-md-10">
                                                <div class="section-title style-white text-center">
                                                    <h3 class="title" style="color: #000000;">+ Plus Benefits*</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="project-slider slider-control-round owl-carousel">
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-benefit/rawat-inap.jpeg" alt="img">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;">Hospitalization and Alternative Inpatient Care</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-benefit/booster.jpeg" alt="img">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;">Annual benefit limit booster</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-benefit/rawat-jalan.jpeg" alt="img">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;">Outpatient</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-benefit/rawat-gigi.jpeg" alt="img">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;">Dental Care</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-benefit/persalinan.jpeg" alt="img">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;">Pregnancy, Labor and Postpartum</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div><br>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                + Plus Services
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">

                                <div class="project-area half-bg-top pd-top-70">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6 col-md-10">
                                                <div class="section-title style-white text-center">
                                                    <h3 class="title" style="color: #000000;">+ Plus Services</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="project-slider slider-control-round owl-carousel">
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-service/obat-online.jpeg" alt="img">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;">Outpatient services in the form of Ask a Doctor and Buy Medicine online</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-service/eazy.jpeg" alt="img" style="margin-top: 2vw;">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center; margin-top: 3vw;">Claim online without the hassle through eAZy Connect</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-service/registrasi.jpeg" alt="img" >
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;" >Overseas Hospital and Medical Concierge Registration and International Assistance</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single-project-inner">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url()?>assets/template_front/img/plus-service/evakuasi.jpeg" alt="img">
                                                    </div>
                                                    <div class="details-wrap">
                                                        <div class="details-inner">
                                                            <h5 style="text-align: center;">Medical Opinion and Medical Evacuation Services</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div><br>

                        <div class="accordion-item single-accordion-inner">
                            <h2 class="accordion-header" id="judul-4">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#benefit-4" aria-expanded="true" aria-controls="benefit-4">
                                    Term & Condition
                                </button>
                            </h2>
                            <div id="benefit-4" class="accordion-collapse collapse" aria-labelledby="judul-3"
                                 data-bs-parent="#accordionExample">
                                <div class="accordion-body">

                                    <div class="row justify-content-center">
                                        <div class="col-lg-6 col-md-10">
                                            <div class="section-title style-white text-center">
                                                <h3 class="title" style="color: #000000;">TERM & CONDITION</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <table id="tablepress-5" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr class="row-1 odd">
                                            <th class="column-1"></th>
                                            <th class="column-2"></th>
                                        </tr>
                                        </thead>
                                        <tbody class="row-hover">
                                        <tr class="row-2 even">
                                            <td class="column-1">Product type </td>
                                            <td class="column-2">Additional health insurance (rider)</td>
                                        </tr>
                                        <tr class="row-3 odd">
                                            <td class="column-1">The insured's entry age <br>(nearest birthday)</td>
                                            <td class="column-2">
                                                <ul>
                                                    <li><b>Inpatient, outpatient, dental</b>
                                                        <br>AGE 30 days until 70th
                                                    </li>
                                                    <li><b>Pregnancy, childbirth and puerperium</b>
                                                        <br>AGE 16 th until 45th
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr class="row-4 odd">
                                            <td class="column-1">Coverage age <br>(nearest birthday)</td>
                                            <td class="column-2">
                                                <ul>
                                                    <li><b>Inpatient, outpatient, dental</b>
                                                        <br>until the insured reaches the age of 99 years or can be chosen between 50,60,70,80,90 and 99 years
                                                    </li>
                                                    <li><b>Pregnancy, childbirth and puerperium </b>
                                                        <br>until the insured reaches the age of 46th
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr class="row-5 odd">
                                            <td class="column-1">Currency</td>
                                            <td class="column-2">Idr</td>
                                        </tr>
                                        <tr class="row-6 odd">
                                            <td class="column-1">Premium payment method</td>
                                            <td class="column-2">Follow the basic policy (monthly, quarterly, semi-annually, yearly)</td>
                                        </tr>
                                        <tr class="row-7 odd">
                                            <td class="column-1">Premium paying period</td>
                                            <td class="column-2">Until the end of coverage</td>
                                        </tr>
                                        <tr class="row-8 odd">
                                            <td class="column-1">Underwritting</td>
                                            <td class="column-2">Full underwriting, follow the basic policy</td>
                                        </tr>
                                        <tr class="row-9 odd">
                                            <td class="column-1">Waiting periode</td>
                                            <td class="column-2">
                                                <ul>
                                                    <li><b>Cancer</b>
                                                        <br>90 days
                                                    </li>
                                                    <li><b>Special diseases, hiv/aids, psychological consultation (outpatient & inpatient) complex dental care, dentures</b>
                                                        <br>12 months
                                                    </li>
                                                    <li><b>Other diseases</b>
                                                        <br>30 days
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr class="row-10 odd">
                                            <td class="column-1">Grace periode</td>
                                            <td class="column-2">90 days</td>
                                        </tr>
                                        <tr class="row-11 odd">
                                            <td class="column-1">Minimum sum insured base policy</td>
                                            <td class="column-2">
                                                <ul>
                                                    <li>plan Basic-Elite plus : N/A</li>
                                                    <li>Plan Prime : Rp. 200.000.000</li>
                                                    <li>Plan signature : Rp. 500.000.000</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <p style="font-size: small;"> * valid from the date the policy starts and the reinstatement of the policy </p>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div><br>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="how-it-work-area bg-blue pd-top-110 pd-bottom-110">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">International Healthcare Insurance</h5>
                    <h2 class="title">Product Terms</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>01</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Entry Age</h4>
                            <p>Prospective policyholders age 1 month - 70 years</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>02</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Currency</h4>
                            <p>The currency used for insurance policy payments is Rupiah (IDR)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>03</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>Underwriting</h4>
                            <p>Full Underwriting from the insurance company Allianz follows the Basic Policy</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="faq-area pd-top-100 pd-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title mb-0 text-center">
                    <h5 class="sub-title double-line">International Healthcare Insurance</h5>
                    <h2 class="title">Product Details</h2>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                Premium Payment Method
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Premium Payment Method for Hospital & Surgical Care Premier Plus Program following the Basic Policy (monthly, quarterly, semester, yearly)</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                Age of Coverage of Insurance Policy Holders
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Up to 99 years, or</li>
                                    <li>Can be selected from 50-90 years (applies every multiple of 10)</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                Waiting Period
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">

                                    <p>The details of the waiting period for Policyholders are as follows:</p>
                                    <ul>
                                        <li>Cancer: 90 days</li>
                                        <li>Catastrophic Critical Illness: 90 days</li>
                                        <li>Special Diseases &amp; HIV / AIDS: 12 months</li>
                                        <li>Other Diseases: 30 days</li></ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-relative pd-top-60 pd-bottom-90">
    <img class="shape-left-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/3.webp" alt="img">
    <img class="shape-right-top top_image_bounce" src="<?php echo base_url() ?>assets/template_front/img/shape/4.webp" alt="img">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line">International Healthcare Insurance</h5>
                    <h2 class="title">Identity required to apply</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp');">
                    <div class="icon-box">
                        <i class="icomoon-profile"></i>
                    </div>
                    <div class="details">
                        <h3>KITAS/KITAP</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-service-inner style-2 text-center" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/01.webp');">
                    <div class="icon-box">
                        <i class="icomoon-cloud-data"></i>
                    </div>
                    <div class="details">
                        <h3>PASSPORT</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>