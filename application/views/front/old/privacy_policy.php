<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img" style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $top_contact_us->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="team-area info-box-two pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-9">
                <div class="section-title text-center">
                    <h5 class="sub-title double-line"><?php echo $page->title ?></h5>
                    <h2 class="title"><?php echo $page->title_sub ?></h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <h4 class="title mt-4">1. How does Allianz Indonesia treat the data collected from your visit?</h4>
                <div class="content">
                    <p>Our main business is life insurance and general insurance. We realize that the information we get from you is strictly confidential. For this reason, protecting your privacy is a priority for us.</p>
                    <p>Allianz Indonesia is firmly committed to maintaining the confidentiality and security of the information you provide.</p>
                    <p>Whoever you are, whether you are a potential customer/customer, partner, or job applicant, your information will be protected not only by applicable privacy laws, but more importantly, our commitment to protect your privacy and protect your right to determine how the information will be used.</p>
                </div>
                <h4 class="title mt-4">2. Notification</h4>
                <div class="content">
                    <p>You can browse all content on this Allianz site anonymously. And if we want to obtain, process or use personal data from your visit to this site, we will notify you in advance.</p>
                    <p>When you complete the information request form, you should be aware that you agree to provide us with your personal information.</p>
                    <p>If you decide not to complete the information request form or leave this site, we will not store the data you have provided.</p>
                </div>
                <h4 class="title mt-4">3. Choice</h4>
                <div class="content">
                    <p>If we wish to obtain personal information, you can decide which personal data we are allowed to receive and how we should use it. We will only collect your personal data if you consent and to the extent necessary to help meet your various needs, for example:</p>
                    <ul>
                        <li>Offer the product or service you want</li>
                        <li>Offer the product or service you want Improve our service;</li>
                        <li>Offer you the product or service you want Inform you about our other products or services</li>
                    </ul>
                </div>
                <h4 class="title mt-4">4. Share</h4>
                <div class="content">
                    <p>When you provide information to us, we will not forward that information to other parties without your consent or legally permitted. We may share your data with our affiliates and/or third parties for the purpose of administering or servicing your Allianz policy.</p>
                    <p>We may also collect and prepare statistics that we get from our site about our customers, sales,  traffic patterns and services and provide these statistics to third parties, but these statistics will not contain personal information that can be used to identify an individual.</p>
                </div>
                <h4 class="title mt-4">5. Access</h4>
                <div class="content">
                    <p>We respect your right to privacy, especially the right to access information. If you request, and to the extent required by applicable law, we will inform you of your personal data that we have received through this site.</p>
                    <p>We strive to keep your data accurate. If you wish to inform of changes or submit corrections to your data, please contact us via the contact listed on this site. </p>
                </div>
                <h4 class="title mt-4">6. EU Resident Status Statement</h4>
                <div class="content">
                    <p>All EU residents wishing to exercise their rights as stipulated in the GDPR (General Data Protection Regulation) are required to download, fill out and sign the “ EU Resident  Declaration Form  ( Allianz Life  /  Allianz Utama )” form and send it back to us via e-mail. The following individuals are included in the  EU Resident group :</p>
                    <ul>
                        <li>European Union (EU) country residents who have lived in an EU country for more than 183 days and have tax filing obligations in one of those EU member states.</li>
                    </ul>
                </div>
                <h4 class="title mt-4">7. Personal Data Requests</h4>
                <div class="content">
                    <p>You can request your personal data in electronic form in one of the following ways</p>
                    <ul>
                        <li>Visit our customer service at the Allianz head office or representative office and fill out the “PERSONAL DATA REQUEST FORM ( Allianz Life  /  Allianz Utama )” provided.</li>
                        <li>Contact us via AllianzCare.</li>
                        <li>Download, fill out the “PERSONAL DATA REQUEST FORM ( Allianz Life  /  Allianz Utama )” and send it back to us.</li>
                    </ul>
                    <h6>
                        <a href="<?php echo base_url() ?>assets/template_front/content/formulir-permintaan-data-pribadi-life.pdf" target="_blank"><i class='far fa-file-pdf'></i> DOWNLOAD the Personal Data Request Form (Life)</a>
                    </h6>
                    <h6>
                        <a href="<?php echo base_url()?>assets/template_front/content/formulir-permintaan-data-pribadi-utama.pdf" target="_blank"><i class='far fa-file-pdf'></i> DOWNLOAD the Personal Data Request Form (Main)</a>
                    </h6>
                </div>
                <h4 class="title mt-4">8. Request for Deletion of Personal Data</h4>
                <div class="content">
                    <p>You can ask Allianz to delete your personal data by downloading, filling out the “PERSONAL DATA REQUEST FORM ( Allianz Life  /  Allianz Utama ​)” and sending it back to us. Please note that we will delete data if the following criteria are met:</p>
                    <ol>
                        <li>You are no longer affiliated with Allianz.</li>
                        <li>As long as it does not violate the provisions of Indonesian law related to keeping company documents</li>
                    </ol>
                    <h6>
                        <a href="<?php echo base_url() ?>assets/template_front/content/formulir-permintaan-data-pribadi-life.pdf" target="_blank"><i class='far fa-file-pdf'></i> DOWNLOAD the Personal Data Request Form (Life)</a>
                    </h6>
                    <h6>
                        <a href="<?php echo base_url()?>assets/template_front/content/formulir-permintaan-data-pribadi-utama.pdf" target="_blank"><i class='far fa-file-pdf'></i> DOWNLOAD the Personal Data Request Form (Main)</a>
                    </h6>
                </div>
                <h4 class="title mt-4">9. Questions Regarding Data Privacy</h4>
                <div class="content">
                    <p>If you have further questions about your privacy at Allianz Indonesia, you can contact us:</p>
                    <h6>Allianz Care - Allianz Life and Allianz Utama</h6>
                    <p>World Trade Center (WTC) 3 and 6<br> Jl. General Sudirman, Kav. 29-31<br> South Jakarta 12920<br> Indonesia<br> Tel: 1500 136</p>
                    <h6>Email Allianz Life</h6>
                    <p><a href="contactus@allianz.co.id"></a></p>
                    <h6>Email Allianz Utama</h6>
                    <p><a href="cs@allianz.co.id"></a></p>
                    <p>Thank you for visiting Allianz Indonesia and we look forward to serving you always.</p>
                </div>
                <h4 class="title mt-4">10. Information Regarding Marketing Agreements</h4>
                <div class="content">
                    <p>When you give your consent to receive information, product and/or service offers and be contacted by Allianz and/or its affiliates (and third parties appointed by Allianz and/or its affiliates) for marketing purposes and service improvement, you understand and acknowledge that the above agreement includes:</p>
                    <ol>
                        <li>Approval to be contacted and sent information regarding product and/or service offerings, customer surveys, product campaigns through every available communication medium (including but not limited to letters, electronic mail (email), short message service (SMS), telephone, and other media other digital/electronic communications) by Allianz and/or its affiliates (as well as third parties appointed by Allianz and/or its affiliates); and</li>
                        <li>Approval to authorize Allianz and/or its affiliates (as well as third parties appointed by Allianz and/or its affiliates) to disclose your personal data information related to the interests referred to in point (1).</li>
                    </ol>
                    <p>You can contact Allianz Care at 1500-136 or send an email to contactus@allianz.co.id in the event that you no longer wish to receive information on product and/or service offers from Allianz and/or its affiliates (as well as third parties appointed by Allianz and and/or affiliates).</p>
                </div>
            </div>
        </div>
    </div>
</div>
