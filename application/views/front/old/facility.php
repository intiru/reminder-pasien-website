<section class="breadcrumb-bnr">
    <div class="breadcrumb-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1><?= $page->title ?><img src="<?= base_url('assets/template_front/images/logos2.png') ?>" alt="Simbol Logo Rumah Sunat Bali"></h1>
                </div>
            </div>
        </div>
        <div class="space"></div>
        <div class="descripsi-banner text-center">
            <?= $page->description ?>
        </div>
    </div>
</section>
<div class="container">
    <div class="row justify-content-center metode-biaya-wrapper">
        <?php foreach ($category_facility as $row) { ?>
            <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="1s">
                <div class="service-item">
                    <img class="layanan-img" src="<?= $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->thumbnail_alt ?>">
                    <div class="space"></div>
                    <h2><?= $row->title ?></h2>
                    <?= $row->description ?>
                </div>
                <div class="space"></div>
            </div>
        <?php } ?>
    </div>
</div>