<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $top_about_us->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="about-area pd-top-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-9">
                <div class="about-mask-bg-wrap about-mask-bg-wrap-1 mb-4 mb-lg-0">
                    <iframe width="100%" height="420" src="https://www.youtube.com/embed/se4PjWBbvqw"
                            title="Allianz Indonesia – Company Profile (English) June 2019" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            style="border-radius: 20px"
                            allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="section-title px-lg-5 mb-0">
                    <h5 class="sub-title right-line"><?= $about_allianz_insurance->title_sub ?></h5>
                    <h2 class="title"><?= $about_allianz_insurance->title ?></h2>
                    <p class="content-strong mt-3">
                        <?= $about_allianz_insurance->description ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="why-choose pd-top-100 pd-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-9 order-lg-last">
                <div class="about-mask-bg-wrap about-mask-bg-wrap-2 mb-4 mb-lg-0">
                    <img class="shape-image-sm top_image_bounce"
                         src="<?php echo base_url() ?>assets/template_front/img/about/2sm.webp" alt="img">
                    <img class="shape-image" src="<?php echo base_url() ?>assets/template_front/img/about/2s.webp"
                         alt="img">
                    <div class="thumb">
                        <img src="<?php echo $this->main->image_preview_url($about_help_insurance->thumbnail) ?>" alt="img">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 order-lg-first align-self-center">
                <div class="section-title px-lg-5 mb-0">
                    <h5 class="sub-title right-line"><?= $about_help_insurance->title_sub ?></h5>
                    <h2 class="title"><?= $about_help_insurance->title ?></h2>
                    <p class="content">
                        <?= $about_help_insurance->description ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="counter-area bg-overlay-black pd-top-115 pd-bottom-90"
     style="background-image: url('<?php echo $this->main->image_preview_url($home_our_customers->thumbnail) ?>');">
    <div class="container">
        <div class="row mg-bottom-60">
            <div class="col-lg-12">
                <div class="section-title style-white text-center">
                    <h2 class="title"><?= $dict_your_insurance ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($home_our_customers_list['title'] as $key => $row) { ?>
            <div class="col-md">
                <div class="single-exp-inner style-white">
                    <h2><span class="counter"><?= $home_our_customers_list['title'][$key] ?></span> <sub>Over</sub></h2>
                    <h5><?= $home_our_customers_list['description'][$key] ?></h5>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="faq-area pd-top-100 pd-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8 order-lg-last mb-4 mb-lg-0">
                <div class="faq-image-wrap">
                    <div class="thumb">
                        <img src="<?php echo $this->main->image_preview_url($about_faq->thumbnail) ?>" alt="img">
                        <img class="img-position-1"
                             src="<?php echo base_url() ?>assets/template_front/img/about/f2.webp" alt="img">
                        <img class="img-position-2 top_image_bounce"
                             src="<?php echo base_url() ?>assets/template_front/img/about/f3.webp" alt="img">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 pe-xl-5 order-lg-first align-self-center">
                <div class="section-title mb-0">
                    <h5 class="sub-title right-line"><?= $about_faq->title_sub ?></h5>
                    <h2 class="title"><?= $about_faq->title ?></h2>
                    <p class="content"><?= $about_faq->description ?></p>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Ask about life insurance benefits?
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ol>
                                    <li>
                                        Protect the family from loss of income if the main breadwinner dies
                                    </li>
                                    <li>
                                        Protecting families from the burden of debt
                                    </li>
                                    <li>
                                        Provide a number of valuable inheritance for children
                                    </li>
                                    <li>
                                        As final expenses (cost of death)
                                    </li>
                                    <li>
                                        To be alms Jariyah for the last time.
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Ask how to register for Customer Online Portal (COP)?
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ol>
                                    <li>Enter the Allianz website: www.Allianz.co.id</li>
                                    <li>Select Customer Portal</li>
                                    <li>Choose life & health insurance (individual)</li>
                                    <li>Login or registration options. If not registered, select the registration menu
                                    </li>
                                    <li>Choose according to the policy you have (life insurance / individual health
                                        insurance / group health insurance)
                                    </li>
                                    <li>Enter the complete policy data</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                What are the advantages of activating email correspondence services?
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <p>There are many benefits that can be obtained from this Allianz email correspondence,
                                    including:</p>
                                <ol>
                                    <li><strong>Fast Delivery:</strong> You can access information on due notification
                                        letters, transaction reports and annual reports via personal email, no need to
                                        wait for delivery by post.
                                    </li>
                                    <li><strong>Easy Access:</strong> Quick and easy access anywhere and anytime via
                                        personal email
                                    </li>
                                    <li><strong>Safe:</strong> Information on due notices, transaction reports and
                                        annual reports sent via email has been protected with an encrypted password. The
                                        invoice will be sent directly to the email address you have registered.
                                    </li>
                                    <li><strong>Save paper:</strong> With this digital service, it supports eco-friendly
                                        campaigns in Indonesia
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item single-accordion-inner mb-0">
                        <h2 class="accordion-header" id="headingFour">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Request for registration and activation of an email address?
                            </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <p>Please contact our Allianz Care via e-mail at contactus@allianz.co.id or call 1500
                                    136 and 1500 139.</p>

                                <p>If you are in the Jakarta area, please visit our Customer Care Center at World Trade
                                    Center (WTC) 3 and 6 Jl. General Sudirman, Kav. 29-31, South Jakarta 12920,
                                    Indonesia</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>