<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $page->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-area pd-top-90 pd-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="mask-bg-wrap mask-bg-img-3">
                    <div class="thumb">
                        <img src="<?php echo base_url() ?>assets/template_front/img/about/3.webp" alt="img">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="section-title px-lg-5 mb-0">
                    <h5 class="sub-title left-border">About The Insurance</h5>
                    <p class="content-strong mt-2 mb-4">
                        "Let's provide the best protection for your comfort with Allianz Car Insurance"
                    </p>
                    <h5 class="sub-title left-border">Key Features</h5>
                    <p class="content-strong mt-2 mb-4">Comprehensive vehicle insurance, not complicated with various
                        protection options by Allianz Car Insurance</p>
                    <div class="list-wrap mt-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Vehicle theft by private drivers</li>
                                    <li>24 hour emergency assistance (Allianz RodA)</li>
                                    <li>Flood protection to water hammer</li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="single-list-inner">
                                    <li>Taxi accommodation</li>
                                    <li>24/7 workshop & AllianzCare network</li>
                                    <li>Third Party Liability</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team-area bg-blue pd-top-115 pd-bottom-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">Car Insurance</h5>
                    <h2 class="title">Allianz MobilKu Packages</h2>
                    <p class="content">
                        There are 3 packages according to your needs
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>01</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>MOBILKU ECO</h4>
                            <p>An economical solution for your vehicle protection that suits your needs</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>02</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>MY CAR GRAND</h4>
                            <p>The most complete and hassle-free solution for your vehicle protection</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>03</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <h4>MY CAR IS NON PACKAGE</h4>
                            <p>Vehicle protection solutions according to your choice and desires</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="faq-area pd-top-100 pd-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title mb-0 text-center">
                    <h5 class="sub-title double-line">Car Insurance</h5>
                    <h2 class="title">Workshop</h2>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-1" aria-expanded="true" aria-controls="benefit-1">
                                Allianz RodA (Road Emergency Assistance)
                            </button>
                        </h2>
                        <div id="benefit-1" class="accordion-collapse collapse" aria-labelledby="judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Emergency services provided to the Insured to provide assistance in overcoming
                                        motor vehicle disturbances so that the Insured can continue his journey. If
                                        within 20 minutes the disturbance cannot be handled, a tow will be carried
                                        out.For further information about Allianz RodA, please contact <strong>AllianzCare
                                            at 1500 136.</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-2" aria-expanded="true" aria-controls="benefit-2">
                                General Exceptions
                            </button>
                        </h2>
                        <div id="benefit-2" class="accordion-collapse collapse" aria-labelledby="judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div id="target-id6354e9db7a3e6" class="collapseomatic_content " style=""><p></p>
                                    <p>Please download the List of General Partner Workshops according to your
                                        location:</p>
                                    <ul>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20Bali%20&amp;%20Lombok.pdf"
                                               target="_blank">General
                                                Partner Workshop Bali and Lombok</a></li>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20JABODETABEK.pdf"
                                               target="_blank">JABODETABEK
                                                General Workshop</a></li>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20Jawa%20Barat.pdf"
                                               target="_blank">West
                                                Java General Partner Workshop</a></li>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20Jateng%20&amp;%20Yogya.pdf"
                                               target="_blank">General
                                                Partner Workshop in Central Java and Yogyakarta</a></li>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20Jawa%20Timur.pdf"
                                               target="_blank">East
                                                Java General Partner Workshop</a></li>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20Kalimantan.pdf"
                                               target="_blank">Kalimantan
                                                General Partner Workshop</a></li>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20Sulawesi.pdf"
                                               target="_blank">Sulawesi
                                                General Partner Workshop</a></li>
                                        <li>
                                            <a href="https://www.allianz.co.id/Layanan/daftar%20bengkel/2019/juli/general/Listed%20of%20Appointed%20General%20Workshops%20Sumatera.pdf"
                                               target="_blank">Sumatra
                                                General Partner Workshop</a></li>
                                    </ul>
                                    <p>For further information regarding the General Partner Workshop in each region
                                        please contact <span style="color: #3366ff;"><strong>AllianzCare</strong></span>
                                        at <span style="color: #3366ff;"><strong>1500 136</strong></span>.</p></div>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-3" aria-expanded="true" aria-controls="benefit-3">
                                Authorized Workshop
                            </button>
                        </h2>
                        <div id="benefit-3" class="accordion-collapse collapse" aria-labelledby="judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>For more information about Authorized Workshops in each region please contact
                                        <span style="color: #3366ff;">AllianzCare</span> at <strong><span
                                                    style="color: #3366ff;">1500 136</span></strong>.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="how-it-work-area bg-blue pd-top-110 pd-bottom-110">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title style-white text-center">
                    <h5 class="sub-title double-line">Car Insurance</h5>
                    <h2 class="title">Claim Steps</h2>
                    <p class="content">
                        Hassle-free claims, wherever you are.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>01</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <p>No need to bother going to our office. We can help you with any claims matters where you
                                are.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>02</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <p>Can check your vehicle repair progress via SMS.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-work-inner style-two text-center">
                    <div class="count-wrap">
                        <div class="count-inner">
                            <h2>03</h2>
                        </div>
                    </div>
                    <div class="details-wrap">
                        <div class="details-inner">
                            <p>You can check the status of your claim through the service site anytime, anywhere.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-center pd-top-60">
                <a href="https://www.allianz.co.id/content/dam/onemarketing/azli/wwwallianzcoid/layanan/klaim/klaim-asuransi-kendaraan/alur-klaim-kendaraan.jpg"
                   class="btn btn-base" target="_blank">Claim Process Details</a>
            </div>
        </div>
    </div>
</div>
<div class="faq-area pd-top-100 pd-bottom-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pe-xl-12 align-self-center">
                <div class="section-title mb-0 text-center">
                    <h5 class="sub-title double-line">Car Insurance</h5>
                    <h2 class="title">FAQ</h2>
                    <p>Allianz MobilKu inquiries.</p>
                </div>
                <div class="accordion mt-4" id="accordionExample">
                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="faq-judul-1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq-1" aria-expanded="true" aria-controls="faq-1">
                                Do I need to purchase auto insurance?
                            </button>
                        </h2>
                        <div id="faq-1" class="accordion-collapse collapse" aria-labelledby="faq-judul-1"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Vehicle insurance is highly recommended for every vehicle owner, especially because the
                                risks on the road are higher and repair costs can be unpredictable which can interfere
                                with cash flow conditions, for that insurance provides financial protection for you.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="faq-judul-2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq-2" aria-expanded="true" aria-controls="faq-2">
                                Are there other benefits that I get from vehicle insurance?
                            </button>
                        </h2>
                        <div id="faq-2" class="accordion-collapse collapse" aria-labelledby="faq-judul-2"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                You will feel safe and calm with the guarantee of any unexpected accident that comes to
                                you and your vehicle, as long as the risk is covered by the policy
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="faq-judul-3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq-3" aria-expanded="true" aria-controls="faq-3">
                                How many types of vehicle insurance coverage are there?
                            </button>
                        </h2>
                        <div id="faq-3" class="accordion-collapse collapse" aria-labelledby="faq-judul-3"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Vehicle insurance coverage consists of: Comprehensive coverage of losses due to
                                collisions, collisions, overturning, slips, other people's evil deeds, lost vehicles,
                                floods, riots, riots, loss of additional equipment, etc. Total Loss Only (TLO)
                                guarantees losses due to total burns, losses, accidents with a minimum damage of 75% of
                                the vehicle price.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-4">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-4" aria-expanded="true" aria-controls="benefit-4">
                                What is the age limit of the vehicle that can be insured for the Allianz Mobilku
                                Package?
                            </button>
                        </h2>
                        <div id="benefit-4" class="accordion-collapse collapse" aria-labelledby="judul-4"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                For Combined Guarantee (Comprehensive) and Total Loss Only (TLO) up to 10 years of age.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-5">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-5" aria-expanded="true" aria-controls="benefit-5">
                                What cars can be insured in the Allianz Mobilku Package?
                            </button>
                        </h2>
                        <div id="benefit-5" class="accordion-collapse collapse" aria-labelledby="judul-5"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                All cars of the non-truck type (for example, sedans, MPVs and SUVs) that are used for
                                personal / official purposes for the vehicle owner.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-6">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-6" aria-expanded="true" aria-controls="benefit-6">
                                What is the difference between personal / commercial use in motor vehicle insurance?
                            </button>
                        </h2>
                        <div id="benefit-6" class="accordion-collapse collapse" aria-labelledby="judul-6"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                For personal / official use, the insured does not receive remuneration because the
                                vehicle is used for the insured's own activities, while for commercial use the vehicle
                                owner receives compensation from the use of the vehicle (eg rent).
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-7">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-7" aria-expanded="true" aria-controls="benefit-7">
                                How do I pay for insurance?
                            </button>
                        </h2>
                        <div id="benefit-7" class="accordion-collapse collapse" aria-labelledby="judul-7"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Payment can be made in several ways: direct transfer to the Allianz account stated in
                                the policy, or using a credit card at the cashier at the nearest Allianz office.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-8">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-8" aria-expanded="true" aria-controls="benefit-8">
                                What is meant by the extension of personal accident coverage?
                            </button>
                        </h2>
                        <div id="benefit-8" class="accordion-collapse collapse" aria-labelledby="judul-8"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                The insurance against the risk of personal accident includes death and permanent
                                disability to the driver and / or passenger who is experienced while using the insured
                                vehicle.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-9">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-9" aria-expanded="true" aria-controls="benefit-9">
                                Why do I need to provide information on my mobile number to Allianz?
                            </button>
                        </h2>
                        <div id="benefit-9" class="accordion-collapse collapse" aria-labelledby="judul-9"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Your cellphone number is required so that you can get additional services from Allianz,
                                such as feature and service notifications, policy due dates, claim processing status,
                                and others.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-10">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-10" aria-expanded="true" aria-controls="benefit-10">
                                How do I renew my Allianz Car policy?
                            </button>
                        </h2>
                        <div id="benefit-10" class="accordion-collapse collapse" aria-labelledby="judul-10"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Allianz will automatically remind you by mail, e-mail, or telephone about one month
                                before your policy expires based on data contained in our database. Therefore, it is
                                important for you to inform us of changes in your address or telephone number. The next
                                step, you just need to confirm with Allianz, pay the nominal premium to the Allianz
                                account. After that your policy will be extended again.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-11">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-11" aria-expanded="true" aria-controls="benefit-11">
                                Does Allianz distribute or sell information about my data to other parties?
                            </button>
                        </h2>
                        <div id="benefit-11" class="accordion-collapse collapse" aria-labelledby="judul-11"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Allianz Indonesia is committed not to disseminate your information and / or data to
                                other parties outside Allianz.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-12">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-12" aria-expanded="true" aria-controls="benefit-12">
                                What is an endorsement?
                            </button>
                        </h2>
                        <div id="benefit-12" class="accordion-collapse collapse" aria-labelledby="judul-12"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Changes in data on insurance policies, for example changes in vehicle data, changes in
                                the name of the insured, and others, which can be done by submitting an application for
                                a change or endorsement of the policy. The purpose of the endorsement is to equalize
                                data between the insured's data and the insurer's data to facilitate claims processing.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-13">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-13" aria-expanded="true" aria-controls="benefit-13">
                                What is a Partial Loss claim?
                            </button>
                        </h2>
                        <div id="benefit-13" class="accordion-collapse collapse" aria-labelledby="judul-13"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Namely claims that the repair value has not reached 75% of the actual price of the
                                vehicle shortly before the loss.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-14">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-14" aria-expanded="true" aria-controls="benefit-14">
                                What is a Total Loss claim?
                            </button>
                        </h2>
                        <div id="benefit-14" class="accordion-collapse collapse" aria-labelledby="judul-14"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Claims consist of two types, namely CTL (Construction Total Loss) claims and loss claims
                                (Stolen). CTL (Construction Total Loss) claims are claims caused by accidents whose
                                repair costs are equal to or greater than 75% of the actual price of the vehicle
                                immediately before the loss. Total Stolen Claims are claims arising from the evil act of
                                another person or as a result of theft, including theft which is preceded or accompanied
                                by or followed by violence or violent threats to the insured person and / or motor
                                vehicle with the aim of facilitating motor vehicle theft.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-15">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-15" aria-expanded="true" aria-controls="benefit-15">
                                What is a third party Legal Liability claim?
                            </button>
                        </h2>
                        <div id="benefit-15" class="accordion-collapse collapse" aria-labelledby="judul-15"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Claims that arise as a result of claims from third parties from the insured directly
                                caused by the insured motor vehicle. Claims can be in the form of: Material Damage or
                                Bodily Injury or even death.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-16">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-16" aria-expanded="true" aria-controls="benefit-16">
                                What is the true price of the vehicle?
                            </button>
                        </h2>
                        <div id="benefit-16" class="accordion-collapse collapse" aria-labelledby="judul-16"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Is the market price for a motor vehicle with the same brand, type, model and year of
                                vehicle as stated in the policy, just before the loss and or damage occurs.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-17">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-17" aria-expanded="true" aria-controls="benefit-17">
                                How do I determine the amount covered according to the condition of my car?
                            </button>
                        </h2>
                        <div id="benefit-17" class="accordion-collapse collapse" aria-labelledby="judul-17"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                The sum insured in accordance with the market price of a vehicle can be seen from the
                                car price table which is updated periodically. The market price of the vehicle can be
                                inquired with the Agency or can be seen in the latest automotive magazine.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-18">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-18" aria-expanded="true" aria-controls="benefit-18">
                                How is the calculation of compensation for under insured vehicles?
                            </button>
                        </h2>
                        <div id="benefit-18" class="accordion-collapse collapse" aria-labelledby="judul-18"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                The trick is the sum insured divided by the Market Price at the time of the accident or
                                loss multiplied by the amount of repair value. (Sum Insured) / (Market Price) ×
                                Correction Value. The results of these calculations are then reduced at your own risk
                                according to the provisions in the policy.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-19">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-19" aria-expanded="true" aria-controls="benefit-19">
                                What is an example of calculating a claim with a sum insured under insured or over
                                insured?
                            </button>
                        </h2>
                        <div id="benefit-19" class="accordion-collapse collapse" aria-labelledby="judul-19"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Example A - Under Insured:</li>
                                    <li>Coverage price for ZZ brand car: IDR 200,000,000</li>
                                    <li>Market price at the time the risk occurs: IDR 250,000,000</li>
                                    <li>Claim of: IDR 20,000,000</li>
                                    <li>The insurance company will pay: (IDR 200,000,000) / (IDR 250,000,000) × IDR
                                        20,000,000 = IDR 16,000,000 - IDR. 300,000 = 15,700,000, -
                                    </li>
                                    <li>The difference of IDR 4,300,000 will be borne by the insured himself.</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-20">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-20" aria-expanded="true" aria-controls="benefit-20">
                                Example B - Over Insured:
                            </button>
                        </h2>
                        <div id="benefit-20" class="accordion-collapse collapse" aria-labelledby="judul-20"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>Coverage price for ZZ brand car: IDR 150,000,000</li>
                                    <li>Market price at the time the risk occurs: IDR 100,000,000</li>
                                    <li>Claim of: IDR 50,000,000</li>
                                    <li>Own risk: Rp. 300,000 (example)</li>
                                    <li>Due to over insured, what the Insurer pays is Rp. 50,000,000 - Rp. 300,000 = Rp.
                                        49,700,000
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-21">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-21" aria-expanded="true" aria-controls="benefit-21">
                                Can the claim submission be represented by other people?
                            </button>
                        </h2>
                        <div id="benefit-21" class="accordion-collapse collapse" aria-labelledby="judul-21"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Yes, as long as you attach the policyholder's original KTP or power of attorney and a
                                photocopy of the policyholder's KTP, and know the incident in detail by attaching these
                                letters.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-22">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-22" aria-expanded="true" aria-controls="benefit-22">
                                Is the driver also guaranteed if he has an accident while driving my car?
                            </button>
                        </h2>
                        <div id="benefit-22" class="accordion-collapse collapse" aria-labelledby="judul-22"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                If you have extended your personal accident insurance, automatically the drivers and
                                passengers will be covered at the time of the incident. So that if the driver is driving
                                your car, he will also be protected.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-23">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-23" aria-expanded="true" aria-controls="benefit-23">
                                Can I get guaranteed maintenance costs in the event of an accident?
                            </button>
                        </h2>
                        <div id="benefit-23" class="accordion-collapse collapse" aria-labelledby="judul-23"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                You can get guaranteed maintenance costs by extending the medical expenses coverage or
                                medical expenses on Allianz Mobilku products.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-24">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-24" aria-expanded="true" aria-controls="benefit-24">
                                What is the Original Defect Clause? How does this affect when I make a claim?
                            </button>
                        </h2>
                        <div id="benefit-24" class="accordion-collapse collapse" aria-labelledby="judul-24"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                The original defect clause is an additional provision in the motor vehicle policy which
                                explains the condition of the damage to your vehicle, which occurred before the vehicle
                                was insured. Allianz does not guarantee for these damages.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item single-accordion-inner">
                        <h2 class="accordion-header" id="judul-25">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#benefit-25" aria-expanded="true" aria-controls="benefit-25">
                                Does Allianz Insurance services cover all cities in Indonesia?
                            </button>
                        </h2>
                        <div id="benefit-25" class="accordion-collapse collapse" aria-labelledby="judul-25"
                             data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Allianz services can be obtained wherever you are in Indonesia as long as there is an
                                Allianz branch or sales office in that city. If you don't have an Allianz office in your
                                city, you can come to the Allianz representative office closest to where you live.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>