
<main class="contact-page style-5">
    <section class="contact section-padding pt-50 style-6">
        <div class="container">
            <div class="section-head text-center mb-100 style-5">
                <h2 class="mb-20"> Sign <span> In </span> </h2>
            </div>
            <div class="text-center mb-100">
                <h4 class="fw-normal mb-10 color-000">Silahkan login pada sistem dibawah</h4>
            </div>
            <div class="content">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <form action="contact.php" class="form" method="post">
                            <p class="text-center text-danger fs-12px mb-30">The field is required mark as *</p>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group mb-20">
                                        <input type="text" name="email" class="form-control" placeholder="Email Address *" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group mb-20">
                                        <input type="password" name="password" class="form-control"  placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group mb-20">
                                        <select name="option" class="form-select">
                                            <option value="how can we help" selected>Client ID</option>
                                            <option value="option 1">Client 1</option>
                                            <option value="option 2">Client 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea rows="10" name="message" class="form-control" placeholder="How can we help you?"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <div class="form-check d-inline-flex mt-30 mb-30">
                                        <input class="form-check-input me-2 mt-0" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label small" for="flexCheckDefault">
                                            By submitting, i’m agreed to the <a href="page-contact-5.html#" class="text-decoration-underline">Terms & Conditons</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <input type="submit" value="Sign In" class="btn rounded-pill blue5-3Dbutn hover-blue2 sm-butn fw-bold text-light">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <img src="<?php echo base_url() ?>assets/template_front/gambar/icons/contact_a.png" alt="Icon Contact" class="contact_a">
                <img src="<?php echo base_url() ?>assets/template_front/gambar/icons/contact_message.png" alt="Icon Contact Message" class="contact_message">
            </div>
        </div>
    </section>
</main>