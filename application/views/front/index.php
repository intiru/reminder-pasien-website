<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>

    <link href="<?php echo base_url() ?>assets/template_front/gambar/favicon-aplikasi-otomatis-reminder-follow-up-kontrol-pasien-via-whatsapp-144.png"
          rel="apple-touch-icon"
          title="Favicon Aplikasi Reminder Pasien Otomatis untuk Follow Up, Kontrol Pasien via WhatsApp"
          sizes="144x144">
    <link href="<?php echo base_url() ?>assets/template_front/gambar/favicon-aplikasi-otomatis-reminder-follow-up-kontrol-pasien-via-whatsapp-114.png"
          rel="apple-touch-icon"
          title="Favicon Aplikasi Reminder Pasien Otomatis untuk Follow Up, Kontrol Pasien via WhatsApp"
          sizes="114x114">
    <link href="<?php echo base_url() ?>assets/template_front/gambar/favicon-aplikasi-otomatis-reminder-follow-up-kontrol-pasien-via-whatsapp-72.png"
          title="Favicon Aplikasi Reminder Pasien Otomatis untuk Follow Up, Kontrol Pasien via WhatsApp"
          rel="apple-touch-icon"
          sizes="72x72">
    <link href="<?php echo base_url() ?>assets/template_front/gambar/favicon-aplikasi-otomatis-reminder-follow-up-kontrol-pasien-via-whatsapp.png"
          rel="shortcut icon"
          title="Favicon Aplikasi Reminder Pasien Otomatis untuk Follow Up, Kontrol Pasien via WhatsApp"
          sizes="16x16">
    <?php if ($view_secret) { ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>assets/template_front/plugin/slick/slick.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/dist/css/lightbox.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>assets/template_front/plugin/bootstrap-icons/bootstrap-icons.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/lib/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/lib/all.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/lib/animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/lib/jquery.fancybox.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/lib/lity.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/lib/swiper.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/style.css"/>
        <script src="<?php echo base_url() ?>assets/template_front/js/lib/jquery-3.0.0.min.js"></script>

    <?php } else { ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('assets/template_front/css/mulia-tunggal-abadi.min.css') ?>">
    <?php } ?>

</head>

<body data-base-url="<?php echo base_url() ?>" data-whatsapp-link="<?php echo $whatsapp_link_contact ?>">

<div class='container-loading d-none'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>

<?php if ($client_status) { ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#modal-whatsapp-redirect').modal('show');
        });

        var settimmer = 0;
        $(function () {
            window.setInterval(function () {
                var timeCounter = $(".whatsapp-countdown").html();
                var updateTime = eval(timeCounter) - eval(1);
                var whatsapp_redirect_link = $('.whatsapp-redirect-link').attr('href');
                $(".whatsapp-countdown").html(updateTime);

                if (updateTime == 0) {
                    $('.whatsapp-redirect-link').click();
                    $(".whatsapp-countdown").html(0);
                    $('#modal-whatsapp-redirect').modal('hide');
                }
            }, 1000);

        });
    </script>

    <div class="modal fade" id="modal-whatsapp-redirect" data-bs-backdrop="static" data-bs-keyboard="false"
         tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/<?php echo $client_data['logo'] ?>"><br/>
                    <div style="font-size: 20px" class="whatsapp-countdown">3</div>
                    <small>sedang menuju ke WhatsApp</small> ...<br/>
                    <strong><?php echo $client_data['title'] ?></strong> ...
                    <br/>
                    <br/>
                    <a href="https://wa.me/<?php echo $client_data['nomer'] . '?text='.urlencode('Pertanyaan: '); ?>"
                       class="whatsapp-redirect-link btn rounded-pill blue5-3Dbutn hover-blue3 fw-bold text-white klik-untuk-regis bg-hijau-wa"
                       target="_blank">
                        <small class="d-flex justify-content-center align-items-center text-light">
                            <i class="fab fa-whatsapp fs-5 me-2 pe-2 border-end"></i>
                            <span class="text-klik-untuk">KLIK DISINI</span></small>
                    </a>
                    <small><i>jika belum terarah ke WhatsApp <?php echo $client_data['title'] ?></i></small>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php echo $menu ?>
<?php echo $content ?>
<?php echo $footer ?>


<?php if ($view_secret) { ?>

    <script src="<?php echo base_url() ?>assets/template_front/dist/js/lightbox.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/jquery-migrate-3.0.0.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/wow.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/jquery.fancybox.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/lity.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/swiper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/jquery.counterup.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/pace.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/lib/scrollIt.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/plugin/slick/slick.min.js"></script>
    <script src="<?php echo base_url() ?>assets/template_front/js/main.js"></script>
<?php } else { ?>
    <script type="text/javascript"
            src="<?php echo base_url('assets/template_front/js/mulia-tunggal-abadi.min.js') ?>"></script>
<?php } ?>
</body>
</html>