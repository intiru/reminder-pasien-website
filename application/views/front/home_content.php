<header class="style-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="info">
                    <h1>Solusi Reminder Pasien
                        <span>
                                via WhatsApp
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/garis.png" alt="Garis" class="head-line">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/verified.png" alt="Logo Verified" class="head-pen">
                            </span>
                    </h1>
                    <p>Mudahkan komunikasi menggunakan Reminder Otomatis dengan Pasien via WhatsApp, sehingga
                        Potensi Calon Pasien baru semakin Besar.
                        Mudahkan operasional klinik untuk memaksimalkan kinerja dengan Sistem di ReminderPasien.com
                    </p>
                </div>
            </div>
            <div class="d-flex d-md-none align-items-center justify-content-center flex-lg-row mt-50 g-20px">
                <a href="https://api.whatsapp.com/send?phone=6281237376068&amp;text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                   class="btn rounded-pill blue5-3Dbutn hover-blue3 fw-bold text-white klik-untuk-regis bg-hijau-wa"
                   target="_blank">
                    <small class="d-flex justify-content-center align-items-center text-light"> <i
                                class="fab fa-whatsapp fs-5 me-2 pe-2 border-end"></i>
                        <span class="text-klik-untuk">Klik Untuk Registrasi</span></small>
                </a>
                <a href="https://youtu.be/xIRH401yQkU" data-lity=""
                   class="play-btn d-flex align-items-center justify-content-center view-promosi">
                        <span class="icon me-2 for-video">
                            <i class="fas fa-play ms-1 text-biru-video"></i>
                        </span>
                    <strong class="small">Video <br> Pengenalan</strong>
                </a>
            </div>
            <div class=" d-none d-md-inline-flex row my-5 justify-content-center align-items-center">
                <div class="col-lg-4 col-12">
                    <div class="d-flex justify-content-center flex-column section-head style-5">
                        <h2 class="mb-1">7 Klinik :</h2>
                        <p class="text-black-50">Daftar klinik-klinik yang sudah menggunakan reminder pasien</p>
                    </div>
                </div>
                <div class="row col-lg-8 col-12">
                    <section class="clients style-5">
                        <div class="container text-center">
                            <div class="content row">
                                <a href="#" class="img img-card col bg-white flex-column">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-rumah-sunat-bali.png" alt="Logo Rumah Sunat Bali">
                                    <div class="my-1 line">
                                        <hr />
                                    </div>
                                    <b class="text-klien">Rumah Sunat <br> Bali</b>
                                </a>
                                <a href="#" class="img img-card col bg-white flex-column">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/circle-dental.png" alt="Logo Circle Dental">
                                    <div class="my-1 line">
                                        <hr/>
                                    </div>
                                    <b class="text-klien">Circle <br> Dental</b>
                                </a>
                                <a href="#" class="img img-card col bg-white flex-column">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/rumah-sakit-umum-daerah-Dr-mohamad-soewandhie.png" alt="Logo Rumah Sakit Umum Daerah Dr. Mohamad Soewandhie">
                                    <div class="my-1 line">
                                        <hr/>
                                    </div>
                                    <b class="text-klien">RSUD Dr. Mohamad Soewandhie</b>
                                </a>
                                <a href="#" class="img img-card col bg-white flex-column">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/cedine-mechala.png" class="rounded-circle" alt="Logo Cedine Mechala">
                                    <div class="my-1 line">
                                        <hr/>
                                    </div>
                                    <b class="text-klien">Cedine <br> Mechala</b>
                                </a>
                                <a href="#" class="img img-card text-start col bg-white">
                                    <h6 class="text-abu">Dan selanjutnya...</h6>
                                </a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <section class="mockup d-block d-md-none my-5 h-250px">
                <div class="container-fluid">
                    <div class="text-center mb-1">
                        <h2 class="mb-1">7 Klinik :</h2>
                        <p class="text-black-50">Daftar klinik-klinik yang sudah menggunakan reminder pasien</p>
                    </div>
                    <section class="clients style-5">
                        <div class="text-center">
                            <div class="content-hp row justify-content-center for-slick2 mt-0 d-flex d-md-none slick-klinik klinik">
                                <a href="#" class="img img-card bg-white flex-column h-card">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-rumah-sunat-bali-mobile.png" alt="Logo Rumah Sunat Bali">
                                    <div class="my-1 line">
                                        <hr>
                                    </div>
                                    <b class="text-klien">Rumah Sunat <br> Bali</b>
                                </a>
                                <a href="#" class="img img-card bg-white flex-column h-card">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-circle-dental-mobile.png" alt="Logo Circle Dental">
                                    <div class="my-1 line">
                                        <hr>
                                    </div>
                                    <b class="text-klien">Circle <br> Dental</b>
                                </a>
                                <a href="#" class="img img-card bg-white flex-column h-card">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/rumah-sakit-umum-daerah-Dr-mohamad-soewandhie-mobile.png" alt="Logo Rumah Sakit Umum Daerah Dr. Mohamad Soewandhie">
                                    <div class="my-1 line">
                                        <hr>
                                    </div>
                                    <b class="text-klien">RSUD Dr. Mohamad Soewandhie</b>
                                </a>
                                <a href="#" class="img img-card bg-white flex-column h-card">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/sumia-aesthetic-clinic-mobile.png" alt="Logo Sumia Aesthetic Clinic">
                                    <div class="my-1 line">
                                        <hr>
                                    </div>
                                    <b class="text-klien">Sumia Aesthetic Clinic</b>
                                </a>
                                <a href="#" class="img img-card bg-white flex-column h-card">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/klinik-krakatau-mobile.png" alt="Logo Klinik Krakatau">
                                    <div class="my-1 line">
                                        <hr>
                                    </div>
                                    <b class="text-klien">Klinik <br> Krakatau</b>
                                </a>
                                <a href="#" class="img img-card bg-white flex-column h-card">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/rsia-frisdhy-angel-mobile.png" alt="Logo RSIA Frisdhy Angel">
                                    <div class="my-1 line">
                                        <hr>
                                    </div>
                                    <b class="text-klien">RSIA <br> Frisdhy Angel</b>
                                </a>
                                <a href="#" class="img img-card bg-white flex-column h-card">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/cedine-mechala-mobile.png" alt="Logo Cedine Mechala">
                                    <div class="my-1 line">
                                        <hr>
                                    </div>
                                    <b class="text-klien">Cedine <br> Mechala</b>
                                </a>
                            </div>
                        </div>
                    </section>
                </div>
            </section>

            <section class="numbers pt-10 mt-2">
                <div class="container">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="number-card style-6 text-total-ps">
                                    <h2 class="me-4 biru-terang">
                                        <span class="counter">12+</span>
                                    </h2>
                                    <div class="text fs-6">
                                        Total <br> Pasien
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="number-card style-6 text-total-wa">
                                    <h2 class="me-4 biru-terang">
                                        <span class="counter">15K</span>
                                    </h2>
                                    <div class="text fs-6">
                                        Total WhatsApp <br> Terkirim
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="number-card style-6 border-0">
                                    <h2 class="me-4 biru-terang">
                                        <span class="counter">265</span>
                                    </h2>
                                    <div class="text fs-6">
                                        Total <br> Appointment
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-md-flex align-items-center justify-content-center flex-lg-row mt-50 g-20px">
                    <a href="https://api.whatsapp.com/send?phone=6281237376068&amp;text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                       class="btn rounded-pill blue5-3Dbutn hover-blue3 fw-bold text-white klik-untuk-regis bg-hijau-wa"
                       target="_blank">
                        <small class="d-flex justify-content-center align-items-center text-light"> <i
                                    class="fab fa-whatsapp fs-5 me-2 pe-2 border-end"></i>
                            <span class="text-klik-untuk">Klik Untuk Registrasi</span></small>
                    </a>
                    <a href="https://youtu.be/xIRH401yQkU" data-lity=""
                       class="play-btn d-flex align-items-center justify-content-center view-promosi">
                            <span class="icon me-2 for-video">
                                <i class="fas fa-play ms-1 biru-terang"></i>
                            </span>
                        <strong class="small">Video <br> Pengenalan</strong>
                    </a>
                </div>
            </section>
            <div class="col-lg-8">
                <div class="info">
                    <div class="main-img">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/dashboard-reminder-pasien.png" alt="Dashboard Reminder Pasien" class="page-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/whatsapp-hand.png" alt="Whatsapp Hand" class="handl-img w-40">
    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/hand-phone.png" alt="Phone Hand" class="handr-img for-hand-phone">
</header>
<svg xmlns="http://www.w3.org/2000/svg" class="gelombang-fitur" viewBox="0 0 1440 320">
    <path fill="#f3f7fe" fill-opacity="1"
          d="M0,256L48,240C96,224,192,192,288,176C384,160,480,160,576,176C672,192,768,224,864,213.3C960,203,1056,149,1152,117.3C1248,85,1344,75,1392,69.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
</svg>
<main>

    <section  id="fitur">
        <section class="features style-5 fitur-desktop d-lg-block d-none bg-gray5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-head text-center mb-60 style-6">
                            <h2 class="mb-20"> Daftar Fitur <span> <small>Aplikasi</small> </span></h2>
                            <p>Menyediakan Fitur Aplikasi Klinik Terbaik
                            </p>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="features-card mb-30 style-5">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/whatsapp-logo.png" alt="Whatsapp">
                                </div>
                                <div class="info">
                                    <h5 class="card-title">
                                        Reminder Otomatis Pasien dengan WhatsApp
                                    </h5>
                                    <p class="text">
                                        Otomatiskan Reminder Jadwal Konsultasi, Operasi, Tindakan dan Kontrol via
                                        WhatsApp
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="features-card mb-30 style-5">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/gmail-logo.png" alt="Gmail">
                                </div>
                                <div class="info">
                                    <h5 class="card-title">
                                        Reminder Jadwal Operasional Klinik via Email
                                    </h5>
                                    <p class="text">
                                        Reminder jadwal operasional klinik untuk hari selanjutnya via Email
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="features-card mb-30 style-5">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/schedule-logo.png" alt="Schedule">
                                </div>
                                <div class="info">
                                    <h5 class="card-title">
                                        Permudah Merangkum Jadwal Operasional
                                    </h5>
                                    <p class="text">
                                        Permudah Operasional Klinik dengan Tampilan Rangkuman Tasks dalam Kalender
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="features-card mb-30 style-5">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/time-logo.png" alt="Time">
                                </div>
                                <div class="info">
                                    <h5 class="card-title">
                                        Solusi Permudah Sistem Operasional Klinik
                                    </h5>
                                    <p class="text">
                                        Efisiensikan waktu operasional klinik dengan sistem komputer secara Online
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="features-card mb-30 mb-lg-0 style-5">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/computer-logo.png" alt="Computer">
                                </div>
                                <div class="info">
                                    <h5 class="card-title">
                                        Aplikasi Komputer Cetak Rekam Medis & Kwitansi
                                    </h5>
                                    <p class="text">
                                        Permudah proses cetak rekam medis & kwitansi pembayaran Pasien
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="features-card mb-30 mb-lg-0 style-5">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/privacy-logo.png" alt="Privacy">
                                </div>
                                <div class="info">
                                    <h5 class="card-title">
                                        Privasi Data Klinik Dijamin Keamanannya
                                    </h5>
                                    <p class="text">
                                        Jaminan keamanan data karena menggunakan Backup Otomatis
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" class="gelombang-fitur-2" viewBox="0 0 1440 320">
                <path fill="#fff" fill-opacity="1"
                      d="M0,256L48,240C96,224,192,192,288,176C384,160,480,160,576,176C672,192,768,224,864,213.3C960,203,1056,149,1152,117.3C1248,85,1344,75,1392,69.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
            </svg>
        </section>

        <section class="features style-5 fitur-desktop d-block d-lg-none bg-gray5 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-head text-center mb-60 style-6">
                            <h2 class="mb-20"> Daftar Fitur <span> <small>Aplikasi</small> </span></h2>
                            <p>Menyediakan Fitur Aplikasi Klinik Terbaik
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row partner for-slick2 my-0 fitur-arrow">
                    <div class="col-3 mx-3">
                        <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                           class="features-card mb-30 style-5">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/whatsapp-logo.png" alt="Whatsapp Logo">
                            </div>
                            <div class="info">
                                <h5 class="card-title fs-15-5px">
                                    Reminder Otomatis Pasien dengan WhatsApp
                                </h5>
                                <p class="text fs-10-5px">
                                    Otomatiskan Reminder Jadwal Konsultasi, Operasi, Tindakan dan Kontrol via
                                    WhatsApp
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-3">
                        <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                           class="features-card mb-30 style-5">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/gmail-logo.png" alt="Gmail Logo">
                            </div>
                            <div class="info">
                                <h5 class="card-title fs-15-5px">
                                    Reminder Jadwal Operasional Klinik via Email
                                </h5>
                                <p class="text fs-10-5px">
                                    Reminder jadwal operasional klinik untuk hari selanjutnya via Email
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-3">
                        <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                           class="features-card mb-30 style-5">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/schedule-logo.png" alt="Schedule Logo">
                            </div>
                            <div class="info">
                                <h5 class="card-title fs-15-5px">
                                    Permudah Merangkum Jadwal Operasional
                                </h5>
                                <p class="text fs-10-5px">
                                    Permudah Operasional Klinik dengan Tampilan Rangkuman Tasks dalam Kalender
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-3">
                        <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                           class="features-card mb-30 style-5">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/time-logo.png" alt="Time Logo">
                            </div>
                            <div class="info">
                                <h5 class="card-title fs-15-5px">
                                    Solusi Permudah Sistem Operasional Klinik
                                </h5>
                                <p class="text fs-10-5px">
                                    Efisiensikan waktu operasional klinik dengan sistem komputer secara Online
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-3">
                        <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                           class="features-card mb-30 mb-lg-0 style-5">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/computer-logo.png" alt="Computer Logo">
                            </div>
                            <div class="info">
                                <h5 class="card-title fs-15-5px">
                                    Aplikasi Komputer Cetak Rekam Medis & Kwitansi
                                </h5>
                                <p class="text fs-10-5px">
                                    Permudah proses cetak rekam medis & kwitansi pembayaran Pasien
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-3">
                        <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                           class="features-card mb-30 mb-lg-0 style-5">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/privacy-logo.png" alt="Privacy Logo">
                            </div>
                            <div class="info">
                                <h5 class="card-title fs-15-5px">
                                    Privasi Data Klinik Dijamin Keamanannya
                                </h5>
                                <p class="text fs-10-5px">
                                    Jaminan keamanan data karena menggunakan Backup Otomatis
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path fill="#fff" fill-opacity="1"
                      d="M0,256L48,240C96,224,192,192,288,176C384,160,480,160,576,176C672,192,768,224,864,213.3C960,203,1056,149,1152,117.3C1248,85,1344,75,1392,69.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
            </svg>
        </section>
    </section>

    <section class="about style-5">
        <div class="content p-0">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 order-2 order-lg-0">
                        <div class="section-head mb-30 style-6 text-md-start text-center">
                            <h2> Mudah Diakses melalui <span> <small>Gadget</small> </span></h2>
                        </div>
                        <p class="text-md-start text-center">Menyediakan Fitur Aplikasi Klinik Terbaik</p>
                        <div class="line-links">
                            <ul class="list-icon">
                                <li class="d-flex justify-content-center align-items-center">
                                    <span class="icon bg-hijau-centang">
                                        <i class="bi bi-check2"></i>
                                    </span>
                                    <h6>

                                        Permudah Akses Proses Operasional Klinik Anda melalui Genggaman Smartphone
                                        secara Online
                                    </h6>
                                </li>
                                <li class="d-flex justify-content-center align-items-center">
                                    <span class="icon bg-hijau-centang">
                                        <i class="bi bi-check2"></i>
                                    </span>
                                    <h6>
                                        <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB">
                                            Menjaga Hubungan Klinik dengan Pasien menggunakan fitur Reminder via
                                            WhatsApp</a>
                                    </h6>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-7 order-0 order-lg-2">
                        <div class="img main-img1">
                            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/background-phone.png" alt="Background Phone" class="top-20px">
                            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/mockup-phone.png" alt="Mockup Phone" class="img-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content mt-3">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="img main-img2 mb-0 pt-40">
                            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/background-computer.png" alt="Background Komputer" class="sm-circle">
                            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/mockup-computer.png" alt="Mockup Komputer" class="img-body d-none d-md-block">
                            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/mockup-computer-mobile.png" alt="Mockup Komputer" class="img-body d-md-none d-block">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="section-head mb-30 style-6 text-md-start text-center">
                            <h2> Permudah Reminder <span> <small>Pasien</small> </span></h2>
                        </div>
                        <p class=" text-md-start text-center">
                            Menyediakan Fitur Aplikasi Klinik Terbaik
                        </p>
                        <ul class="list-icon">
                            <li>
                                    <span class="icon bg-hijau-centang">
                                        <i class="bi bi-check2"></i>
                                    </span>
                                <h6>
                                    Reminder Pasien secara Otomatis dengan WhatsApp.
                                </h6>
                            </li>
                            <li>
                                    <span class="icon bg-hijau-centang">
                                        <i class="bi bi-check2"></i>
                                    </span>
                                <h6>
                                    Rangkum Jadwal Operasional pada Kalender Task.
                                </h6>
                            </li>
                            <li>
                                    <span class="icon bg-hijau-centang">
                                        <i class="bi bi-check2"></i>
                                    </span>
                                <h6>
                                    Manajemen Data Pasien secara Terstruktur & Online.
                                </h6>
                            </li>
                            <li>
                                    <span class="icon bg-hijau-centang">
                                        <i class="bi bi-check2"></i>
                                    </span>
                                <h6>
                                    Skema Digital Operasional Pasien dengan Mudah.
                                </h6>
                            </li>
                        </ul>
                        <div class="w-100 text-center text-md-start">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="btn rounded-pill blue5-3Dbutn hover-blue2 sm-butn fw-bold mt-3 text-center bg-hijau-wa">
                                <span class="text-light">  <i
                                            class="fab fa-whatsapp fs-5 mx-1 pe-2 border-end"></i> Klik Untuk Registrasi </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="clients style-5 my-5 mb-0">
        <div class="container text-center">
            <div class="section-head mb-4 style-6 text-center">
                <h2 class="mb-3"> 7 Klien Yang Sudah
                    <span> <small> Menggunakan </small> </span>
                </h2>
                <p class="color-666">Klien yang sudah menggunakan jasa kami</p>
            </div>
            <div class="content row justify-content-center d-none d-md-flex">
                <a href="#" class="img img-card flex-column">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-rumah-sunat-bali-halaman-klien.png" alt="Logo Rumah Sunat Bali">
                    <div class="my-1 line">
                        <hr>
                    </div>
                    <b class="text-klien">Rumah Sunat Bali</b>
                </a>
                <a href="#" class="img img-card flex-column">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-circle-dental-halaman-klien.png" alt="Logo Circle Dental">
                    <div class="my-1 line">
                        <hr/>
                    </div>
                    <b class="text-klien">Circle Dental</b>
                </a>
                <a href="#" class="img img-card flex-column">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/rumah-sakit-umum-daerah-Dr-mohamad-soewandhie-halaman-klien.png" alt="Logo Rumah Sakit Umum Daerah Dr. Mohamad Soewandhie">
                    <div class="my-1 line">
                        <hr/>
                    </div>
                    <b class="text-klien">RSUD Dr. Mohamad Soewandhie</b>
                </a>
                <a href="#" class="img img-card flex-column">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/sumia-aesthetic-clinic.png" alt="Logo Sumia Aesthetic Clinic">
                    <div class="my-1 line">
                        <hr/>
                    </div>
                    <b class="text-klien">Sumia Aesthetic Clinic</b>
                </a>
                <a href="#" class="img img-card flex-column">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/klinik-krakatau.png" alt="Logo Klinik Krakatau">
                    <div class="my-1 line">
                        <hr/>
                    </div>
                    <b class="text-klien">Klinik Krakatau</b>
                </a>
                <a href="#" class="img img-card flex-column">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/rsia-frisdhy-angel.png" alt="Logo RSIA Frisdhy Angel">
                    <div class="my-1 line">
                        <hr/>
                    </div>
                    <b class="text-klien">RSIA Frisdhy Angel</b>
                </a>
                <a href="#" class="img img-card flex-column">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/cedine-mechala-halaman-klien.png" class="rounded-circle" alt="Logo Cedine Mechala">
                    <div class="my-1 line">
                        <hr/>
                    </div>
                    <b class="text-klien">Cedine Mechala</b>
                </a>
            </div>

            <div class="content-hp row justify-content-center for-slick2 mt-0 d-flex d-md-none dabel klinik">
                <a href="#" class="img img-card bg-white flex-column h-card">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/logo-rumah-sunat-bali-halaman-klien-mobile.png" alt="Logo Rumah Sunat Bali">
                    <div class="my-1 line">
                        <hr class="h-1-5px">
                    </div>
                    <b class="text-klien">Rumah Sunat <br> Bali</b>
                </a>
                <a href="#" class="img img-card bg-white flex-column h-card">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/circle-dental-halaman-klien-mobile.png" alt="Logo Circle Dental">
                    <div class="my-1 line">
                        <hr class="h-1-5px">
                    </div>
                    <b class="text-klien">Circle <br> Dental</b>
                </a>
                <a href="#" class="img img-card bg-white flex-column h-card">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/rumah-sakit-umum-daerah-Dr-mohamad-soewandhie-halaman-klien-mobile.png" alt="Logo RSUD Dr. Mohamad Soewandhie" class="mt-3">
                    <div class="my-1 line">
                        <hr class="h-1-5px">
                    </div>
                    <b class="text-klien">RSUD Dr. <br> Mohamad <br> Soewandhie</b>
                </a>
                <a href="#" class="img img-card bg-white flex-column h-card">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/sumia-aesthetic-clinic-halaman-klien-mobile.png" alt="Logo Sumia Aesthetic Clinic">
                    <div class="line my-1">
                        <hr class="h-1-5px">
                    </div>
                    <b class="text-klien">Sumia <br> Aesthetic Clinic</b>
                </a>
                <a href="#" class="img img-card bg-white flex-column h-card">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/klinik-krakatau-halaman-klien-mobile.png" alt="Logo Klinik Krakatau">
                    <div class="my-1 line">
                        <hr class="h-1-5px">
                    </div>
                    <b class="text-klien">Klinik <br> Krakatau</b>
                </a>
                <a href="#" class="img img-card bg-white flex-column h-card">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/rsia-frishdy-angel-halaman-klien-mobile.png" alt="Logo RSIA Frisdhy Angel">
                    <div class="my-1 line">
                        <hr class="h-1-5px">
                    </div>
                    <b class="text-klien">RSIA <br> Frisdhy Angel</b>
                </a>
                <a href="#" class="img img-card bg-white flex-column h-card">
                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/cedine-mechala-halaman-klien-mobile.png" class="rounded-circle" alt="Logo Cedine Mechala">
                    <div class="my-1 line">
                        <hr class="h-1-5px">
                    </div>
                    <b class="text-klien">Cedine <br> Mechala</b>
                </a>
            </div>
        </div>
    </section>

    <svg xmlns="http://www.w3.org/2000/svg" class="gelombang-responsive" viewBox="0 0 1440 320">
        <path fill="#f3f7fe" fill-opacity="1"
              d="M0,256L48,240C96,224,192,192,288,176C384,160,480,160,576,176C672,192,768,224,864,213.3C960,203,1056,149,1152,117.3C1248,85,1344,75,1392,69.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
    </svg>

    <section class="testimonials bg-gray5 style-5 pt-0 pb-0 mb-0" id="testimoni">
        <div class="container">
            <div class="section-head text-center mb-60 style-6">
                <h2 class="mb-20"> Testimonial <span> <small>Pengguna</small> </span></h2>
                <p> Tanggapan Pengguna ReminderPasien.com </p>
            </div>
        </div>
        <section class="for-slick position-relative px-2 px-md-5 mt-0 mb-0">
            <div class="container-fluid testimonial-arrow py-3">
                <div class="row responsive-testimonials">
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars">
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/dokter-ari.png" alt="user">
                                </div>
                                <h6> Carlos Martinelli </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars text-warning">
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/author-4.jpg" alt="user">
                                </div>
                                <h6> Eduard Mendy </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/author-5.jpg" alt="user">
                                </div>
                                <h6> Lucas Digne </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/author-7.jpg" alt="user">
                                </div>
                                <h6>Thomas Eristen</h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars">
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/dokter-ari.png" alt="user">
                                </div>
                                <h6> Carlos Martinelli </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars text-warning">
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/author-4.jpg" alt="user">
                                </div>
                                <h6> Eduard Mendy </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/author-5.jpg" alt="user">
                                </div>
                                <h6> Lucas Digne </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/author-7.jpg" alt=user"">
                                </div>
                                <h6>Thomas Eristen</h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars">
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star "></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/dokter-ari.png" alt="user">
                                </div>
                                <h6> Carlos Martinelli </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                    <div class="col  mx-md-2">
                        <a href="#" class="testi-card style-5">
                            <div class="stars text-warning">
                                <i class="fas fa-star "></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="text fs-15px">
                                “Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam, quia.”
                            </div>
                            <div class="user mt-40 text-center">
                                <div class="icon-80 rounded-circle img-cover overflow-hidden m-auto">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/author-4.jpg" alt="user">
                                </div>
                                <h6> Eduard Mendy </h6>
                                <small> Pengguna </small>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#fff" fill-opacity="1"
                  d="M0,256L48,240C96,224,192,192,288,176C384,160,480,160,576,176C672,192,768,224,864,213.3C960,203,1056,149,1152,117.3C1248,85,1344,75,1392,69.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
        </svg>
    </section>

    <section  id="harga">
        <section class="pricing style-10 mb-5 d-md-block d-none mt-0">
            <div class="container">
                <div class="section-head mb-4 style-6 text-center">
                    <h2 class="mb-3"> Solusi Harga
                        <span> <small> Terbaik </small> </span>
                    </h2>
                    <p class="color-666">Menyediakan Fitur Aplikasi Klinik Terbaik </p>
                </div>
                <div class="content">
                    <div class="pricing-table">
                        <div class="table-titles">
                            <div class="main-head d-flex justify-content-center align-items-center flex-column">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/money.png" alt="Logo Money">
                                </div>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                            <div class="main-body d-none d-lg-block">
                                <ul>
                                    <li> Reminder Otomatis via WhatsApp & Email</li>
                                    <li> Manajemen Reminder</li>
                                    <li> Kalender Reminder Klinik</li>
                                    <li> Kuota WA Chat/Bulan</li>
                                    <li> Manajemen Konsultasi</li>
                                    <li> Manajemen Tindakan</li>
                                    <li> Manajemen Kontrol</li>
                                    <li> Manajemen Follow Up</li>
                                    <li> Manajemen Data Rekam Medis</li>
                                    <li> Manajemen Kasir Klinik</li>

                                </ul>
                            </div>
                        </div>
                        <div class="table-body-card">
                            <div class="sub-head">
                                <p class="teks-biru mb-2"> Paket WhatsApp </p>
                                <h2> RP499,000 <small> /bulan </small></h2>
                                <a href="#" class="btn btn-icon-circle rounded-pill fw-bold brd-gray hover-orange2 button-harga">
                                    <small class="d-flex justify-content-center align-items-center text-light"> <i
                                                class="fab fa-whatsapp fs-5 me-2 pe-2 border-end"></i> Pilih Paket </small>
                                </a>
                            </div>
                            <div class="sub-body">
                                <ul>
                                    <li><strong class="fs-6"> Reminder Otomatis via WhatsApp & Email </strong> <i
                                                class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-6"> Manajemen Reminder </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Kalender Reminder Klinik </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Kuota Wa Chat/Bulan </strong> 170 Chat/Bulan</li>
                                    <li><strong class="fs-6"> Manajemen Konsultasi </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-6"> Manajemen Tindakan </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-6"> Manajemen Kontrol </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-6"> Manajemen Follow Up </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-6"> Manajemen Data Rekam Medis </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-6"> Manajemen Kasir Klinik </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="table-body-card">
                            <div class="sub-head">
                                <p class="teks-biru mb-2"> Paket Operasional </p>
                                <h2> RP599,000 <small> /bulan </small></h2>
                                <a href="#" class="btn btn-icon-circle rounded-pill fw-bold brd-gray hover-orange2 button-harga">
                                    <small class="d-flex justify-content-center align-items-center text-light"> <i
                                                class="fab fa-whatsapp fs-5 me-2 pe-2 border-end"></i> Pilih Paket </small>
                                </a>
                            </div>
                            <div class="sub-body">
                                <ul>
                                    <li><strong class="fs-6"> Reminder Otomatis Pasien via WhatsApp </strong> <i
                                                class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-6"> Manajemen Reminder </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Kalender Reminder Klinik </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Kuota Wa Chat/Bulan </strong> 270 Chat/Bulan</li>
                                    <li><strong class="fs-6"> Manajemen Konsultasi </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Tindakan </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Kontrol </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Follow Up </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Data Rekam Medis </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-6"> Manajemen Kasir Klinik </strong> <i
                                                class="fa fa-close text-danger fs-5"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="table-body-card recommended-card bg-harga-laris">
                            <div class="sub-head">
                                <p class="text-white mb-2"> Paket Lengkap </p>
                                <h2> RP699,000 <small> /bulan </small></h2>
                                <a href="#" class="btn btn-icon-circle rounded-pill fw-bold brd-gray bg-orange2 border-0 mt-min-25px">
                                    <small class="d-flex justify-content-center align-items-center"> <i
                                                class="fab fa-whatsapp fs-5 me-2 pe-2 border-end border-dark"></i> Pilih Paket
                                    </small>
                                </a>
                            </div>
                            <div class="sub-body border-harga-laris">
                                <ul class="bg-harga-laris">
                                    <li><strong class="fs-6"> Reminder Otomatis Pasien via WhatsApp </strong> <i
                                                class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-6"> Manajemen Reminder </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Kalender Reminder Klinik </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Kuota Wa Chat/Bulan </strong> 400 Chat/Bulan</li>
                                    <li><strong class="fs-6"> Manajemen Konsultasi </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Tindakan </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Kontrol </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Follow Up </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Data Rekam Medis </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                    <li><strong class="fs-6"> Manajemen Kasir Klinik </strong> <i class="fa fa-check hijau-centang"></i>
                                    </li>
                                </ul>
                            </div>
                            <div class="diskon">
                                    <span>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="pricing style-10 mb-5 d-flex d-md-none">
            <div class="container pt-40">
                <div class="text-center">
                    <p class=" mb-10"> Menyediakan Fitur Aplikasi Klinik Terbaik </p>
                    <h2> Solusi Harga <span class="teks-biru">Terbaik</span></h2>
                </div>
                <div class="content mt-4">
                    <div class="pricing-table harga harga-arrow harga-terbaik justify-content-center">
                        <div class="table-body-card mt-0 mx-2">
                            <div class="sub-head">
                                <p class="teks-biru mb-2"> Paket WhatsApp </p>
                                <h2 class="fs-25px"> RP499,000 <small> /bulan </small></h2>
                                <button class="btn rounded-pill brd-gray hover-orange2 text-light fs-12px bg-hijau-wa"><i
                                            class="bi bi-whatsapp me-2 pe-2 border-end"></i> <b>Pilih Paket</b></button>
                            </div>
                            <div class="sub-body">
                                <ul>
                                    <li><strong class="fs-13-5px"> Reminder Otomatis via
                                            WhatsApp & Email </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Reminder </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Kalender Reminder
                                            Klinik </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Kuota Wa
                                            Chat/Bulan </strong> 170 Chat/Bulan
                                    </li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Konsultasi </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Tindakan </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Kontrol </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Follow
                                            Up </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Data Rekam
                                            Medis </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Kasir
                                            Klinik </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="table-body-card mt-0 mx-2">
                            <div class="sub-head">
                                <p class="teks-biru mb-2"> Paket Operasional </p>
                                <h2 class="fs-25px"> RP599,000 <small> /bulan </small></h2>
                                <button class="btn rounded-pill brd-gray hover-orange2 text-light fs-12px bg-hijau-wa"><i
                                            class="bi bi-whatsapp me-2 pe-2 border-end"></i> <b>Pilih Paket</b></button>
                            </div>
                            <div class="sub-body">
                                <ul>
                                    <li><strong class="fs-13-5px"> Reminder Otomatis Pasien
                                            via WhatsApp </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Reminder </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Kalender Reminder
                                            Klinik </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Kuota Wa
                                            Chat/Bulan </strong> 270 Chat/Bulan
                                    </li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Konsultasi </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Tindakan </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Kontrol </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Follow
                                            Up </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Data Rekam
                                            Medis </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Kasir
                                            Klinik </strong> <i class="fa fa-close text-danger fs-5"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="table-body-card recommended-card mt-0 mx-2 bg-harga-laris">
                            <div class="sub-head">
                                <p class="text-white mb-2"> Paket Lengkap </p>
                                <h2 class="fs-25px"> RP699,000 <small> /bulan </small></h2>
                                <button class="btn rounded-pill brd-gray bg-orange2 fs-12px"><i
                                            class="bi bi-whatsapp me-2 pe-2 border-end border-dark"></i> <b>Pilih Paket</b>
                                </button>
                            </div>
                            <div class="sub-body border-harga-laris">
                                <ul class="bg-harga-laris">
                                    <li><strong class="fs-13-5px"> Reminder Otomatis Pasien
                                            via WhatsApp </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Reminder </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Kalender Reminder
                                            Klinik </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Kuota Wa
                                            Chat/Bulan </strong> 400 Chat/Bulan
                                    </li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Konsultasi </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Tindakan </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen
                                            Kontrol </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Follow
                                            Up </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Data Rekam
                                            Medis </strong> <i class="fa fa-check hijau-centang"></i></li>
                                    <li><strong class="fs-13-5px"> Manajemen Kasir
                                            Klinik </strong> <i class="fa fa-check hijau-centang"></i></li>
                                </ul>
                            </div>
                            <div class="diskon-mobile">
                                    <span>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>


    <section class="for-slick tampilan-apk d-md-block d-none">
        <div class="container-fluid position-relative">
            <div class="section-head text-center mb-60 style-6">
                <h2> Daftar Tampilan Aplikasi <br> Reminder Otomatis via <span> <small>WhatsApp</small> </span></h2>
                <p> Daftar Tampilan Reminder Pasien </p>
            </div>
            <div class="row slick-tampilan daftar-tampilan">
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Appointment">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tambah-appointment.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tambah-appointment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Tambah Appointment">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-appointment.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-appointment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Appointment">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment-rumah-sunat-bali.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment-rumah-sunat-bali.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Appointment Rumah Sunat Bali">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Konsultasi">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-selesai.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Konsultasi Selesai">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-batal.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-batal.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Konsultasi Batal">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-konsultasi.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-konsultasi.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Konsultasi">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-detail-pasien.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-detail-pasien.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Konsultasi Detail Pasien">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tindakan.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Tindakan">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-data-tindakan-selesai.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-data-tindakan-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Data Tindakan Selesai">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-jadwal-kontrol-tindakan.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-jadwal-kontrol-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Jadwal Kontrol Tindakan">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-rekam-medis-tindakan.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-rekam-medis-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Rekam Medis Tindakan">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment-tindakan.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Payment Tindakan">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-tindakan.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Tindakan">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-tindakan.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Pasien Tindakan">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Kontrol">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol-selesai.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Kontrol Selesai">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-kontrol.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-kontrol.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Kontrol">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-kontrol.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-kontrol.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Pasien Kontrol">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-selesai.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Selesai">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-selesai.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Pasien Selesai">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Payment">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kalender-jadwal.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kalender-jadwal.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Kalender Jadwal">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-data-pasien.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-data-pasien.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Data Pasien">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pengaturan-reminder.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pengaturan-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Pengaturan Reminder">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-riwayat-reminder.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-riwayat-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Riwayat Reminder">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-reminder-history.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-reminder-history.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Reminder History">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pesan-reminder.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pesan-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Pesan Reminder">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-pesan-reminder.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-pesan-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Pesan Reminder">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-link-whatsapp.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-link-whatsapp.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Link Whatsapp">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-staff.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-staff.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Staff">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-role-user.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-role-user.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Role User">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-user.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-user.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat User">
                    </a>
                </div>
                <div class="col-3 mx-3">
                    <a href="<?php echo base_url() ?>assets/template_front/gambar/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-metode-tindakan.png" data-lightbox="image1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-metode-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Metode Tindakan">
                    </a>
                </div>
            </div>
            <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/macbook-frame-empty.png" class="position-absolute d-lg-block d-none comp mockup-macbook" alt="Gambar Mockup Komputer">
        </div>
    </section>
    <svg class="d-md-none d-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#f0eff5" fill-opacity="1"
              d="M0,256L48,240C96,224,192,192,288,176C384,160,480,160,576,176C672,192,768,224,864,213.3C960,203,1056,149,1152,117.3C1248,85,1344,75,1392,69.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
    </svg>


    <div class="text-center d-md-none d-block section-head style-5 pt-5 bg-services">
        <h2 class="fs-28px"> Daftar Tampilan Aplikasi Reminder Otomatis via <span> WhatsApp </span>
        </h2>
        <p>Daftar Tampilan Reminder Pasien</p>
    </div>
    <div class="screenshots style-4 d-md-none d-block">
        <div class="screenshots-slider style-4">
            <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                <div class="swiper-wrapper for-swiper-tampilan">
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-navbar.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-navbar.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Navbar">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-dashboard.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-dashboard.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Dashboard">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Appointment">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tambah-appointment.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tambah-appointment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Tambah Appointment">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment-rumah-sunat-bali.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-appointment-rumah-sunat-bali.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Appointment Rumah Sunat Bali">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-appointment.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-appointment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Appointment">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Konsultasi">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-selesai.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Konsultasi Selesai">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-batal.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-konsultasi-batal.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Konsultasi Batal">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-konsultasi.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-konsultasi.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Konsultasi">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-konsultasi.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-konsultasi.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Pasien Konsultasi">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tindakan.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Tindakan">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tindakan-selesai.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-tindakan-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Tindakan Selesai">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-jadwal-kontrol-tindakan.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-jadwal-kontrol-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Jadwal Kontrol Tindakan">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-rekam-medis-tindakan.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-rekam-medis-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Rekam Medis Tindakan">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment-tindakan.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Payment Tindakan">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-tindakan.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Tindakan">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-tindakan.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Pasien Tindakan">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Kontrol">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol-selesai.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kontrol-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Kontrol Selesai">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-kontrol.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-kontrol.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Kontrol">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-kontrol.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-kontrol.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Pasien Kontrol">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-selesai.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Selesai">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-selesai.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-detail-pasien-selesai.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Detail Pasien Selesai">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-payment.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Payment">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kalender-jadwal.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-kalender-jadwal.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Kalender Jadwal">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-data-pasien.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-data-pasien.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Data Pasien">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pengaturan-reminder.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pengaturan-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Pengaturan Reminder">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-riwayat-reminder.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-riwayat-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Riwayat Reminder">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pesan-reminder.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-pesan-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Pesan Reminder">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-pesan-reminder.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-edit-pesan-reminder.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Edit Pesan Reminder">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-link-whatsapp.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-link-whatsapp.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Link Whatsapp">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-staff.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-staff.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Staff">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-role-user.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-role-user.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Role User">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-user.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-user.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat User">
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-prev w-265-6px" data-swiper-slide-index="0">
                        <div class="img">
                            <a href="<?php echo base_url() ?>assets/template_front/gambar/hp/tampilan/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-metode-tindakan.png" data-lightbox="image1">
                                <img src="<?php echo base_url() ?>assets/template_front/gambar/hp/pameran/aplikasi-reminder-follow-up-pasien-via-whatsapp-saat-metode-tindakan.png" alt="Aplikasi Reminder Follow Up Pasien via Whatsapp Saat Metode Tindakan">
                            </a>
                        </div>
                    </div>
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
        </div>
        <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/hand.png" alt="Mockup Mobile" class="mob-hand">
    </div>

    <section class="blog section-padding bg-gray style-1 text-center" id="blog">
        <div class="container">
            <div class="row">
                <div class="col offset-lg-1">
                    <div class="section-head mb-60">
                        <h6 class="color-main text-uppercase wow fadeInUp for-blog">Blog</h6>
                        <h2 class="wow fadeInUp for-blog">
                            Latest Posts <span class="fw-normal">From Our Press</span>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="blog_slider">
                    <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                        <div class="swiper-wrapper for-swiper-blog">
                            <div class="swiper-slide swiper-slide-prev slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="detail-hub-pasien.html">news</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/1.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="detail-hub-pasien.html">Crypto Trends 2023</a></h6>
                                        <div class="auther">
                                            <span>
                                                <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user1.jpeg" alt="User">
                                                <small><a href="detail-hub-pasien.html">By Admin</a></small>
                                            </span>
                                            <span>
                                                <i class="bi bi-calendar2"></i>
                                                <small><a href="detail-hub-pasien.html">May 15, 2022</a></small>
                                            </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-active slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="detail-hub-pasien.html">technology</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/2.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="detail-hub-pasien.html">How To Become Web Developer</a></h6>
                                        <div class="auther">
                                            <span>
                                                <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user2.jpeg" alt="User">
                                                <small><a href="detail-hub-pasien.html">By Moussa</a></small>
                                            </span>
                                            <span>
                                                <i class="bi bi-calendar2"></i>
                                                <small><a href="detail-hub-pasien.html">May 15, 2022</a></small>
                                            </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-next slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="detail-hub-pasien.html">tips &amp; tricks</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/3.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="detail-hub-pasien.html">Wireframe For UI/UX?</a></h6>
                                        <div class="auther">
                                            <span>
                                                <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user3.jpeg" alt="User">
                                                <small><a href="detail-hub-pasien.html">By Admin</a></small>
                                            </span>
                                            <span>
                                                <i class="bi bi-calendar2"></i>
                                                <small><a href="detail-hub-pasien.html">May 15, 2022</a></small>
                                            </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="detail-hub-pasien.html">news</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/4.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="detail-hub-pasien.html">VR Game, Opportunity &amp; Challenge</a></h6>
                                        <div class="auther">
                                            <span>
                                                <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user1.jpeg" alt="User">
                                                <small><a href="detail-hub-pasien.html">By David</a></small>
                                            </span>
                                            <span>
                                                <i class="bi bi-calendar2"></i>
                                                <small><a href="detail-hub-pasien.html">May 15, 2022</a></small>
                                            </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="detail-hub-pasien.html">technology</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/2.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="detail-hub-pasien.html">How To Become Web Developer</a></h6>
                                        <div class="auther">
                                            <span>
                                                <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user2.jpeg" alt="User">
                                                <small><a href="detail-hub-pasien.html">By Moussa</a></small>
                                            </span>
                                            <span>
                                                <i class="bi bi-calendar2"></i>
                                                <small><a href="detail-hub-pasien.html">May 15, 2022</a></small>
                                            </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                    <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"></div>
                    <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"></div>
                </div>
            </div>
        </div>
        <a href="blog-hub-pasien.html" class="btn rounded-pill blue5-3Dbutn hover-blue3 sm-butn fw-bold mt-4 fs-16px">
            <span> Lihat Semua </span>
        </a>
    </section>

    <section class="chat-banner style-7">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-3 col-sm-6 d-none d-lg-block">
                    <div class="img img1">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/vector1.png" alt="Vector 1">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="info">
                        <h3> Segera Optimalkan Klinik Anda </h3>
                        <p>dengan klik tombol dibawah untuk melakukan registrasi dan segera dapatkan akun
                            Aplikasinya</p>
                        <div class="btns mt-50">
                            <a href="https://api.whatsapp.com/send?phone=6281237376068&text=Hello%20Bapak%2FIbu%2C%20%F0%9F%99%82%0ATolong%20isi%20form%20dibawah%20untuk%20Registrasi%20%3A%0ANama%20Anda%20%3A%20%0ANama%20Klinik%20%3A%20%0AAlamat%20Klinik%20%3A%20%0APertanyaan%20%3A%20%0A%0ATerima%20Kasih%20%F0%9F%99%8F%F0%9F%8F%BB"
                               class="btn btn-icon-circle rounded-pill bg-black fw-bold text-dark me-4 mb-3 mb-lg-0 bg-emas">
                                <small class="d-flex justify-content-center align-items-center"> <i
                                            class="fab fa-whatsapp fs-5 me-2 pe-2 border-end border-dark"></i> Klik Disini
                                    Untuk Registrasi <i class="fas fa-long-arrow-alt-right"></i> </small>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="img img2">
                        <img src="<?php echo base_url() ?>assets/template_front/gambar/reminder-pasien/vector2.png" alt="Vector 2">
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

