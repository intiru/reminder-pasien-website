
<main class="blog-page style-5">


    <section class="blog-slider pt-50 pb-50 style-1">
        <div class="container">
            <div class="section-head text-center mb-60 style-5">
                <h2 class="mb-20"> Our <span> Journal </span> </h2>
                <div class="text color-666">Get the latest articles from our journal, writing, discuss and share</div>
            </div>
            <div class="blog-details-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="content-card">
                                <div class="img overlay">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/s_blog.png" alt="Gambar Blog">
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="cont">
                                                <small class="date small mb-20"> <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3"> News </a> <i class="far fa-clock me-1"></i> Posted on <a href="<?php echo base_url('blog') ?>">3 Days ago</a> </small>
                                                <h2 class="title">
                                                    <a href="<?php echo base_url('blog/detail/1') ?>">Solutions For Big Data Issue, Expert Perspective</a>
                                                </h2>
                                                <p class="fs-13px mt-10 text-light text-info">If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content-card">
                                <div class="img overlay">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/1.jpeg" alt="Gambar Blog">
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="cont">
                                                <small class="date small mb-20"> <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3"> News </a> <i class="far fa-clock me-1"></i> Posted on <a href="<?php echo base_url('blog') ?>">3 Days ago</a> </small>
                                                <h2 class="title">
                                                    <a href="<?php echo base_url('blog/detail/1') ?>">Solutions For Big Data Issue, Expert Perspective</a>
                                                </h2>
                                                <p class="fs-13px mt-10 text-light text-info">If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content-card">
                                <div class="img overlay">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/2.jpeg" alt="Gambar Blog">
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="cont">
                                                <small class="date small mb-20"> <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3"> News </a> <i class="far fa-clock me-1"></i> Posted on <a href="<?php echo base_url('blog') ?>">3 Days ago</a> </small>
                                                <h2 class="title">
                                                    <a href="<?php echo base_url('blog/detail/1') ?>">Solutions For Big Data Issue, Expert Perspective</a>
                                                </h2>
                                                <p class="fs-13px mt-10 text-light text-info">If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </section>


    <section class="blog style-1 text-center bg-white py-1">
        <div class="container">
            <div class="row">
                <div class="col offset-lg-1">
                    <div class="section-head mb-60">
                        <h2 class="wow fadeInUp for-blog">
                            Popular <span class="fw-normal">Posts</span>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="blog_slider">
                    <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                        <div class="swiper-wrapper for-swiper-blog">
                            <div class="swiper-slide swiper-slide-prev slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="<?php echo base_url('blog/detail/1') ?>">news</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/1.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="<?php echo base_url('blog/detail/1') ?>">Crypto Trends 2023</a></h6>
                                        <div class="auther">
                                                <span>
                                                    <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user1.jpeg" alt="User">
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">By Admin</a></small>
                                                </span>
                                            <span>
                                                    <i class="bi bi-calendar2"></i>
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">May 15, 2022</a></small>
                                                </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-active slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="<?php echo base_url('blog/detail/1') ?>">technology</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/2.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="<?php echo base_url('blog/detail/1') ?>">How To Become Web Developer</a></h6>
                                        <div class="auther">
                                                <span>
                                                    <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user2.jpeg" alt="User">
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">By Moussa</a></small>
                                                </span>
                                            <span>
                                                    <i class="bi bi-calendar2"></i>
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">May 15, 2022</a></small>
                                                </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-next slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="<?php echo base_url('blog/detail/1') ?>">tips &amp; tricks</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/3.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="<?php echo base_url('blog/detail/1') ?>">Wireframe For UI/UX?</a></h6>
                                        <div class="auther">
                                                <span>
                                                    <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user3.jpeg" alt="User">
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">By Admin</a></small>
                                                </span>
                                            <span>
                                                    <i class="bi bi-calendar2"></i>
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">May 15, 2022</a></small>
                                                </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="<?php echo base_url('blog/detail/1') ?>">news</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/4.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="<?php echo base_url('blog/detail/1') ?>">VR Game, Opportunity &amp; Challenge</a></h6>
                                        <div class="auther">
                                                <span>
                                                    <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user1.jpeg" alt="User">
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">By David</a></small>
                                                </span>
                                            <span>
                                                    <i class="bi bi-calendar2"></i>
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">May 15, 2022</a></small>
                                                </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slide-blog">
                                <div class="blog_box">
                                    <div class="tags">
                                        <a href="<?php echo base_url('blog/detail/1') ?>">technology</a>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/2.jpeg" alt="Gambar Blog">
                                    </div>
                                    <div class="info">
                                        <h6><a href="<?php echo base_url('blog/detail/1') ?>">How To Become Web Developer</a></h6>
                                        <div class="auther">
                                                <span>
                                                    <img class="auther-img" src="<?php echo base_url() ?>assets/template_front/gambar/blog/user2.jpeg" alt="User">
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">By Moussa</a></small>
                                                </span>
                                            <span>
                                                    <i class="bi bi-calendar2"></i>
                                                    <small><a href="<?php echo base_url('blog/detail/1') ?>">May 15, 2022</a></small>
                                                </span>
                                        </div>
                                        <div class="text">
                                            If there’s one way that wireless technology has changed the way we work, it’s that will everyone is now connected [...]
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                    <!-- ====== slider navigation ====== -->
                    <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"></div>
                    <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"></div>
                </div>
            </div>
        </div>
    </section>


    <section class="all-news section-padding blog bg-transparent style-3">
        <div class="container">
            <div class="row gx-4 gx-lg-5">
                <div class="col-lg-8">
                    <div class="card border-0 bg-transparent rounded-0 border-bottom brd-gray pb-30 mb-30">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img img-cover">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/6.png" class="radius-7" alt="...">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body p-0">
                                    <small class="d-block date text">
                                        <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3 color-blue5 fw-bold"> news </a>
                                        <i class="bi bi-clock me-1"></i>
                                        <a href="<?php echo base_url('blog') ?>" class="op-8">12 Days ago</a>
                                    </small>
                                    <a href="<?php echo base_url('blog/detail/1') ?>" class="card-title mb-10">How To Become A Python Develop Expert</a>
                                    <p class="fs-13px color-666">If there’s one way that wireless technology has changed the way we work, it’s that will everyone [...]</p>
                                    <div class="auther-comments d-flex small align-items-center justify-content-between op-9">
                                        <div class="l_side d-flex align-items-center">
                                                <span
                                                    class="icon-10 rounded-circle d-inline-flex justify-content-center align-items-center text-uppercase bg-blue5 p-2 me-2 text-white">
                                                    a
                                                </span>
                                            <a href="<?php echo base_url('blog') ?>">
                                                <small class="text-muted">By</small> Admin
                                            </a>
                                        </div>
                                        <div class="r-side mt-1">
                                            <i class="bi bi-chat-left-text me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">24</a>
                                            <i class="bi bi-eye ms-4 me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">774k</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 bg-transparent rounded-0 border-bottom brd-gray pb-30 mb-30">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img img-cover">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/4.jpeg" class="radius-7" alt="...">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body p-0">
                                    <small class="d-block date text">
                                        <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3 color-blue5 fw-bold">news</a>
                                        <i class="bi bi-clock me-1"></i>
                                        <a href="<?php echo base_url('blog') ?>" class="op-8">12 Days ago</a>
                                    </small>
                                    <a href="<?php echo base_url('blog/detail/1') ?>" class="card-title mb-10">VR Game, Oppoturnity & Challenge</a>
                                    <p class="fs-13px color-666">If there’s one way that wireless technology has changed the way we work, it’s that will everyone [...]</p>
                                    <div class="auther-comments d-flex small align-items-center justify-content-between op-9">
                                        <div class="l_side d-flex align-items-center">
                                                <span
                                                    class="icon-10 rounded-circle d-inline-flex justify-content-center align-items-center text-uppercase bg-blue5 p-2 me-2 text-white">
                                                    a
                                                </span>
                                            <a href="<?php echo base_url('blog') ?>">
                                                <small class="text-muted">By</small> Admin
                                            </a>
                                        </div>
                                        <div class="r-side mt-1">
                                            <i class="bi bi-chat-left-text me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">24</a>
                                            <i class="bi bi-eye ms-4 me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">774k</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 bg-transparent rounded-0 border-bottom brd-gray pb-30 mb-30">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img img-cover">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/10.png" class="radius-7" alt="...">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body p-0">
                                    <small class="d-block date text">
                                        <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3 color-blue5 fw-bold">tips
                                            & tricks</a>
                                        <i class="bi bi-clock me-1"></i>
                                        <a href="<?php echo base_url('blog') ?>" class="op-8">12 Days ago</a>
                                    </small>
                                    <a href="<?php echo base_url('blog/detail/1') ?>" class="card-title mb-10">6 Tips To Help You Build Your Social Media Effeciency & Better</a>
                                    <p class="fs-13px color-666">If there’s one way that wireless technology has changed the way we work, it’s that will everyone [...]</p>
                                    <div class="auther-comments d-flex small align-items-center justify-content-between op-9">
                                        <div class="l_side d-flex align-items-center">
                                                <span
                                                    class="icon-10 rounded-circle d-inline-flex justify-content-center align-items-center text-uppercase bg-blue5 p-2 me-2 text-white">
                                                    a
                                                </span>
                                            <a href="<?php echo base_url('blog') ?>">
                                                <small class="text-muted">By</small> Admin
                                            </a>
                                        </div>
                                        <div class="r-side mt-1">
                                            <i class="bi bi-chat-left-text me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">24</a>
                                            <i class="bi bi-eye ms-4 me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">774k</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 bg-transparent rounded-0 border-bottom brd-gray pb-30 mb-30">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img img-cover">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/11.png" class="radius-7" alt="...">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body p-0">
                                    <small class="d-block date text">
                                        <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3 color-blue5 fw-bold">tips
                                            & tricks</a>
                                        <i class="bi bi-clock me-1"></i>
                                        <a href="<?php echo base_url('blog') ?>" class="op-8">12 Days ago</a>
                                    </small>
                                    <a href="<?php echo base_url('blog/detail/1') ?>" class="card-title mb-10">The New Trend Of Marketing With Tiktok, Should Or Not?</a>
                                    <p class="fs-13px color-666">If there’s one way that wireless technology has changed the way we work, it’s that will everyone [...]</p>
                                    <div class="auther-comments d-flex small align-items-center justify-content-between op-9">
                                        <div class="l_side d-flex align-items-center">
                                                <span
                                                    class="icon-10 rounded-circle d-inline-flex justify-content-center align-items-center text-uppercase bg-blue5 p-2 me-2 text-white">
                                                    a
                                                </span>
                                            <a href="<?php echo base_url('blog') ?>">
                                                <small class="text-muted">By</small> Admin
                                            </a>
                                        </div>
                                        <div class="r-side mt-1">
                                            <i class="bi bi-chat-left-text me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">24</a>
                                            <i class="bi bi-eye ms-4 me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">774k</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 bg-transparent rounded-0 border-bottom brd-gray pb-30 mb-30">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img img-cover">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/9.png" class="radius-7" alt="...">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body p-0">
                                    <small class="d-block date text">
                                        <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3 color-blue5 fw-bold">tips
                                            & tricks</a>
                                        <i class="bi bi-clock me-1"></i>
                                        <a href="<?php echo base_url('blog') ?>" class="op-8">12 Days ago</a>
                                    </small>
                                    <a href="<?php echo base_url('blog/detail/1') ?>" class="card-title mb-10">Workflow Strategy For E-Shop</a>
                                    <p class="fs-13px color-666">If there’s one way that wireless technology has changed the way we work, it’s that will everyone [...]</p>
                                    <div class="auther-comments d-flex small align-items-center justify-content-between op-9">
                                        <div class="l_side d-flex align-items-center">
                                                <span
                                                    class="icon-10 rounded-circle d-inline-flex justify-content-center align-items-center text-uppercase bg-blue5 p-2 me-2 text-white">
                                                    a
                                                </span>
                                            <a href="<?php echo base_url('blog') ?>">
                                                <small class="text-muted">By</small> Admin
                                            </a>
                                        </div>
                                        <div class="r-side mt-1">
                                            <i class="bi bi-chat-left-text me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">24</a>
                                            <i class="bi bi-eye ms-4 me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">774k</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 bg-transparent rounded-0 border-bottom brd-gray pb-30 mb-30">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img img-cover">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/3.jpeg" class="radius-7" alt="...">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body p-0">
                                    <small class="d-block date text">
                                        <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3 color-blue5 fw-bold">tips
                                            & tricks</a>
                                        <i class="bi bi-clock me-1"></i>
                                        <a href="<?php echo base_url('blog') ?>" class="op-8">12 Days ago</a>
                                    </small>
                                    <a href="<?php echo base_url('blog/detail/1') ?>" class="card-title mb-10">Wireframe For UI/UX</a>
                                    <p class="fs-13px color-666">If there’s one way that wireless technology has changed the way we work, it’s that will everyone [...]</p>
                                    <div class="auther-comments d-flex small align-items-center justify-content-between op-9">
                                        <div class="l_side d-flex align-items-center">
                                                <span
                                                    class="icon-10 rounded-circle d-inline-flex justify-content-center align-items-center text-uppercase bg-blue5 p-2 me-2 text-white">
                                                    a
                                                </span>
                                            <a href="<?php echo base_url('blog') ?>">
                                                <small class="text-muted">By</small> Admin
                                            </a>
                                        </div>
                                        <div class="r-side mt-1">
                                            <i class="bi bi-chat-left-text me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">24</a>
                                            <i class="bi bi-eye ms-4 me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">774k</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 bg-transparent rounded-0 pb-30 mb-30 mb-lg-0 pb-lg-0">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img img-cover">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/12.png" class="radius-7" alt="...">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="card-body p-0">
                                    <small class="d-block date text">
                                        <a href="<?php echo base_url('blog') ?>" class="text-uppercase border-end brd-gray pe-3 me-3 color-blue5 fw-bold">tips
                                            & tricks</a>
                                        <i class="bi bi-clock me-1"></i>
                                        <a href="<?php echo base_url('blog') ?>" class="op-8">12 Days ago</a>
                                    </small>
                                    <a href="<?php echo base_url('blog/detail/1') ?>" class="card-title mb-10">Freelancer Days 2022, What’s new?</a>
                                    <p class="fs-13px color-666">If there’s one way that wireless technology has changed the way we work, it’s that will everyone [...]</p>
                                    <div class="auther-comments d-flex small align-items-center justify-content-between op-9">
                                        <div class="l_side d-flex align-items-center">
                                                <span
                                                    class="icon-10 rounded-circle d-inline-flex justify-content-center align-items-center text-uppercase bg-blue5 p-2 me-2 text-white">
                                                    a
                                                </span>
                                            <a href="<?php echo base_url('blog') ?>">
                                                <small class="text-muted">By</small> Admin
                                            </a>
                                        </div>
                                        <div class="r-side mt-1">
                                            <i class="bi bi-chat-left-text me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">24</a>
                                            <i class="bi bi-eye ms-4 me-1"></i>
                                            <a href="<?php echo base_url('blog') ?>">774k</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pagination style-5 color-5 justify-content-center mt-60">
                        <a href="<?php echo base_url('blog') ?>" class="active">
                            <span>1</span>
                        </a>
                        <a href="<?php echo base_url('blog') ?>">
                            <span>2</span>
                        </a>
                        <a href="<?php echo base_url('blog') ?>">
                            <span>3</span>
                        </a>
                        <a href="<?php echo base_url('blog') ?>">
                            <span>4</span>
                        </a>
                        <a href="<?php echo base_url('blog') ?>">
                            <span>...</span>
                        </a>
                        <a href="<?php echo base_url('blog') ?>">
                            <span>20</span>
                        </a>
                        <a href="<?php echo base_url('blog') ?>">
                            <span class="text">next <i class="fas fa-chevron-right"></i> </span>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="side-blog style-5 ps-lg-5 mt-5 mt-lg-0">

                        <form action="contact.php" class="search-form mb-50" method="post">
                            <h6 class="title mb-20 text-uppercase fw-normal">
                                search
                            </h6>
                            <div class="form-group position-relative">
                                <input type="text" class="form-control rounded-pill" placeholder="Type and hit enter">
                                <button class="search-btn border-0 bg-transparent"> <i class="fas fa-search"></i> </button>
                            </div>
                        </form>

                        <div class="side-recent-post mb-50">
                            <h6 class="title mb-20 text-uppercase fw-normal">
                                recent post
                            </h6>
                            <a href="<?php echo base_url('blog/detail/1') ?>" class="post-card pb-3 mb-3 border-bottom brd-gray">
                                <div class="img me-3">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/1.jpeg" alt="Gambar Blog">
                                </div>
                                <div class="inf">
                                    <h6> Crypto Trend 2023 </h6>
                                    <p> If there’s one way that wireless technology has [...] </p>
                                </div>
                            </a>
                            <a href="<?php echo base_url('blog/detail/1') ?>" class="post-card pb-3 mb-3 border-bottom brd-gray">
                                <div class="img me-3">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/2.jpeg" alt="Gambar Blog">
                                </div>
                                <div class="inf">
                                    <h6> How To Become Web Developer </h6>
                                    <p> If there’s one way that wireless technology has [...] </p>
                                </div>
                            </a>
                            <a href="<?php echo base_url('blog/detail/1') ?>" class="post-card pb-3 mb-3 border-bottom brd-gray">
                                <div class="img me-3">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/3.jpeg" alt="Gambar Blog">
                                </div>
                                <div class="inf">
                                    <h6> Wireframe for UI/UX </h6>
                                    <p> If there’s one way that wireless technology has [...] </p>
                                </div>
                            </a>
                            <a href="<?php echo base_url('blog/detail/1') ?>" class="post-card">
                                <div class="img me-3">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/7.png" alt="Gambar Blog">
                                </div>
                                <div class="inf">
                                    <h6> AI With Fingerprint </h6>
                                    <p> If there’s one way that wireless technology has [...] </p>
                                </div>
                            </a>
                        </div>

                        <div class="side-categories mb-50">
                            <h6 class="title mb-20 text-uppercase fw-normal">
                                categories
                            </h6>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item">
                                <span> all </span>
                                <span> 265 </span>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item">
                                <span> News </span>
                                <span> 38 </span>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item">
                                <span> Technology </span>
                                <span> 16 </span>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item">
                                <span> Tips & Tricks </span>
                                <span> 85 </span>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item">
                                <span> Career </span>
                                <span> 21 </span>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item">
                                <span> Community </span>
                                <span> 874 </span>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item">
                                <span> Videos </span>
                                <span> 54 </span>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="cat-item border-0">
                                <span> Others </span>
                                <span> 85 </span>
                            </a>
                        </div>

                        <div class="side-newsletter mb-50">
                            <h6 class="title mb-10 text-uppercase fw-normal">
                                newsletter
                            </h6>
                            <div class="text">
                                Register now to get latest updates on promotions & coupons.
                            </div>
                            <form action="contact.php" class="form-subscribe" method="post">
                                <div class="email-input d-flex align-items-center py-3 px-3 bg-white mt-3 radius-5">
                                        <span class="icon me-2 flex-shrink-0">
                                            <i class="far fa-envelope"></i>
                                        </span>
                                    <input type="text" placeholder="Email Address" class="border-0 bg-transparent fs-13px">
                                </div>
                                <button class="btn bg-blue5 sm-butn text-white hover-darkBlue w-100 mt-3 radius-5 py-3">
                                    <span>Subscribe</span>
                                </button>
                            </form>
                        </div>

                        <div class="side-share mb-50">
                            <h6 class="title mb-20 text-uppercase fw-normal">
                                social
                            </h6>
                            <a href="<?php echo base_url('blog') ?>" class="social-icon">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="social-icon">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="social-icon">
                                <i class="fab fa-pinterest"></i>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="social-icon">
                                <i class="fab fa-goodreads-g"></i>
                            </a>
                            <a href="<?php echo base_url('blog') ?>" class="social-icon">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>

                        <div class="side-insta mb-50">
                            <h6 class="title mb-20 text-uppercase fw-normal">
                                our instagram
                            </h6>
                            <div class="d-flex justify-content-between flex-wrap">
                                <a href="assets/gambar/blog/1.jpeg" class="insta-img img-cover" data-fancybox="gallery">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/1.jpeg" alt="Gambar Blog">
                                    <i class="fab fa-instagram icon"></i>
                                </a>
                                <a href="assets/gambar/blog/10.png" class="insta-img img-cover" data-fancybox="gallery">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/10.png" alt="Gambar Blog">
                                    <i class="fab fa-instagram icon"></i>
                                </a>
                                <a href="assets/gambar/blog/11.png" class="insta-img img-cover" data-fancybox="gallery">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/11.png" alt="Gambar Blog">
                                    <i class="fab fa-instagram icon"></i>
                                </a>
                                <a href="assets/gambar/blog/12.png" class="insta-img img-cover" data-fancybox="gallery">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/12.png" alt="Gambar Blog">
                                    <i class="fab fa-instagram icon"></i>
                                </a>
                                <a href="assets/gambar/blog/2.jpeg" class="insta-img img-cover" data-fancybox="gallery">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/2.jpeg" alt="Gambar Blog">
                                    <i class="fab fa-instagram icon"></i>
                                </a>
                                <a href="assets/gambar/blog/3.jpeg" class="insta-img img-cover" data-fancybox="gallery">
                                    <img src="<?php echo base_url() ?>assets/template_front/gambar/blog/3.jpeg" alt="Gambar Blog">
                                    <i class="fab fa-instagram icon"></i>
                                </a>
                            </div>
                        </div>

                        <div class="side-tags">
                            <h6 class="title mb-20 text-uppercase fw-normal">
                                popular tags
                            </h6>
                            <div class="content">
                                <a href="<?php echo base_url('blog') ?>">WordPress</a>
                                <a href="<?php echo base_url('blog') ?>">PHP</a>
                                <a href="<?php echo base_url('blog') ?>">HTML/CSS</a>
                                <a href="<?php echo base_url('blog') ?>">Figma</a>
                                <a href="<?php echo base_url('blog') ?>">Technology</a>
                                <a href="<?php echo base_url('blog') ?>">Marketing</a>
                                <a href="<?php echo base_url('blog') ?>">Consultation</a>
                                <a href="<?php echo base_url('blog') ?>">Seo</a>
                                <a href="<?php echo base_url('blog') ?>">Envato</a>
                                <a href="<?php echo base_url('blog') ?>">Psd</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>