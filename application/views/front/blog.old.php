<div class="breadcrumb-area bg-black bg-relative">
    <div class="banner-bg-img"
         style="background-image: url('<?php echo base_url() ?>assets/template_front/img/bg/1.webp');"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner text-center">
                    <h2 class="page-title"><?php echo $page->title ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo site_url() ?>"><?php echo $top_home->title_menu ?></a></li>
                        <li><?php echo $page->title_menu ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blog-area pd-top-120 pd-bottom-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <?php foreach ($blog_list as $row) {
                    $permalink = $this->main->permalink([$top_blog->title_menu, $row->title]);
                    ?>
                    <div class="single-blog-inner">
                        <div class="thumb">
                            <a href="<?php echo $permalink ?>">
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                     alt="<?php echo $row->thumbnail_alt ?>">
                            </a>
                        </div>
                        <div class="details">
                            <h2><a href="<?php echo $permalink ?>"><?php echo $row->title ?></a></h2>
                            <ul class="blog-meta">
                                <li><i class="far fa-user"></i> <?= $dict_admin ?></li>
                                <li>
                                    <i class="far fa-calendar-alt"></i> <?php echo date('d F Y', strtotime($row->created_at)) ?>
                                </li>
                                <li><i class="far fa-eye"></i> <?php echo $row->views ?> Views</li>
                            </ul>
                            <p><?php echo $this->main->short_desc($row->description) ?></p>
                            <a class="read-more-text" href="<?php echo $permalink ?>"><?= $dict_read_more ?>
                                <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                <?php } ?>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="td-sidebar">
                    <div class="widget widget_search">
                        <h4 class="widget-title"><?= $dict_search ?></h4>
                        <form class="search-form" action="<?php echo $this->main->permalink([$top_blog->title_menu]) ?>" method="get">
                            <div class="form-group">
                                <input type="text" placeholder="Search here ..." value="<?php echo $keyword ?>">
                            </div>
                            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>