<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function product_1()
    {
        $data = $this->main->data_front('product_1');
        $data['page'] = $this
            ->db
            ->where(array('type' => 'product_1', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();

        $this->template->front('product_1', $data);
    }

    public function product_2()
    {
        $data = $this->main->data_front('product_2');
        $data['page'] = $this
            ->db
            ->where(array('type' => 'product_2', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();

        $this->template->front('product_2', $data);
    }

    public function product_3()
    {
        $data = $this->main->data_front('product_3');
        $data['page'] = $this
            ->db
            ->where(array('type' => 'product_3', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();

        $this->template->front('product_3', $data);
    }

    public function product_4()
    {
        $data = $this->main->data_front('product_4');
        $data['page'] = $this
            ->db
            ->where(array('type' => 'product_4', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();

        $this->template->front('product_4', $data);
    }

    public function product_5()
    {
        $data = $this->main->data_front('product_5');
        $data['page'] = $this
            ->db
            ->where(array('type' => 'product_5', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();

        $this->template->front('product_5', $data);
    }
}
