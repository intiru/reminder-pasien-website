<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('main');
    }

    public function index()
    {

        $client_list = [
            'frisdhy_angel' => [
                'title' => 'RSIA FRISDHY ANGEL',
                'nomer' => '6281237376068',
                'logo' => 'rsia-frisdhy-angel.png'
            ],
            'klinik_krakatau' => [
                'title' => 'KLINIK KRAKATAU',
                'nomer' => '6281237376068',
                'logo' => 'klinik-krakatau.png'
            ],
        ];
        $client_data = [];
        $client_status = FALSE;

        if (count($_GET) > 0) {
            $client_get = '';
            foreach ($_GET as $client => $empty) {
                $client_get = $client;
            }

            foreach ($client_list as $client => $data) {
                if (strpos($client_get, $client) !== FALSE) {
                    $client_data = $data;
                    $client_status = TRUE;
                    break;
                }
            }
        }


        $data = $this->main->data_front('home');

        $data['page'] = $this->db->where(array('type' => 'home', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['client_status'] = $client_status;
        $data['client_data'] = $client_data;


        $this->template->front('home_content', $data);
    }

    // function modal_announcement_hide()
    // {
    //     $this->session->set_userdata(array('modal_announcement_status' => 'hide'));
    // }
}
