<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front('about_us');
        $data['page'] = $this
            ->db
            ->where(array('type' => 'about_us', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();
        $data['page_list'] = json_decode($data['page']->data_1, TRUE);

        $data['about_allianz_insurance'] = $this
        ->db
        ->where(array(
            'type' => 'about_allianz_insurance',
            'id_language' => $data['id_language']
        ))
        ->get('pages')
        ->row();
        $data['about_help_insurance'] = $this
            ->db
            ->where(array(
                'type' => 'about_help_insurance',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        $data['home_our_customers'] = $this
            ->db
            ->where(array(
                'type' => 'home_our_customers',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        $data['home_our_customers_list'] = json_decode($data['home_our_customers']->data_1, TRUE);
        $data['about_faq'] = $this
            ->db
            ->where(array(
                'type' => 'about_faq',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['about_owner'] = $this
            ->db
            ->where(array('type' => 'about_owner', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();
        $data['daftar_owner'] = $this
            ->db
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language']
            ))
            ->get('about_owner')
            ->result();

        $this->template->front('about_us', $data);
    }

    public function team($id)
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where('id', $id)
            ->get('team')
            ->row();
        $data['page']->type = 'profile';

        $this->template->front('team_detail', $data);
    }
}
