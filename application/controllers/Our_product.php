<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Our_product extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front('our_product');
        $page = $this
            ->db
            ->where(array('id_language' => $data['id_language']))
            ->where_in('type', array('our_product'))
            ->get('pages')
            ->row();
        $product_list = $this
            ->db
            ->where(['id_language' => $data['id_language']])
            ->where('use', 'yes')
            ->get('category_product')
            ->result();
        $data['page'] = $page;
        $data['product_list'] = $product_list;
        $this->template->front('our_product', $data);
    }

    public function detail($id = '')
    {
        $data = $this->main->data_front('our_product');
        $data['page'] = $this
            ->db
            ->where('id', $id)
            ->get('category_product')
            ->row();
        $data['page']->type = 'our_product';

        $this->template->front('product_detail', $data);
    }
}
