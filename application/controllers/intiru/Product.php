<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model(array('m_tour', 'm_category'));
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {
        $data = $this->main->data_main();
        $data['product'] = $this->db
            ->select('t.*, c.title AS category_title')
            ->where('t.id_language', $data['id_language'])
            ->join('category c', 'c.id = t.id_category', 'left')
            ->get('product t')
            ->result();

        $top_menu_list = $this
            ->db
            ->where(array(
                'data_type' => 'top_menu'
            ))
            ->order_by('title', 'ASC')
            ->get('product')
            ->result();

        foreach ($top_menu_list as $row) {
            $row->sub_menu_list = $this
                ->db
                ->where(array(
                    'data_type' => 'sub_menu_1',
                    'id_parent' => $row->id
                ))
                ->order_by('title', 'ASC')
                ->get('product')
                ->result();

            $row->product_list = $this
                ->db
                ->where(array(
                    'data_type' => 'product_description',
                    'id_parent' => $row->id
                ))
                ->order_by('title', 'ASC')
                ->get('product')
                ->result();

            foreach ($row->sub_menu_list as $row_sub_menu) {
                $row_sub_menu->product_list = $this
                    ->db
                    ->where(array(
                        'data_type' => 'product_description',
                        'id_parent' => $row_sub_menu->id
                    ))
                    ->order_by('title', 'ASC')
                    ->get('product')
                    ->result();

                foreach ($row_sub_menu->product_list as $row_descriptions) {
                    $id_parent_2 = $row_descriptions->id_parent;
                    $id_parent = $this
                        ->db
                        ->select('id_parent')
                        ->where('id', $id_parent_2)
                        ->get('product')
                        ->row()
                        ->id_parent;

                    $row_descriptions->id_parent = $id_parent;
                    $row_descriptions->id_parent_2 = $id_parent_2;

                }

            }
        }

//        echo json_encode($top_menu_list);
//        exit;

        $data['top_menu_list'] = $top_menu_list;
        $data['category'] = $this->m_category->get_where(array('use' => 'yes', 'id_language' => $data['id_language']))->result();
        $data['top_menu'] = $this->db->where('data_type', 'top_menu')->order_by('title', 'ASC')->get('product')->result();

        $this->template->set('tour', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Manajemen Produk Asuransi');
        $this->template->load_admin('product/index', $data);
    }

    public function sub_menu($id)
    {
        $sub_menu = $this
            ->db
            ->where(array(
                'data_type' => 'sub_menu_1',
                'id_parent' => $id
            ))
            ->order_by('title', 'ASC')
            ->get('product')
            ->result();

        echo json_encode($sub_menu);
    }

    public function createprocess()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Service Title', 'required');
//		$this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alternative', 'required');
//		$this->form_validation->set_rules('meta_title', 'Meta title', 'required');
//		$this->form_validation->set_rules('meta_description', 'Meta description', 'required');
//		$this->form_validation->set_rules('meta_keywords', 'meta_keywords', 'required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                    'thumbnail_alt' => form_error('thumbnail_alt'),
                    'description' => form_error('description'),
                    'meta_title' => form_error('meta_title'),
                    'meta_description' => form_error('meta_description'),
                    'meta_keywords' => form_error('meta_keywords'),
                )
            ));
        } else {

            $data = $this->input->post(NULL, TRUE);
unset($data['submit']);
unset($data['submit']);
            $data['description'] = $this->input->post('description', TRUE);
            if ($data['id_parent_2'] != 0) {
                $data['id_parent'] = $data['id_parent_2'];
            }

            unset($data['id_parent_2']);


            if ($_FILES['thumbnail']['name']) {
                $response = $this->main->upload_file_thumbnail('thumbnail', $this->input->post('title'));
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'thumbnail' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['thumbnail'] = $response['filename'];
                }
            }


            $this->db->insert('product', $data);

            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
            ));
        }
    }

    public function delete($id)
    {
        $this->db->where('id', $id)->delete('product');
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Service Title', 'required');
//		$this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alternative', 'required');
//        $this->form_validation->set_rules('meta_title', 'Meta title', 'required');
//        $this->form_validation->set_rules('meta_description', 'Meta description', 'required');
//        $this->form_validation->set_rules('meta_keywords', 'meta_keywords', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
//                    'thumbnail_alt' => form_error('thumbnail_alt'),
//                    'description' => form_error('description'),
//                    'meta_title' => form_error('meta_title'),
//                    'meta_description' => form_error('meta_description'),
//                    'meta_keywords' => form_error('meta_keywords'),
                )
            ));
        } else {
            $id = $this->input->post('id');
            $data = $this->input->post(NULL, TRUE);
unset($data['submit']);
            $where = array(
                'id' => $id
            );

            if ($_FILES['thumbnail']['name']) {
                $response = $this->main->upload_file_thumbnail('thumbnail', $this->input->post('title'));
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'thumbnail' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['thumbnail'] = $response['filename'];
                }
            }

            if ($data['id_parent_2'] != 0) {
                $data['id_parent'] = $data['id_parent_2'];
            }

            unset($data['id_parent_2']);

            $this->db->where('id', $id)->update('product', $data);


            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diperbarui',
            ));

        }
    }

}
