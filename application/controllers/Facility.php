<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facility extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('main');
	}

	public function index()
	{
		$data = $this->main->data_front('price_list');
		$data['page'] = $this->db->where(array('type' => 'facility', 'id_language' => $data['id_language']))->get('pages')->row();

		$data['category_facility'] = $this->db->where(array('use' => 'yes', 'id_language' => $data['id_language']))->get('category_facility')->result();
		$this->template->front('facility', $data);
	}
	
	public function test() {
	
		return '';
	}
}
