<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Quote extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front('quote');
        $data['page'] = $this
            ->db
            ->where(array('type' => 'quote', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();
        $data['page_list'] = json_decode($data['page']->data_1, TRUE);
        $data['about_owner'] = $this
            ->db
            ->where(array('type' => 'about_owner', 'id_language' => $data['id_language']))
            ->get('pages')
            ->row();
        $data['daftar_owner'] = $this
            ->db
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language']
            ))
            ->get('about_owner')
            ->result();

        $this->template->front('quote', $data);
    }

    public function send()
    {
        $name = $this->input->post('name');
        $birthdate = $this->input->post('birthdate');
        $gender = $this->input->post('gender');
        $marital = $this->input->post('marital');
        $country = $this->input->post('country');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $whatsapp = $this->input->post('whatsapp');
        $product = $this->input->post('product');
        $pesan = $this->input->post('pesan');
        $person_add = $this->input->post('person_add');

        $person_name_arr = $this->input->post('person_name');
        $person_birthdate_arr = $this->input->post('person_birthdate');
        $person_member_arr = $this->input->post('person_member');
        $no = 1;

        $message = 'Hello Admin,
I send you a quote insurance, I Hope you can help me

Personal Detail       
- Name : ' . $name . '
- Date of Birth : ' . $birthdate . '
- Gender : ' . $gender . '
- Marital Status : ' . $marital . '
- Country : ' . $country . '
- Email : ' . $email . '
- Telephone : ' . $phone . '
- WhatsApp : ' . $whatsapp . '
- Product : ' . $product . '
- Message : ' . $pesan;

        if (count($person_name_arr) > 0) {

            $message .= ' 
- Person Count Added: ' . $person_add . '
            
Person Added';
            foreach ($person_name_arr as $key => $person_name) {
                $person_birthdate = $person_birthdate_arr[$key];
                $person_member = $person_member_arr[$key];
                $message .= '
#Person ' . $no . '
- Name : ' . $person_name . '
- Date of Birth : ' . $person_birthdate . '
- Family Member : ' . $person_member . '
';

                $no++;
            }

        }

        $message .= '
Thank You';

        redirect('https://wa.me/6287777118807?text=' . urlencode($message));
    }

    public function send_email()
    {
        error_reporting(0);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama', 'required');
        $this->form_validation->set_rules('phone', 'No Telepon', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('marital', 'Marital', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('whatsapp', 'Whatsapp', 'required');
        $this->form_validation->set_rules('product', 'Product', 'required');
        $this->form_validation->set_rules('pesan', 'Pesan', 'required');

        $this->form_validation->set_rules('person_add', 'Person Add', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Fill form completly',
                'errors' => array(
                    'name' => form_error('name'),
                    'phone' => form_error('phone'),
                    'email' => form_error('email'),
                    'birthdate' => form_error('birthdate'),
                    'gender' => form_error('gender'),
                    'marital' => form_error('marital'),
                    'country' => form_error('country'),
                    'whatsapp' => form_error('whatsapp'),
                    'product' => form_error('product'),
                    'pesan' => form_error('pesan'),
                )
            ));
        } else {
            $email_admin = $this->db->where('use', 'yes')->get('email')->result();
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');

            $birthdate = $this->input->post('birthdate');
            $gender = $this->input->post('gender');
            $marital = $this->input->post('marital');
            $country = $this->input->post('country');
            $whatsapp = $this->input->post('whatsapp');
            $product = $this->input->post('product');
            $pesan = $this->input->post('pesan');
            $person_add = $this->input->post('person_add');

            $person_name_arr = $this->input->post('person_name');
            $person_birthdate_arr = $this->input->post('person_birthdate');
            $person_member_arr = $this->input->post('person_member');
            $person_gender_arr = $this->input->post('person_gender');
            $no = 1;


            $message_admin = '

            Hello Admin,<br /><br />
            I send you a quote insurance, I Hope you can help me<br />
            Personal Details:<br /><br />

            Name : ' . $name . '<br>
            Email : ' . $email . '<br>
            Telephone : ' . $phone . '<br>
            Date of Birth : ' . $birthdate . '<br>
            Gender : ' . $gender . '<br>
            Marital Status : ' . $marital . '<br>
            Country : ' . $country . '<br>
            Whatsapp : ' . $whatsapp . '<br>
            Product : ' . $product . '<br>
            Message : ' . $pesan . '<br /><br />';

            if (count($person_name_arr) > 0) {

                $message_admin .= ' 
                - Person Count Added: ' . $person_add . '<br><br>';

                foreach ($person_name_arr as $key => $person_name) {
                    $person_birthdate = $person_birthdate_arr[$key];
                    $person_member = $person_member_arr[$key];
                    $person_gender = $person_gender_arr[$key];

                    $message_admin .= '
                #Person ' . $no . '<br>
                - Name : ' . $person_name . '<br>
                - Date of Birth : ' . $person_birthdate . '<br>
                - Family Member : ' . $person_member . '<br>
                - Gender : ' . $person_gender . '<br><br>
                ';

                    $no++;
                }

            }

            $message_admin .= '
            Thank You <br><br>
            Regarding,<br />
            Emailing System ' . $this->main->web_name();

            $message_user = '
			    Dear ' . $name . ',<br />
			    <br />
			    Thank you for get a quote, We will follow up your message as soon as possible ^_^<br />
			    <br /><br />
			    Regarding,<br />
			    ' . $this->main->web_name();

            $this->main->email_send($email, 'Quote ' . $this->main->web_name(), $message_user);
//            $this->main->mailer_auth('Quote ' . $this->main->web_name(), $email, $name, $message_user);

            foreach ($email_admin as $r) {
                $this->main->email_send($r->email, 'You Have a Quote - ' . $name, $message_admin);
//                $this->main->mailer_auth('You Have a Quote - ' . $name, $r->email, $name, $message_admin);
            }

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Success',
                'message' => 'Thank you for contact us, we will follow up you as soon as possible ^_^',
                'email_admin' => $email_admin,
            ));
        }
    }

    public function team($id)
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where('id', $id)
            ->get('team')
            ->row();
        $data['page']->type = 'profile';

        $this->template->front('team_detail', $data);
    }
}
