<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Portfolio extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front('portfolio');
        $page = $this
            ->db
            ->where(array('id_language' => $data['id_language']))
            ->where_in('type', array('portfolio'))
            ->get('pages')
            ->row();
        $portfolio_list = $this
            ->db
            ->where([
                'id_language' => $data['id_language'],
                'use' => 'yes'
            ])
            ->get('category_portfolio')
            ->result();

        $data['page'] = $page;
        $data['portfolio_list'] = $portfolio_list;
        $this->template->front('portfolio', $data);
    }
}
