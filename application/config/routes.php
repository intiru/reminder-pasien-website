<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$lang_code_list = array('id', 'en');


$route['default_controller'] = 'home';
$route['intiru'] = 'intiru/login';
$route['intiru/login'] = 'intiru/login/process';

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;

$connection = mysqli_connect('localhost', 'root', '', 'intiru_sistem_reminder_pasien_website_official');


$blog = mysqli_query($connection, "SELECT id, title FROM blog");
$blog_category = mysqli_query($connection, "SELECT id, title FROM blog_category");
$product = mysqli_query($connection, "SELECT id, title FROM category_product");
$menu = mysqli_query($connection, "SELECT title_menu, controller_method, type FROM pages WHERE type IN ('home', 'product', 'product_detail', 'quote', 'blog', 'about_us', 'contact_us', 'product_1', 'product_2', 'product_3', 'product_4', 'product_5', 'privacy_policy', 'usage_policy')");
$our_product_title_menu = [];

$blog_title_menu = '';


while ($data = mysqli_fetch_array($menu)) {
    $route[slug($data['title_menu'])] = $data['controller_method'];
    $route[slug($data['title_menu']) . '/(:num)'] = $data['controller_method'];

    if ($data['type'] == 'blog') {
        $blog_title_menu = $data['title_menu'];
    }
}

while ($data = mysqli_fetch_array($blog)) {
        $route[slug($blog_title_menu) . '/detail'] = "blog/detail/" . $data['id'];
        $route[slug($blog_title_menu) . '/' . slug($data['title'])] = "blog/detail/" . $data['id'];
}
while ($data = mysqli_fetch_array($product)) {
    foreach ($lang_code_list as $code) {
        foreach ($our_product_title_menu as $title_menu) {
            $route[$code . '/' . slug($title_menu) . '/' . slug($data['title'])] = "our_product/detail/" . $data['id'];
        }


//        echo $code . '/' . slug($our_product_title_menu[$code]) . '/' . slug($data['title']).'<br />';
    }
}
while ($data = mysqli_fetch_array($blog_category)) {
    foreach ($lang_code_list as $code) {
        $route[$code . '/blog/' . slug($data['title'])] = "blog/category/" . $data['id'];
        $route[$code . '/blog/' . slug($data['title']) . '/(:num)'] = "blog/category/" . $data['id'];
    }
}

foreach ($lang_code_list as $code) {

    $route['^' . $code . '/(.+)$'] = "$1";
    $route['^' . $code . '$'] = $route['default_controller'];
}

function slug($string)
{
    $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '+');
    $replace = array('-', '-', 'and', '-', '-', '-', '', '', '-plus');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
