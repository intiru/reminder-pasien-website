<?php

class Main
{

    private $ci;
    private $web_name = 'Allianz Insurance Bali';
    private $web_url = 'allianzinsurancebali.com';
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 100;

    public function __construct()
    {
        error_reporting(0);
        $this->ci = &get_instance();
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function web_url()
    {
        return $this->web_url;
    }

    function credit()
    {
        return 'development by <a href="https://www.intiru.com">INTIRU - Web Development</a>';
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
                //				/unlink($this->path_images . $filename);
            }
        }
    }

    function data_main()
    {
        $id_language = $this->ci->session->userdata('id_language') ?
            $this->ci->session->userdata('id_language') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;

        $data = array(
            'web_name' => $this->web_name,
            'menu_list' => $this->menu_list(),
            'name' => $this->ci->session->userdata('name'),
            'language' => $this->ci->db->where('use', 'yes')->get('language')->result(),
            'id_language' => $id_language,
        );


        $tab_language = $this->ci->load->view('admins/components/tab_language', $data, TRUE);
        $data['tab_language'] = $tab_language;

        return $data;
    }

    function data_front($type = '')
    {

        $lang_code = 'en';//$this->ci->lang->lang();
        $lang_active = $this->ci->db->where('code', $lang_code)->get('language')->row();
//        $footer = $this->ci->db->select('description')->where(array('type' => 'footer', 'id_language' => $lang_active->id))->get('pages')->row()->description;
//        $footer = $this->ci->db->where(array('type' => 'footer', 'id_language' => $lang_active->id))->get('pages')->row();
//        $bar_footer = $this->ci->db->where(array('type' => 'bar_footer', 'id_language' => $lang_active->id))->get('pages')->row();
//        $category_services = $this->ci->db->where(array('use' => 'yes', 'id_language' => $lang_active->id))->get('category_services')->result();

        //		$service_list = $this->ci->db->select('title,id')->where('id_language', $lang_active->id)->order_by('title', 'ASC')->get('category')->result();
//        $language_list = $this->ci->db->where('use', 'yes')->order_by('title', 'ASC')->get('language')->result();
//        $language_list_other = $this->ci->db->where('use', 'yes')->where_not_in('id', array($lang_active->id))->order_by('title', 'ASC')->get('language')->result();
//        $popup_trial = $this->ci->db->where(array('id_language' => $lang_active->id, 'type' => 'free_trial'))->get('pages')->row();
//        $show_hide = TRUE;
//        $menu_list = $this->ci
//            ->db
//            ->select('title_menu, title, type')
//            ->where_in('type', array('home', 'product', 'product_detail', 'quote', 'blog', 'about_us', 'contact_us','product_1', 'product_2', 'product_3', 'product_4', 'product_5', 'privacy_policy', 'usage_policy'))
//            ->where('id_language', $lang_active->id)
//            ->get('pages')
//            ->result();

//        foreach ($language_list as $row) {
//            if ($type == 'home') {
//                $title_menu = '';
//            } else {
//                $title_menu = $this
//                    ->ci
//                    ->db
//                    ->where(array(
//                        'id_language' => $row->id,
//                        'type' => $type
//                    ))
//                    ->get('pages')
//                    ->row()
//                    ->title_menu;
//            }
//
//            $row->current_url = site_url($row->code . '/' . $this->slug($title_menu));
//        }


//        $dictionary = $this->ci->db->where('id_language', $lang_active->id)->get('dictionary')->result();

//        $menu_product_list = $this->menu_product_list();
//        $category_list = $this->ci
//            ->db
//            ->where(array(
//                'use' => 'yes',
//                'id_language' => $lang_active->id
//            ))
//            ->order_by('title', 'ASC')
//            ->get('category')
//            ->result();
//
//        foreach ($category_list as $row) {
//            $row->tour = $this->ci->db->where('id_category', $row->id)->get('tour')->result();
//        }
//
//        $modal_announcement_status = $this->ci->session->userdata('modal_announcement_status') ? $this->ci->session->userdata('modal_announcement_status') : 'show';


        $data = array(
            'logo_alt' => 'Trusted coconut oil company in Indonesia | PT. Mulia Tunggal Abadi',
            'address' => 'Jl. Raya Puputan No.122c, Sumerta Kelod, Kec. Denpasar Timur, Kota Denpasar, Bali 80234',
            'address_1' => 'Jl. Raya Puputan No.122c, Sumerta Kelod, Kec. Denpasar Timur, Kota Denpasar, Bali 80234',
            'address_2' => 'Jl. Raya Puputan No.122c, Sumerta Kelod, Kec. Denpasar Timur, Kota Denpasar, Bali 80234',
            'address_mini' => 'Denpasar Timur, Bali',
            'address_link' => 'https://goo.gl/maps/mtYjAfaDmYxB3Rjh6',
            'address_link_1' => 'https://goo.gl/maps/mtYjAfaDmYxB3Rjh6',
            'address_link_2' => 'https://goo.gl/maps/mtYjAfaDmYxB3Rjh6',
            'maps' => 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7888.38721516396!2d115.24245809851686!3d-8.673130259039569!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240f31a77aec3%3A0x27962c50cf61ced2!2sKantor%20Pusat%20Allianz%20Denpasar!5e0!3m2!1sid!2sid!4v1668411868637!5m2!1sid!2sid',
            'telephone' => '+62877 7711 8807',
            'telegram' => '@allianzinsurancebali',
            'telegram_link' => 'https://t.me/allianzinsurancebali',
            'phone' => '+62877 7711 8807',
            'phone_link' => 'tel:6287777118807',
            'whatsapp' => '62877 7711 8807',
            'whatsapp_phone' => '+62877 7711 8807',
            'whatsapp_link' => 'https://api.whatsapp.com/send?phone=6287777118807',
            'whatsapp_link_contact' => 'https://wa.me/6287777118807?text=',
            'wechat_id' => '',
            'wechat_link' => '',
            'email_link' => 'mailto:info@allianzinsurancebali.com',
            'email' => 'info@allianzinsurancebali.com',
            'facebook_link' => 'https://web.facebook.com/muliatunggalabadi',
            'line_link' => 'https://msng.link/ln/',
            'twitter_link' => 'https://twitter.com',
            'linkedin_link' => '',
            'instagram_link' => 'https://www.instagram.com',
            'view_secret' => TRUE,
            'author' => 'www.allianzinsurancebali.com',
            //			'services_list' => $service_list,
//            'language_list' => $language_list,
//            'language_list_other' => $language_list_other,
            'lang_code' => $lang_code,
            'lang_active' => $lang_active,
            'id_language' => $lang_active->id,
//            'footer' => $footer,
//            'bar_footer' => $bar_footer,
//            'category_services' => $category_services,
//            'popup_trial' => $popup_trial,
            //            'captcha' => $this->captcha(),
//            'show_hide' => $show_hide,
//            'menu_product_list' => $menu_product_list,
//            'category_list' => $category_list,
//            'id_product_top_menu' => '',
//            'modal_announcement_status' => $modal_announcement_status
        );

//        foreach ($menu_list as $row) {
//            $data['top_' . $row->type] = $row;
//        }
//
//        foreach ($dictionary as $row) {
//            $data['dict_' . $row->dict_variable] = $row->dict_word;
//        }

        //        echo json_encode($data);
        //        exit;

        return $data;
    }

    function menu_product_list()
    {
        $top_menu_list = $this
            ->ci
            ->db
            ->where(array(
                'data_type' => 'top_menu',
                'use' => 'yes'
            ))
            ->order_by('title', 'ASC')
            ->get('product')
            ->result();

        foreach ($top_menu_list as $row) {
            $row->sub_menu_list = $this
                ->ci
                ->db
                ->where(array(
                    'data_type' => 'sub_menu_1',
                    'id_parent' => $row->id,
                    'use' => 'yes'
                ))
                ->order_by('title', 'ASC')
                ->get('product')
                ->result();

            $row->product_list = $this
                ->ci
                ->db
                ->where(array(
                    'data_type' => 'product_description',
                    'id_parent' => $row->id,
                    'use' => 'yes'
                ))
                ->order_by('title', 'ASC')
                ->get('product')
                ->result();

            foreach ($row->sub_menu_list as $row_sub_menu) {
                $row_sub_menu->product_list = $this
                    ->ci
                    ->db
                    ->where(array(
                        'data_type' => 'product_description',
                        'id_parent' => $row_sub_menu->id,
                        'use' => 'yes'
                    ))
                    ->order_by('title', 'ASC')
                    ->get('product')
                    ->result();

                foreach ($row_sub_menu->product_list as $row_descriptions) {
                    $id_parent_2 = $row_descriptions->id_parent;
                    $id_parent = $this
                        ->ci
                        ->db
                        ->select('id_parent')
                        ->where(array(
                            'id' => $id_parent_2,
                            'use' => 'yes'
                        ))
                        ->get('product')
                        ->row()
                        ->id_parent;

                    $row_descriptions->id_parent = $id_parent;
                    $row_descriptions->id_parent_2 = $id_parent_2;
                }
            }
        }

        return $top_menu_list;
    }

    function translate_number($number)
    {
        switch ($number) {
            case 1:
                return "first";
                break;
            case 2:
                return "second";
                break;
            case 3:
                return "third";
                break;
            case 4:
                return "four";
                break;
        }
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('login');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            redirect('intiru/dashboard');
        }
    }

    function permalink($data)
    {

        $slug = '';
        foreach ($data as $r) {
            $slug .= $this->slug($r) . '/';
        }

        return site_url($slug);
    }

    function breadcrumb($data)
    {
        $breadcrumb = '<ul class="breadcrumb">';
        $count = count($data);
        $no = 1;
        foreach ($data as $url => $label) {
            $current = '';
            if ($no == $count) {
                $current = ' class="current"';
            }

            $breadcrumb .= '<li' . $current . '><a href="' . $url . '">' . $label . '</a></li>';
        }

        $breadcrumb .= '</ul>';


        return $breadcrumb;
    }

    function slug($text)
    {

        $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '?', '+');
        $replace = array('-', '-', 'and', '-', '-', '-', '', '', '', '-plus');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function slug_back($slug)
    {
        $slug = trim($slug);
        if (empty($slug)) return '';
        $slug = str_replace('-', ' ', $slug);
        $slug = ucwords($slug);
        return $slug;
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
        //		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function upload_file_slider($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 6000;
        $config['max_width'] = 19200;
        $config['max_height'] = 9000;
        //		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function captcha()
    {
        $this->ci->load->helper(array('captcha', 'string'));
        $this->ci->load->library('session');

        $vals = array(
            'img_path' => './upload/images/captcha/',
            'img_url' => base_url() . 'upload/images/captcha',
            'img_width' => '200',
            'img_height' => 35,
            'border' => 0,
            'expiration' => 7200,
            'word' => random_string('numeric', 5)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $captcha = $cap['image'];

        // store the captcha word in a session
        //$cap['word'];
        $this->ci->session->set_userdata('captcha_mwz', $cap['word']);

        return $captcha;
    }

    function share_link($socmed_type, $title, $link)
    {
        switch ($socmed_type) {
            case "facebook":
                return "https://www.facebook.com/sharer/sharer.php?u=" . $link;
                break;
            case "twitter":
                return "https://twitter.com/home?status=" . $link;
                break;
            case "googleplus":
                return "https://plus.google.com/share?url=" . $link;
                break;
            case "linkedin":
                return "https://www.linkedin.com/shareArticle?mini=true&url=" . $link . "&title=" . $title . "&summary=&source=";
                break;
            case "pinterest":
                return "https://pinterest.com/pin/create/button/?url=" . $title . "&media=" . $link . "&description=";
                break;
            case "email":
                return "mailto:" . $link . "?&subject=" . $title;
            default:
                return $link;
                break;
        }
    }

    function mailer_auth($subject, $to_email, $to_name, $body, $file = '')
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "mail.allianzinsurancebali.com"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->Username = "info@allianzinsurancebali.com"; //user email
            $mail->Password = "Info123!@#"; //password email
            $mail->SetFrom("info@allianzinsurancebali.com", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            if ($file) {
                $mail->addAttachment("upload/images/" . $file);
            }
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    function email_send($email_to, $subject, $body)
    {
        $this->ci->load->library('email'); // Note: no $config param needed
        $this->ci->email->from('info@allianzinsurancebali.com', 'Allianz Insurance Bali');
        $this->ci->email->to($email_to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($body);
        $this->ci->email->send();
    }

    function menu_list()
    {
        $menu = array(
            'MAIN' => array(
                'dashboard' => array(
                    'label' => 'Dashboard',
                    'route' => base_url('intiru/dashboard'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'view_front' => array(
                    'label' => 'View Website',
                    'route' => base_url(''),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),

            'PAGES' => array(
                'home' => array(
                    'label' => 'Beranda',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halaman Umum Beranda',
                            'route' => base_url('intiru/pages/type/home'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'home_slider' => array(
                            'label' => 'Slider Beranda',
                            'route' => base_url('intiru/pages/type/home_slider'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//                        'home_slider' => array(
//                            'label' => 'Slider Beranda',
//                            'route' => base_url('intiru/home_slider'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
                        'sesi_1' => array(
                            'label' => 'Bar Our Service',
                            'route' => base_url('intiru/pages/type/home_our_service'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_2' => array(
                            'label' => 'Bar Insurance Service',
                            'route' => base_url('intiru/pages/type/home_insurance_service'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_3' => array(
                            'label' => 'Bar About Allianz',
                            'route' => base_url('intiru/pages/type/home_about_allianz'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_4' => array(
                            'label' => 'Bar Our Product',
                            'route' => base_url('intiru/pages/type/home_our_product'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_5' => array(
                            'label' => 'Bar Our Customers',
                            'route' => base_url('intiru/pages/type/home_our_customers'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_6' => array(
                            'label' => 'Bar Our Health Tips',
                            'route' => base_url('intiru/pages/type/home_our_health'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    ),
                ),

                'about_us' => array(
                    'label' => 'Tentang Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halaman Tentang Kami',
                            'route' => base_url('intiru/pages/type/about_us'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_1' => array(
                            'label' => 'About Allianz Insurance',
                            'route' => base_url('intiru/pages/type/about_allianz_insurance'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_2' => array(
                            'label' => 'Bar Help Your Insurance',
                            'route' => base_url('intiru/pages/type/about_help_insurance'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_3' => array(
                            'label' => 'Bar FAQ',
                            'route' => base_url('intiru/pages/type/about_faq'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_4' => array(
                            'label' => 'FAQ',
                            'route' => base_url('intiru/faq'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),

//                'product' => array(
//                    'label' => 'Healthcare Insurance',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'pages' => array(
//                            'label' => 'Halaman Healthcare',
//                            'route' => base_url('intiru/pages/type/product_1'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'sesi_1' => array(
//                            'label' => 'About Healthcare',
//                            'route' => base_url('intiru/pages/type/about_healthcare'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'benefit_table' => array(
//                            'label' => 'Benefit Table',
//                            'route' => base_url('intiru/benefit_table'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'extra_table' => array(
//                            'label' => 'Extra Table',
//                            'route' => base_url('intiru/extra_table'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'product_detail' => array(
//                            'label' => 'Tabel Product Detail',
//                            'route' => base_url('intiru/product_detail'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    ),
//                ),
//                'portfolio' => array(
//                    'label' => 'Portofolio',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'pages' => array(
//                            'label' => 'Halaman Portofolio',
//                            'route' => base_url('intiru/pages/type/portfolio'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'categories_portfolio' => array(
//                            'label' => 'Daftar Portofolio',
//                            'route' => base_url('intiru/category_portfolio'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    ),
//                ),

//                'testimonial' => array(
//                    'label' => 'Testimonial',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'home_testimonial' => array(
//                            'label' => 'Halaman Testimonial',
//                            'route' => base_url('intiru/pages/type/testimonial'),
//                            'icon' => 'fab fa-asymmetrik'
//
//                        ), 'gallery_testimonial' => array(
//                            'label' => 'Daftar Foto Testimonial',
//                            'route' => base_url('intiru/testimonial'),
//                            'icon' => 'fab fa-asymmetrik'
//
//                        ),
//                    ),
//                ),

//                'method' => array(
//                    'label' => 'Metode',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'home_page' => array(
//                            'label' => 'Halaman Metode',
//                            'route' => base_url('intiru/pages/type/method'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'sesi_1' => array(
//                            'label' => 'Konten Metode',
//                            'route' => base_url('intiru/pages/type/info_method'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    )
//                ),

//                'price_list' => array(
//                    'label' => 'Paket Harga',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'pages' => array(
//                            'label' => 'Halaman Paket Harga',
//                            'route' => base_url('intiru/pages/type/price_list'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'categories_price_list' => array(
//                            'label' => 'Kategori Price List',
//                            'route' => base_url('intiru/category_price_list'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    ),
//                ),
//
//                'facility' => array(
//                    'label' => 'Fasilitas',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'pages' => array(
//                            'label' => 'Halaman Paket Harga',
//                            'route' => base_url('intiru/pages/type/facility'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'categories_price_list' => array(
//                            'label' => 'Data Fasilitas',
//                            'route' => base_url('intiru/category_facility'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    ),
//                ),

                // 'services' => array(
                //     'label' => 'Layanan Kami',
                //     'route' => 'javascript:;',
                //     'icon' => 'fab fa-asymmetrik',
                //     'sub_menu' => array(
                //         'pages' => array(
                //             'label' => 'Halaman Layanan',
                //             'route' => base_url('intiru/pages/type/services'),
                //             'icon' => 'fab fa-asymmetrik'
                //         ),
                //         'categories_services' => array(
                //             'label' => 'Kategori Layanan',
                //             'route' => base_url('intiru/category_services'),
                //             'icon' => 'fab fa-asymmetrik'
                //         ),
                //     ),
                // ),

                // 'booking' => array(
                //     'label' => 'Booking',
                //     'route' => base_url('intiru/pages/type/booking'),
                //     'icon' => 'fab fa-asymmetrik',
                //     'sub_menu' => array()
                // ),
                // 'about_us' => array(
                //     'label' => 'About Us',
                //     'route' => base_url('intiru/pages/type/about_us'),
                //     'icon' => 'fab fa-asymmetrik',
                //     'sub_menu' => array()
                // ),


                'blog' => array(
                    'label' => 'Blog & Tips',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'blog_page' => array(
                            'label' => 'Halaman Blog & Tips',
                            'route' => base_url('intiru/pages/type/blog'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//                        'blog_category' => array(
//                            'label' => 'Kategori Blog',
//                            'route' => base_url('intiru/blog_category'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
                        'blog_list' => array(
                            'label' => 'Daftar Blog & Tips',
                            'route' => base_url('intiru/blog_content'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),

//                'quote' => array(
//                    'label' => 'Quote',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'pages' => array(
//                            'label' => 'Halaman Quote',
//                            'route' => base_url('intiru/pages/type/quote'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    ),
//                ),
                'quote' => array(
                    'label' => 'Halaman Quote',
                    'route' => base_url('intiru/pages/type/quote'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),

                //                'testimonial' => array(
                //                    'label' => 'Testimonial',
                //                    'route' => 'javascript:;',
                //                    'icon' => 'fab fa-asymmetrik',
                //                    'sub_menu' => array(
                //                        'testimonial_page' => array(
                //                            'label' => 'Testimonial Page',
                //                            'route' => base_url('intiru/pages/type/testimonial'),
                //                            'icon' => 'fab fa-asymmetrik'
                //                        ),
                //                        'testimonial_list' => array(
                //                            'label' => 'Testimonial List',
                //                            'route' => base_url('intiru/testimonial'),
                //                            'icon' => 'fab fa-asymmetrik'
                //                        ),
                //                    )
                //                ),

//                                'faq' => array(
//                                    'label' => 'FAQ',
//                                    'route' => base_url('intiru/pages/type/faq'),
//                                    'icon' => 'fab fa-asymmetrik',
//                                    'sub_menu' => array()
//                                ),
                //
                //                'privacy' => array(
                //                    'label' => 'Kebijakan & Privasi',
                //                    'route' => base_url('intiru/pages/type/privacy_policy'),
                //                    'icon' => 'fab fa-asymmetrik',
                //                    'sub_menu' => array()
                //                ),
                //
                //                'about' => array(
                //                    'label' => 'Tentang Kami',
                //                    'route' => base_url('intiru/pages/type/about_us'),
                //                    'icon' => 'fab fa-asymmetrik',
                //                    'sub_menu' => array()
                //                ),

                //                'profile' => array(
                //                    'label' => 'Tentang Kami',
                //                    'route' => 'javascript:;',
                //                    'icon' => 'fab fa-asymmetrik',
                //                    'sub_menu' => array(
                //                        'profile_page' => array(
                //                            'label' => 'Halaman Tentang Kami',
                //                            'route' => base_url('intiru/pages/type/about_us'),
                //                            'icon' => 'fab fa-asymmetrik'
                //                        ),
                ////                        'profile_team' => array(
                ////                            'label' => 'Daftar Team',
                ////                            'route' => base_url('intiru/profile_team'),
                ////                            'icon' => 'fab fa-asymmetrik'
                ////                        ),
                //                    )
                //                ),
                //				'gallery_video' => array(
                //					'label' => 'Gallery Video',
                //					'route' => 'javascript:;',
                //					'icon' => 'fab fa-asymmetrik',
                //					'sub_menu' => array(
                //						'gallery_photo_page' => array(
                //							'label' => 'Gallery Video Page',
                //							'route' => base_url('intiru/pages/type/gallery_video'),
                //							'icon' => 'fab fa-asymmetrik'
                //						),
                //						'gallery_photo_list' => array(
                //							'label' => 'Gallery Video List',
                //							'route' => base_url('intiru/gallery_video'),
                //							'icon' => 'fab fa-asymmetrik'
                //
                //						),
                //					)
                //				),


                'contact_us' => array(
                    'label' => 'Kontak Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'pages' => array(
                            'label' => 'Halaman Kontak Kami',
                            'route' => base_url('intiru/pages/type/contact_us'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//                        'info_contact_us' => array(
//                            'label' => 'Info Kontak Kami',
//                            'route' => base_url('intiru/pages/type/info_contact_us'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
                    ),
                ),
                //                'reservation' => array(
                //                    'label' => 'Reservation',
                //                    'route' => 'javascript:;',
                //                    'icon' => 'fab fa-asymmetrik',
                //                    'sub_menu' => array(
                //                        'reservation_page' => array(
                //                            'label' => 'Reservation Page',
                //                            'route' => base_url('intiru/pages/type/reservation'),
                //                            'icon' => 'fab fa-asymmetrik'
                //                        ),
                //                        'reservation_list' => array(
                //                            'label' => 'Reservation List',
                //                            'route' => base_url('intiru/reservation'),
                //                            'icon' => 'fab fa-asymmetrik'
                //                        ),
                //                    )
                //                ),
                'footer' => array(
                    'label' => 'Footer Web',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'pages' => array(
                            'label' => 'Halaman Footer',
                            'route' => base_url('intiru/pages/type/footer'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'bar_footer' => array(
                            'label' => 'Bar Footer',
                            'route' => base_url('intiru/pages/type/bar_footer'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    ),
                ),

                'dictionary' => array(
                    'label' => 'Kamus Web',
                    'route' => base_url('intiru/dictionary'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
            'OTHERS MENU' => array(
                'file_manager' => array(
                    'label' => 'File Manager',
                    'route' => base_url('intiru/file_manager'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'email' => array(
                    'label' => 'Email',
                    'route' => base_url('intiru/email'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'language' => array(
                    'label' => 'Language',
                    'route' => base_url('intiru/language'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'admin' => array(
                    'label' => 'Manage Admin',
                    'route' => base_url('intiru/admin'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }
}
